<%-- 
    Document   : Home
    Created on : Jun 5, 2022, 5:31:15 PM
    Author     : genni
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Home | NewSite</title>


        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5.0, minimum-scale=0.86">
        <meta name="author" content="Creativeitem" />

        <meta name="keywords" content="LMS,Learning Management System,Creativeitem,demo,hello,How are you"/>
        <meta name="description" content="Study any topic, anytime. explore thousands of courses for the lowest price ever!" />

        <!--Social sharing content-->
        <meta property="og:title" content="Home" />
        <meta property="og:image" content="http://localhost/uploads/system/home-banner.jpg">
        <meta property="og:url" content="http://localhost/" />
        <meta property="og:type" content="Learning management system" />
        <!--Social sharing content-->

        <link name="favicon" type="image/x-icon" href="http://localhost/uploads/system/favicon.png" rel="shortcut icon" />
        <link rel="favicon" href="http://localhost/assets/frontend/default/img/icons/favicon.ico">
        <link rel="apple-touch-icon" href="http://localhost/assets/frontend/default/img/icons/icon.png">

        <link rel="stylesheet" href="http://localhost/assets/frontend/default/css/jquery.webui-popover.min.css">
        <link rel="stylesheet" href="http://localhost/assets/frontend/default/css/slick.css">
        <link rel="stylesheet" href="http://localhost/assets/frontend/default/css/slick-theme.css">

        <!-- font awesome 5 -->
        <link rel="stylesheet" href="http://localhost/assets/frontend/default/css/fontawesome-all.min.css">

        <link rel="stylesheet" href="http://localhost/assets/frontend/default/css/bootstrap.min.css">

        <link rel="stylesheet" href="http://localhost/assets/frontend/default/css/main.css">
        <link rel="stylesheet" href="http://localhost/assets/frontend/default/css/responsive.css">
        <link rel="stylesheet" href="http://localhost/assets/frontend/default/css/custom.css">
        <link rel="stylesheet" href="http://localhost/assets/frontend/default/css/custom.css">
        <link rel="stylesheet" href="http://localhost/assets/frontend/default/css/tagify.css">
        <link rel="stylesheet" href="http://localhost/assets/global/toastr/toastr.css">
        <script src="http://localhost/assets/backend/js/jquery-3.3.1.min.js"></script>
    </head>
    <body class="gray-bg">
        <section class="menu-area bg-white">
            <div class="container-xl">
                <nav class="navbar navbar-expand-lg bg-white">

                    <ul class="mobile-header-buttons">
                        <li><a class="mobile-nav-trigger" href="#mobile-primary-nav">http://localhost/menu<span></span></a></li>
                        <li><a class="mobile-search-trigger" href="#mobile-search">http://localhost/search<span></span></a></li>
                    </ul>

                    <a href="http://localhost/" class="navbar-brand" href="#"><img src="http://localhost/uploads/system/logo-dark.png" alt="" height="35"></a>

                    <div class="main-nav-wrap">
                        <div class="mobile-overlay"></div>
                        <style type="text/css">
                            @media only screen and (max-width: 767px) {
                                .category.corner-triangle.top-left.pb-0.is-hidden{
                                    display: none !important;
                                }
                                .sub-category.is-hidden{
                                    display: none !important;
                                }
                            }
                        </style>

                        <ul class="mobile-main-nav">
                            <div class="mobile-menu-helper-top"></div>
                            <li class="has-children text-nowrap fw-bold">
                                <a href="">
                                    <i class="fas fa-th d-inline text-20px"></i>
                                    <span class="fw-500">Categories</span>
                                    <span class="has-sub-category"><i class="fas fa-angle-right"></i></span>
                                </a>

                                <ul class="category corner-triangle top-left is-hidden pb-0" >
                                    <li class="go-back"><a href=""><i class="fas fa-angle-left"></i>Menu</a></li>

                                    <li class="all-category-devided mb-0 p-0">
                                        <a href="http://localhost/home/courses" class="py-3">
                                            <span class="icon"><i class="fa fa-align-justify"></i></span>
                                            <span>All courses</span>
                                        </a>
                                    </li>


                                </ul>
                            </li>

                            <div class="mobile-menu-helper-bottom"></div>
                        </ul>
                    </div>

                    <form class="inline-form me-auto" action="http://localhost/home/search" method="get">
                        <div class="input-group search-box mobile-search">
                            <input type="text" name = 'query' class="form-control" placeholder="Search for courses">
                            <div class="input-group-append">
                                <button class="btn" type="submit"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </form>


                    <div class="cart-box menu-icon-box" id = "cart_items">
                        <div class="icon">
                            <a href="http://localhost/home/shopping_cart"><i class="fas fa-shopping-cart"></i></a>
                            <span class="number">0</span>
                        </div>

                        <!-- Cart Dropdown goes here -->
                        <div class="dropdown course-list-dropdown corner-triangle top-right" style="display: none;"> <!-- Just remove the display none from the css to make it work -->
                            <div class="list-wrapper">
                                <div class="item-list">
                                    <ul>
                                    </ul>
                                </div>
                                <div class="dropdown-footer">
                                    <div class="cart-total-price clearfix">
                                        <span>Total:</span>
                                        <div class="float-end">
                                            <span class="current-price">$0</span>
                                            <!-- <span class="original-price">$94.99</span> -->
                                        </div>
                                    </div>
                                    <a href = "http://localhost/home/shopping_cart">Go to cart</a>
                                </div>
                            </div>
                            <div class="empty-box text-center d-none">
                                <p>Your cart is empty.</p>
                                <a href="">Keep Shopping</a>
                            </div>
                        </div>      </div>

                    <span class="signin-box-move-desktop-helper"></span>
                    <div class="sign-in-box btn-group">

                        <a href="http://localhost/home/login" class="btn btn-sign-in">Log in</a>

                        <a href="http://localhost/home/sign_up" class="btn btn-sign-up">Sign up</a>

                    </div> <!--  sign-in-box end -->
                </nav>
            </div>
        </section>
        <link rel="stylesheet" type="text/css" href="http://localhost/assets/frontend/eu-cookie/purecookie.css" async />

        <div class="cookieConsentContainer" id="cookieConsentContainer" style="opacity: .9; display: block; display: none;">
            <!-- <div class="cookieTitle">
              <a>Cookies.</a>
            </div> -->
            <div class="cookieDesc">
                <p>
                    This website uses cookies to personalize content and analyse traffic in order to offer you a better experience.      <a class="link-cookie-policy" href="http://localhost/home/cookie_policy">Cookie policy</a>
                </p>
            </div>
            <div class="cookieButton">
                <a onclick="cookieAccept();">Accept</a>
            </div>
        </div>
        <script>
            $(document).ready(function () {
                if (localStorage.getItem("accept_cookie_academy")) {
                    //localStorage.removeItem("accept_cookie_academy");
                } else {
                    $('#cookieConsentContainer').fadeIn(1000);
                }
            });

            function cookieAccept() {
                if (typeof (Storage) !== "undefined") {
                    localStorage.setItem("accept_cookie_academy", true);
                    localStorage.setItem("accept_cookie_time", "06/05/2022");
                    $('#cookieConsentContainer').fadeOut(1200);
                }
            }
        </script>
        <section class="home-banner-area" id="home-banner-area" style="background-image: url('./img/home-banner.jpg'); background-position: right; background-repeat: no-repeat; padding: 100px 0 75px; background-size: auto 100%; color: #fff;">
            <div class="cropped-home-banner" ></div>
            <div class="container-xl">
                <div class="row">
                    <div class="col position-relative">
                        <div class="home-banner-wrap">
                            <h2 class="fw-bold">Start learning from best platform</h2>
                            <p>Study any topic, anytime. explore thousands of courses for the lowest price ever!</p>
                            <form class="" action="http://localhost/home/search" method="get">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="query" placeholder="What do you want to learn?">
                                    <div class="input-group-append p-6px bg-white">
                                        <button class="btn" type="submit">Search <i class="fas fa-search"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
