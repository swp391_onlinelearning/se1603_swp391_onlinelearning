<%-- 
    Document   : AdminUserDetail
    Created on : Jun 13, 2022, 3:50:50 AM
    Author     : genni
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <title>Dashboard | Academy Learning Club</title>
        <!-- all the meta tags -->
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- all the css files -->
        <link rel="shortcut icon" href="./img/favicon.png">
        <!-- third party css -->
        <link href="./css/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
        <link href="./css/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
        <link href="./css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="./css/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
        <link href="./css/select.bootstrap4.css" rel="stylesheet" type="text/css" />
        <link href="./css/summernote-bs4.css" rel="stylesheet" type="text/css" />
        <link href="./css/fullcalendar.min.css" rel="stylesheet" type="text/css" />
        <link href="./css/dropzone.css" rel="stylesheet" type="text/css" />
        <!-- third party css end -->

        <!-- App css -->
        <link href="./css/app.min.css" rel="stylesheet" type="text/css" />
        <link href="./css/icons.min.css" rel="stylesheet" type="text/css" />

        <link href="./css/backend/main.css" rel="stylesheet" type="text/css" />

        <!-- font awesome 5 -->
        <link href="./css/backend/fontawesome-all.min.css" rel="stylesheet" type="text/css" />
        <link href="./css/fontawesome-iconpicker.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="./css/bootstrap-tagsinput.css">

        <script src="./js/jquery-3.3.1.min.js" charset="utf-8"></script>
        <script src="./js/onDomChange.js"></script></head>
    <body data-layout="detached">
        <c:if test="${sessionScope.acc == null}">
            <c:redirect url="Home"/>  
        </c:if>
        <c:if test="${sessionScope.acc.role != 1}">
            <c:redirect url="Home"/>  
        </c:if>
        <!-- Topbar Start -->
        <div class="navbar-custom topnav-navbar topnav-navbar-dark">
            <div class="container-fluid">
                <!-- LOGO -->
                <a href="http://localhost/Admin" class="topnav-logo" style = "min-width: unset;">
                    <span class="topnav-logo-lg">
                        <img src="./img/logo-light.png" alt="" height="40">
                    </span>
                    <span class="topnav-logo-sm">
                        <img src="./img/logo-light-sm.png" alt="" height="40">
                    </span>
                </a>

                <ul class="list-unstyled topbar-right-menu float-right mb-0">
                    <li class="dropdown notification-list">
                        <a class="nav-link dropdown-toggle arrow-none" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                            <i class="dripicons-view-apps noti-icon"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated dropdown-lg p-0 mt-5 border-top-0" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-278px, 70px, 0px);">

                            <div class="rounded-top py-3 border-bottom bg-primary">
                                <h4 class="text-center text-white">Quick actions</h4>
                            </div>

                            <div class="row row-paddingless" style="padding-left: 15px; padding-right: 15px;">
                                <!--begin:Item-->
                                <div class="col-6 p-0 border-bottom border-right">
                                    <a href="AdminAddCourse" class="d-block text-center py-3 bg-hover-light">
                                        <i class="dripicons-archive text-20"></i>
                                        <span class="w-100 d-block text-muted">Add course</span>
                                    </a>
                                </div>



                                <div class="col-6 p-0 border-right">
                                    <a href="AdminAddUser" class="d-block text-center py-3 bg-hover-light" >
                                        <i class="dripicons-user text-20"></i>
                                        <span class="w-100 d-block text-muted">Add student</span>
                                    </a>
                                </div>


                            </div>
                        </div>
                    </li>
                    <li class="dropdown notification-list">
                        <a class="nav-link dropdown-toggle nav-user arrow-none mr-0" data-toggle="dropdown" id="topbar-userdrop"
                           href="#" role="button" aria-haspopup="true" aria-expanded="false">
                            <span class="account-user-avatar">
                                <img src="./img/placeholder.png" alt="user-image" class="rounded-circle">
                            </span>
                            <span  style="color: #fff;">
                                <span class="account-user-name">${sessionScope.acc.last_name}</span>
                                <span class="account-position">
                                    Admin                    </span>
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated topbar-dropdown-menu profile-dropdown"
                             aria-labelledby="topbar-userdrop">
                            <!-- item-->
                            <div class=" dropdown-header noti-title">
                                <h6 class="text-overflow m-0">Welcome !</h6>
                            </div>

                            <!-- Logout-->
                            <a href="Logout" class="dropdown-item notify-item">
                                <i class="mdi mdi-logout mr-1"></i>
                                <span>Logout</span>
                            </a>

                        </div>
                    </li>
                </ul>
                <a class="button-menu-mobile disable-btn">
                    <div class="lines">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </a>
                <div class="visit_website">
                    <h4 style="color: #fff; float: left;" class="d-none d-md-inline-block"> NewSite</h4>
                    <a href="/Home" target="" class="btn btn-outline-light ml-3 d-none d-md-inline-block">Visit website</a>
                </div>
            </div>
        </div>
        <!-- end Topbar -->    <div class="container-fluid">
            <div class="wrapper">
                <!-- BEGIN CONTENT -->
                <!-- SIDEBAR -->
                <!-- ========== Left Sidebar Start ========== -->
                <div class="left-side-menu left-side-menu-detached">
                    <div class="leftbar-user">
                        <a href="javascript: void(0);">
                            <img src="./img/placeholder.png" alt="user-image" height="42" class="rounded-circle shadow-sm">
                            <span class="leftbar-user-name">Tien Nguyen</span>
                        </a>
                    </div>

                    <!--- Sidemenu -->
                    <ul class="metismenu side-nav side-nav-light">

                        <li class="side-nav-title side-nav-item">Navigation</li>

                        <li class="side-nav-item ">
                            <a href="Dashboard" class="side-nav-link">
                                <i class="dripicons-view-apps"></i>
                                <span>Dashboard</span>
                            </a>
                        </li>

                        <li class="side-nav-item ">
                            <a href="javascript: void(0);" class="side-nav-link ">
                                <i class="dripicons-archive"></i>
                                <span> Courses </span>
                                <span class="menu-arrow"></span>
                            </a>
                            <ul class="side-nav-second-level" aria-expanded="false">
                                <li class="">
                                    <a href="/AdminCourse">Manage courses</a>
                                </li>

                                <li class="">
                                    <a href="/AdminAddCourse">Add new course</a>
                                </li>

                                <li class="">
                                    <a href=".#">Course category (update)</a>
                                </li>


                            </ul>
                        </li>


                        <li class="side-nav-item ">

                        </li>

                        <li class="side-nav-item">

                            <ul class="side-nav-second-level" aria-expanded="false">
                                <li class=""> <a href="http://localhost/admin/admin_revenue">Admin revenue</a> </li>
                                <li class="">
                                    <a href="http://localhost/admin/instructor_revenue">
                                        Instructor revenue							</a>
                                </li>
                            </ul>
                        </li>

                        <li class="side-nav-item ">
                            <a href="javascript: void(0);" class="side-nav-link ">
                                <i class="dripicons-box"></i>
                                <span> Users </span>
                                <span class="menu-arrow"></span>
                            </a>
                            <ul class="side-nav-second-level" aria-expanded="false">
                                <li class="side-nav-item ">
                                    <a href="/AdminUser">View List</a>
                                </li>



                                <li class="side-nav-item ">
                                    <a href="/AdminAddUser">Add new</a>
                                </li>
                            </ul>
                        </li>





                        <li class="side-nav-item">
                            <a href="javascript: void(0);" class="side-nav-link  active ">
                                <i class="dripicons-blog"></i>
                                <span> Blog </span>
                                <span class="menu-arrow"></span>
                            </a>
                            <ul class="side-nav-second-level" aria-expanded="false">
                                <li class="">
                                    <a href="#">All blogs (Update)</a>
                                </li>
                                <li class="">
                                    <a href="#">Add blogs (Update)</a>
                                </li>
                                <li class="">
                                    <a href="/AdminBlogCategory">Blog category</a>
                                </li>
                                <li class="">
                                    <a href="/AdminBlogCategortAdd">Add category</a>
                                </li>   

                            </ul>
                        </li>






                        <li class="side-nav-item  ">
                            <a href="javascript: void(0);" class="side-nav-link">
                                <i class="dripicons-toggles"></i>
                                <span> Settings </span>
                                <span class="menu-arrow"></span>
                            </a>
                            <ul class="side-nav-second-level" aria-expanded="false">
                                <li class="">
                                    <a href="http://localhost/admin/system_settings">System settings</a>
                                </li>

                                <li class="">
                                    <a href="http://localhost/admin/frontend_settings">Website settings</a>
                                </li>

                                <li class="">
                                    <a href="http://localhost/admin/drip_content_settings">Drip content settings</a>
                                </li>




                                <li class="">
                                    <a href="http://localhost/admin/payment_settings">Payment settings</a>
                                </li>
                                <li class="">
                                    <a href="http://localhost/admin/manage_language">Language settings</a>
                                </li>
                                <li class="">
                                    <a href="http://localhost/admin/smtp_settings">Smtp settings</a>
                                </li>
                                <li class="">
                                    <a href="http://localhost/admin/social_login_settings">Social login</a>
                                </li>
                                <li class="">
                                    <a href="http://localhost/admin/about">About</a>
                                </li>
                            </ul>
                        </li>

                        <li class="side-nav-item ">
                            <a href="./manage_profile.html" class="side-nav-link">
                                <i class="dripicons-user"></i>
                                <span>Manage profile</span>
                            </a>
                        </li>
                    </ul>
                </div>         <!-- PAGE CONTAINER-->
                <div class="content-page">
                    <div class="content">
                        <!-- BEGIN PlACE PAGE CONTENT HERE -->
                        <div class="row">

                        </div>




                        <div class="row">
                            <form class="w-100 bg-white radius-10 p-4" action="AdminUpdateUser" method="post">
                                <div class="row">
                                    <div class="col-12 border-bottom mb-3 pb-3">
                                        <h4>User profile</h4>
                                        <p>Add information about yourself to share on your profile</p>

                                        <div class="row">
                                            <div class="w-25 h-25 mx-auto" >
                                                <img class="center mx-auto" style="border-radius: 50%; height: 118px; width:118px" src="./img/placeholder.png">
                                            </div>
                                        </div>
                                    </div>



                                    <div class="col-md-6">

                                        <label class="text-dark fw-600" for="FristName">First name</label>
                                        <div class="input-group">
                                            <span class="input-group-text"><i class="fas fa-user"></i></span>
                                            <input style="pointer-events: none;"  type="text" class="form-control" name = "first_name" id="FristName" placeholder="First name" value="${user.first_name}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="text-dark fw-600" for="FristName">Last name</label>
                                        <div class="input-group">
                                            <span class="input-group-text"><i class="fas fa-user"></i></span>
                                            <input style="pointer-events: none;" type="text" class="form-control" name = "last_name" placeholder="Last name" value="${user.last_name}">
                                        </div>
                                    </div>

                                    <div class="col-12 mt-3">

                                        <hr class="my-5 bg-secondary">

                                        <label class="text-dark fw-600">Email</label>
                                        <div class="input-group mb-3">
                                            <span class="input-group-text"><i class="fab fa-twitter"></i></span>
                                            <input style="pointer-events: none;" type="text" class="form-control" maxlength="60" name = "email" placeholder="" value="${user.email}">
                                        </div>


                                        <label class="text-dark fw-600">Role</label>
                                        <div class="col-mb-3">
                                            <input style="pointer-events: none;" type="text" class="form-control" maxlength="60" name = "role" placeholder="" value="${user.role}">
                                        </div>
                                        <br>


                                        <label class="text-dark fw-600">Avatar</label>
                                        <div class="input-group mb-3">
                                            <span class="input-group-text"><i class="fab fa-linkedin"></i></span>
                                            <input style="pointer-events: none;" type="text" class="form-control" maxlength="60" name = "avatar" placeholder="Update" value="${user.image}">
                                        </div>
                                    </div>

                                    <div class="col-12 pt-4">
                                        <button class="btn red px-5 py-2 radius-8">OK</button>
                                    </div>
                                </div>
                            </form>

                        </div>

                        <script type="text/javascript">
                            $('#unpaid-instructor-revenue').mouseenter(function () {
                                $('#go-to-instructor-revenue').show();
                            });
                            $('#unpaid-instructor-revenue').mouseleave(function () {
                                $('#go-to-instructor-revenue').hide();
                            });
                        </script>
                        <!-- END PLACE PAGE CONTENT HERE -->
                    </div>
                </div>
                <!-- END CONTENT -->
            </div>
        </div>
        <!-- all the js files -->
        <!-- bundle -->
        <script src="./js/backend/app.min.js"></script>
        <!-- third party js -->
        <script src="./js/vendor/Chart.bundle.min.js"></script>
        <script src="./js/vendor/jquery-jvectormap-1.2.2.min.js"></script>
        <script src="./js/vendor/jquery-jvectormap-world-mill-en.js"></script>
        <script src="./js/vendor/jquery.dataTables.min.js"></script>
        <script src="./js/vendor/dataTables.bootstrap4.js"></script>
        <script src="./js/vendor/dataTables.responsive.min.js"></script>
        <script src="./js/vendor/responsive.bootstrap4.min.js"></script>
        <script src="./js/vendor/dataTables.buttons.min.js"></script>
        <script src="./js/vendor/buttons.bootstrap4.min.js"></script>
        <script src="./js/vendor/buttons.html5.min.js"></script>
        <script src="./js/vendor/buttons.flash.min.js"></script>
        <script src="./js/vendor/buttons.print.min.js"></script>
        <script src="./js/vendor/dataTables.keyTable.min.js"></script>
        <script src="./js/vendor/dataTables.select.min.js"></script>
        <script src="./js/vendor/summernote-bs4.min.js"></script>
        <script src="./js/vendor/fullcalendar.min.js"></script>
        <script src="./js/pages/demo.summernote.js"></script>
        <script src="./js/vendor/dropzone.js"></script>
        <script src="./js/page/datatable-initializer.js"></script>
        <script src="./js/backend/font-awesome-icon-picker/fontawesome-iconpicker.min.js" charset="utf-8"></script>
        <script src="./js/vendor/bootstrap-tagsinput.min.js" charset="utf-8"></script>
        <script src="./js/backend/bootstrap-tagsinput.min.js"></script>
        <script src="./js/vendor/dropzone.min.js" charset="utf-8"></script>
        <script src="./js/ui/component.fileupload.js" charset="utf-8"></script>
        <script src="./js/pages/demo.form-wizard.js"></script>
        <!-- dragula js-->
        <script src="./js/vendor/dragula.min.js"></script>
        <!-- component js -->
        <script src="./js/backend/component.dragula.js"></script>

        <script src="./js/backend/custom.js"></script>

        <!-- Dashboard chart's data is coming from this file -->

        <script type="text/javascript">
                            !function (o) {
                                "use strict";
                                var t = function () {
                                    this.$body = o("body"), this.charts = []
                                };
                                t.prototype.respChart = function (r, a, n, e) {
                                    Chart.defaults.global.defaultFontColor = "#8391a2", Chart.defaults.scale.gridLines.color = "#8391a2";
                                    var i = r.get(0).getContext("2d"),
                                            s = o(r).parent();
                                    return function () {
                                        var t;
                                        switch (r.attr("width", o(s).width()), a) {
                                            case "Line":
                                                t = new Chart(i, {
                                                    type: "line",
                                                    data: n,
                                                    options: e
                                                });
                                                break;
                                            case "Doughnut":
                                                t = new Chart(i, {
                                                    type: "doughnut",
                                                    data: n,
                                                    options: e
                                                })
                                        }
                                        return t
                                    }()
                                }, t.prototype.initCharts = function () {
                                    var t = [];
                                    if (0 < o("#task-area-chart").length) {
                                        t.push(this.respChart(o("#task-area-chart"), "Line", {
                                            labels: [
                                                "January",
                                                "February",
                                                "March",
                                                "April",
                                                "May",
                                                "June",
                                                "July",
                                                "August",
                                                "September",
                                                "October",
                                                "November",
                                                "December",
                                            ],
                                            datasets: [{
                                                    label: "This year",
                                                    backgroundColor: "rgba(114, 124, 245, 0.3)",
                                                    borderColor: "#727cf5",
                                                    data: [
                                                        "0",
                                                        "0",
                                                        "0",
                                                        "0",
                                                        "0",
                                                        "0",
                                                        "0",
                                                        "0",
                                                        "0",
                                                        "0",
                                                        "0",
                                                        "0",
                                                    ]
                                                }]
                                        }, {
                                            maintainAspectRatio: !1,
                                            legend: {
                                                display: !1
                                            },
                                            tooltips: {
                                                intersect: !1
                                            },
                                            hover: {
                                                intersect: !0
                                            },
                                            plugins: {
                                                filler: {
                                                    propagate: !1
                                                }
                                            },
                                            scales: {
                                                xAxes: [{
                                                        reverse: !0,
                                                        gridLines: {
                                                            color: "rgba(0,0,0,0.05)"
                                                        }
                                                    }],
                                                yAxes: [{
                                                        ticks: {
                                                            stepSize: 10,
                                                            display: !1
                                                        },
                                                        min: 10,
                                                        max: 100,
                                                        display: !0,
                                                        borderDash: [5, 5],
                                                        gridLines: {
                                                            color: "rgba(0,0,0,0)",
                                                            fontColor: "#fff"
                                                        }
                                                    }]
                                            }
                                        }))
                                    }
                                    if (0 < o("#project-status-chart").length) {
                                        t.push(this.respChart(o("#project-status-chart"), "Doughnut", {
                                            labels: ["Active course", "Pending course"],
                                            datasets: [{
                                                    data: [0, 0],
                                                    backgroundColor: ["#0acf97", "#FFC107"],
                                                    borderColor: "transparent",
                                                    borderWidth: "2"
                                                }]
                                        }, {
                                            maintainAspectRatio: !1,
                                            cutoutPercentage: 80,
                                            legend: {
                                                display: !1
                                            }
                                        }))
                                    }
                                    return t
                                }, t.prototype.init = function () {
                                    var r = this;
                                    Chart.defaults.global.defaultFontFamily = '-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen-Sans,Ubuntu,Cantarell,"Helvetica Neue",sans-serif', r.charts = this.initCharts(), o(window).on("resize", function (t) {
                                        o.each(r.charts, function (t, r) {
                                            try {
                                                r.destroy()
                                            } catch (t) {
                                            }
                                        }), r.charts = r.initCharts()
                                    })
                                }, o.ChartJs = new t, o.ChartJs.Constructor = t
                            }(window.jQuery),
                                    function (t) {
                                        "use strict";
                                        window.jQuery.ChartJs.init()
                                    }();

        </script>

        <script type="text/javascript">
            $(document).ready(function () {
                $(function () {
                    $('.icon-picker').iconpicker();
                });
            });
        </script>

        <!-- Toastr and alert notifications scripts -->
        <script type="text/javascript">
            function notify(message) {
                $.NotificationApp.send("Heads up!", message, "top-right", "rgba(0,0,0,0.2)", "info");
            }

            function success_notify(message) {
                $.NotificationApp.send("Congratulations!", message, "top-right", "rgba(0,0,0,0.2)", "success");
            }

            function error_notify(message) {
                $.NotificationApp.send("Oh snap!", message, "top-right", "rgba(0,0,0,0.2)", "error");
            }

            function error_required_field() {
                $.NotificationApp.send("Oh snap!", "Please fill all the required fields", "top-right", "rgba(0,0,0,0.2)", "error");
            }
        </script>



        <script type="text/javascript">
            function showAjaxModal(url, header)
            {
                // SHOWING AJAX PRELOADER IMAGE
                jQuery('#scrollable-modal .modal-body').html('<div style="width: 100px; height: 100px; line-height: 100px; padding: 0px; text-align: center; margin-left: auto; margin-right: auto;"><div class="spinner-border text-secondary" role="status"></div></div>');
                jQuery('#scrollable-modal .modal-title').html('loading...');
                // LOADING THE AJAX MODAL
                jQuery('#scrollable-modal').modal('show', {backdrop: 'true'});

                // SHOW AJAX RESPONSE ON REQUEST SUCCESS
                $.ajax({
                    url: url,
                    success: function (response)
                    {
                        jQuery('#scrollable-modal .modal-body').html(response);
                        jQuery('#scrollable-modal .modal-title').html(header);
                    }
                });
            }
            function showLargeModal(url, header)
            {
                // SHOWING AJAX PRELOADER IMAGE
                jQuery('#large-modal .modal-body').html('<div style="width: 100px; height: 100px; line-height: 100px; padding: 0px; text-align: center; margin-left: auto; margin-right: auto;"><div class="spinner-border text-secondary" role="status"></div></div>');
                jQuery('#large-modal .modal-title').html('...');
                // LOADING THE AJAX MODAL
                jQuery('#large-modal').modal('show', {backdrop: 'true'});

                // SHOW AJAX RESPONSE ON REQUEST SUCCESS
                $.ajax({
                    url: url,
                    success: function (response)
                    {
                        jQuery('#large-modal .modal-body').html(response);
                        jQuery('#large-modal .modal-title').html(header);
                    }
                });
            }
        </script>

        <!-- (Large Modal)-->
        <div class="modal fade" id="large-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myLargeModalLabel">Large modal</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        ...
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>

        <!-- Scrollable modal -->
        <div class="modal fade" id="scrollable-modal" tabindex="-1" role="dialog" aria-labelledby="scrollableModalTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="scrollableModalTitle">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body ml-2 mr-2">

                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>

        <script type="text/javascript">
            function confirm_modal(delete_url)
            {
                jQuery('#alert-modal').modal('show', {backdrop: 'static'});
                document.getElementById('update_link').setAttribute('href', delete_url);
            }
        </script>


        <!-- Info Alert Modal -->
        <div id="alert-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-body p-4">
                        <div class="text-center">
                            <i class="dripicons-information h1 text-info"></i>
                            <h4 class="mt-2">Heads up!</h4>
                            <p class="mt-3">Are you sure?</p>
                            <button type="button" class="btn btn-info my-2" data-dismiss="modal">Cancel</button>
                            <a href="#" id="update_link" class="btn btn-danger my-2">Continue</a>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <script type="text/javascript">

            function ajax_confirm_modal(delete_url, elem_id)
            {
                jQuery('#ajax-alert-modal').modal('show', {backdrop: 'static'});
                $("#appent_link_a").bind("click", function () {
                    delete_by_ajax_calling(delete_url, elem_id);
                });
            }

            function delete_by_ajax_calling(delete_url, elem_id) {
                $.ajax({
                    url: delete_url,
                    success: function (response) {
                        var response = JSON.parse(response);
                        if (response.status == 'success') {
                            $('#' + elem_id).fadeOut(600);
                            $.NotificationApp.send("Success!", response.message, "top-right", "rgba(0,0,0,0.2)", "success");
                        } else {
                            $.NotificationApp.send("Oh snap!", response.message, "top-right", "rgba(0,0,0,0.2)", "error");
                        }
                    }
                });
            }
        </script>

        <!-- Info Alert Modal -->
        <div id="ajax-alert-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-body p-4">
                        <div class="text-center" id="appent_link">
                            <i class="dripicons-information h1 text-info"></i>
                            <h4 class="mt-2">Heads up!</h4>
                            <p class="mt-3">Are you sure?</p>
                            <button type="button" class="btn btn-info my-2" data-dismiss="modal">Cancel</button>
                            <a id="appent_link_a" href="javascript:;" class="btn btn-danger my-2" data-dismiss="modal">Continue</a>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->    <script type="text/javascript">
            function togglePriceFields(elem) {
                if ($("#" + elem).is(':checked')) {
                    $('.paid-course-stuffs').slideUp();
                } else
                    $('.paid-course-stuffs').slideDown();
            }
        </script>


        <script type="text/javascript">
            function switch_language(language) {
                $.ajax({
                    url: 'http://localhost/home/site_language',
                    type: 'post',
                    data: {language: language},
                    success: function (response) {
                        setTimeout(function () {
                            location.reload();
                        }, 500);
                    }
                });
            }
        </script></body>
</html>
