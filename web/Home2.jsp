<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Home | NewSite</title>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5.0, minimum-scale=0.86">
        <meta name="author" content="Creativeitem" />

        <meta name="keywords" content="LMS,Learning Management System,Creativeitem,demo,hello,How are you"/>
        <meta name="description" content="Study any topic, anytime. explore thousands of courses for the lowest price ever!" />

        <!--Social sharing content-->


        <!--Drips icons-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dripicons/2.0.0/webfont.min.css" integrity="sha512-pi7KSLdGMxSE62WWJ62B1R5/H7WNnIsj2f51MikplRt31K0uCZ1lfPSw/0Jb1flSz6Ed2YLSlox6Uulf7CaFiA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dripicons/2.0.0/webfont.min.css" integrity="sha512-pi7KSLdGMxSE62WWJ62B1R5/H7WNnIsj2f51MikplRt31K0uCZ1lfPSw/0Jb1flSz6Ed2YLSlox6Uulf7CaFiA==" crossorigin="anonymous" referrerpolicy="no-referrer" />

        <!--Material Design Icon-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/2.0.46/css/materialdesignicons.min.css" integrity="sha512-HmBTsbqKSDy0wIk8SGSCj68xUg8b22mGtXx8cXF64qcmnQnJepz6Aq37X43gF/WhbvqPcx68GoiaWu8wE8/y4g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/2.0.46/css/materialdesignicons.css" integrity="sha512-OSd9YjMibKZ0CN1njc/7H/IK8zmRevGk+n/Z+jAyClgFZ+sOvQKzmeJKBKW313Ln+630pAlcCMjX1dzTDGe4+w==" crossorigin="anonymous" referrerpolicy="no-referrer" />


        <link name="favicon" type="image/x-icon" href="./img/favicon.png" rel="shortcut icon" />
        <link rel="favicon" href="./img/favicon.ico">
        <link rel="apple-touch-icon" href="./img/icon.png">


        <!-- font awesome 5 -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.3.1/css/all.min.css" integrity="sha512-KulI0psuJQK8UMpOeiMLDXJtGOZEBm8RZNTyBBHIWqoXoPMFcw+L5AEo0YMpsW8BfiuWrdD1rH6GWGgQBF59Lg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="./css/fontawesome-all.min.css">

        <link rel="stylesheet" href="./css/bootstrap.min.css">

        <link rel="stylesheet" href="./css/main.css">
        <link rel="stylesheet" href="./css/responsive.css">
        <link rel="stylesheet" href="./css/custom.css">
        <link rel="stylesheet" href="./css/custom.css">
        <link rel="stylesheet" href="./css/tagify.css">
        <link rel="stylesheet" href="./toastr.css">
        <script src="./js/jquery-3.3.1.min.js"></script>
    </head>
    <body class="gray-bg">
        <jsp:include page="header.jsp"/>
        <link rel="stylesheet" type="text/css" href="http://localhost/onlinecource/assets/frontend/eu-cookie/purecookie.css" async />

        <div class="cookieConsentContainer" id="cookieConsentContainer" style="opacity: .9; display: block; display: none;">
            <!-- <div class="cookieTitle">
              <a>Cookies.</a>
            </div> -->
            <div class="cookieDesc">
                <p>
                    This website uses cookies to personalize content and analyse traffic in order to offer you a better experience.      <a class="link-cookie-policy" href="http://localhost/onlinecource/home/cookie_policy">Cookie policy</a>
                </p>
            </div>
            <div class="cookieButton">
                <a onclick="cookieAccept();">Accept</a>
            </div>
        </div>
        <script>
            $(document).ready(function () {
                if (localStorage.getItem("accept_cookie_academy")) {
                    //localStorage.removeItem("accept_cookie_academy");
                } else {
                    $('#cookieConsentContainer').fadeIn(1000);
                }
            });

            function cookieAccept() {
                if (typeof (Storage) !== "undefined") {
                    localStorage.setItem("accept_cookie_academy", true);
                    localStorage.setItem("accept_cookie_time", "06/27/2022");
                    $('#cookieConsentContainer').fadeOut(1200);
                }
            }
        </script>
        <section class="home-banner-area" id="home-banner-area" style="background-image: url('./img/home-banner.jpg'); background-position: right; background-repeat: no-repeat; padding: 100px 0 75px; background-size: auto 100%; color: #fff;">
            <div class="cropped-home-banner" ></div>
            <div class="container-xl">
                <div class="row">
                    <div class="col position-relative">
                        <div class="home-banner-wrap">
                            <h2 class="fw-bold">Start learning from best platform</h2>
                            <p>Study any topic, anytime. explore thousands of courses for the lowest price ever!</p>
                            <form class="" action="http://localhost/onlinecource/home/search" method="get">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="query" placeholder="What do you want to learn?">
                                    <div class="input-group-append p-6px bg-white">
                                        <button class="btn" type="submit">Search <i class="fas fa-search"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                var border_bottom = $('.home-banner-wrap').height() + 242;
                $('.cropped-home-banner').css('border-bottom', border_bottom + 'px solid #f1f7f8');

                mRight = Number("1.4994232987313") * $('.home-banner-area').outerHeight();
                $('.cropped-home-banner').css('right', (mRight - 65) + 'px');
            </script>
        </section>




        <section class="home-fact-area">
            <div class="container-lg">
                <div class="row">
                    <div class="col-md-4 d-flex">
                        <div class="home-fact-box mr-md-auto mr-auto">
                            <i class="fas fa-bullseye float-start"></i>
                            <div class="text-box">
                                <h4>1 Online courses</h4>
                                <p>Explore a variety of fresh topics</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 d-flex">
                        <div class="home-fact-box mr-md-auto mr-auto">
                            <i class="fa fa-check float-start"></i>
                            <div class="text-box">
                                <h4>Expert instruction</h4>
                                <p>Find the right course for you</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 d-flex">
                        <div class="home-fact-box mr-md-auto mr-auto">
                            <i class="fa fa-clock float-start"></i>
                            <div class="text-box">
                                <h4>Lifetime access</h4>
                                <p>Learn on your schedule</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>



        <section class="course-carousel-area">
            <div class="container-lg">
                <div class="row">
                    <div class="col">
                        <h3 class="course-carousel-title mb-4">Top 10 Latest courses</h3>

                        <!-- page loader -->
                        <div class="animated-loader"><div class="spinner-border text-secondary" role="status"></div></div>

                        <div class="course-carousel shown-after-loading" style="display: none;">
                            <div class="col">
                                <div class="course-box-wrap"> 
                                    <div class="row">
                                        <c:forEach items="${List}" var="o">
                                            <div class="course-box ms-1 col">
                                                <a href="CourseDetail?id=${o.id}">
                                                <div class="course-image">
                                                    <img style="width: 110px; height: 120px;" src="${o.thumbnail}"  alt="" class="img-fluid">
                                                </div>
                                                
                                                <div class="course-details">
                                                    <h5 class="title">${o.title}</h5>
                                                    <hr class="divider-1">
                                                    <div class="d-block">
                                                        <div class="floating-user d-inline-block">
                                                            <img src="./img/placeholder.png" width="30px" data-bs-toggle="tooltip" data-bs-placement="top" title="Minh Kỳ"  onclick="return check_action(this, 'http://localhost/onlinecource/home/instructor_page/1');">
                                                        </div>
                                                        <p class="price text-right d-inline-block float-end">Free</p>
                                                    </div>
                                                </div>
                                                </a>        
                                            </div>
                                        </c:forEach>
                                        

                                    </div>

                                </div>


                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </section>


        <section class="section-blog py-5">
            <div class="container-lg">
                <div class="row row-cols-1 row-cols-lg-4 row-cols-md-3 g-4 justify-content-center">
                    <div class="col-12">
                        <h4 class="fw-700">Latest blogs</h4>
                    </div>
                    <c:forEach items="${laster}" var="o">
                        <div class="col">

                            <div class="card radius-10">
                                <img src="${o.thumbnail}" class="card-img-top radius-top-10" alt="Test blog">
                                <div class="card-body pt-4">
                                    <p class="card-text">
                                        <small class="text-muted">Created by - <a href="BlogDetail?id=${o.blog_id}">Minh Kỳ</a></small>
                                    </p>
                                    <h5 class="card-title"><a href="BlogDetail?id=${o.blog_id}">${o.title}</a></h5>
                                    <p class="card-text ellipsis-line-3">
                                        ${o.short_description}                              </p>

                                    <a class="fw-600" href="BlogDetail?id=${o.blog_id}">More details</a>

                                    <p class="card-text mt-2 mb-0">
                                        <small class="text-muted text-12px">Published - ${o.added_date}</small>
                                    </p>
                                </div>
                            </div>

                        </div>
                    </c:forEach>
                    <div class="col-12">
                        <a class="float-end btn btn-outline-secondary px-3 fw-600" href="Blogs">View all</a>
                    </div>
                </div>
            </div>
        </section>


        <div class="container-xl">
            <div class="row justify-content-evenly py-3 mb-4">
                <div class="col-md-6  mt-3 mt-md-0">
                    <div class="become-user-label text-center mt-3">
                        <h3 class="pb-4">Join now to start learning</h3>
                        <a href="http://localhost/onlinecource/home/sign_up">Get started</a>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="become-user-label text-center mt-3">
                        <h3 class="pb-4">Become instructor</h3>
                        <a href="http://localhost/onlinecource/home/sign_up">Join now</a>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            function handleWishList(elem) {

                $.ajax({
                    url: 'http://localhost/onlinecource/home/handleWishList',
                    type: 'POST',
                    data: {
                        course_id: elem.id
                    },
                    success: function (response) {
                        if (!response) {
                            window.location.replace("http://localhost/onlinecource/login");
                        } else {
                            if ($(elem).hasClass('active')) {
                                $(elem).removeClass('active')
                            } else {
                                $(elem).addClass('active')
                            }
                            $('#wishlist_items').html(response);
                        }
                    }
                });
            }

            function handleCartItems(elem) {
                url1 = 'http://localhost/onlinecource/home/handleCartItems';
                url2 = 'http://localhost/onlinecource/home/refreshWishList';
                $.ajax({
                    url: url1,
                    type: 'POST',
                    data: {
                        course_id: elem.id
                    },
                    success: function (response) {
                        $('#cart_items').html(response);
                        if ($(elem).hasClass('addedToCart')) {
                            $('.big-cart-button-' + elem.id).removeClass('addedToCart')
                            $('.big-cart-button-' + elem.id).text("Add to cart");
                        } else {
                            $('.big-cart-button-' + elem.id).addClass('addedToCart')
                            $('.big-cart-button-' + elem.id).text("Added to cart");
                        }
                        $.ajax({
                            url: url2,
                            type: 'POST',
                            success: function (response) {
                                $('#wishlist_items').html(response);
                            }
                        });
                    }
                });
            }

            function handleEnrolledButton() {
                $.ajax({
                    url: 'http://localhost/onlinecource/home/isLoggedIn',
                    success: function (response) {
                        if (!response) {
                            window.location.replace("http://localhost/onlinecource/login");
                        }
                    }
                });
            }

            $(document).ready(function () {
                if (!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                    if ($(window).width() >= 840) {
                        $('a.has-popover').webuiPopover({
                            trigger: 'hover',
                            animation: 'pop',
                            placement: 'horizontal',
                            delay: {
                                show: 500,
                                hide: null
                            },
                            width: 330
                        });
                    } else {
                        $('a.has-popover').webuiPopover({
                            trigger: 'hover',
                            animation: 'pop',
                            placement: 'vertical',
                            delay: {
                                show: 100,
                                hide: null
                            },
                            width: 335
                        });
                    }
                }

                if ($(".course-carousel")[0]) {
                    $(".course-carousel").slick({
                        dots: false,
                        infinite: false,
                        speed: 300,
                        slidesToShow: 4,
                        slidesToScroll: 4,
                        swipe: false,
                        touchMove: false,
                        responsive: [
                            {breakpoint: 840, settings: {slidesToShow: 3, slidesToScroll: 3, }, },
                            {breakpoint: 620, settings: {slidesToShow: 2, slidesToScroll: 2, }, },
                            {breakpoint: 480, settings: {slidesToShow: 1, slidesToScroll: 1, }, },
                        ],
                    });
                }

                if ($(".top-istructor-slick")[0]) {
                    $(".top-istructor-slick").slick({
                        dots: false
                    });
                }
            });
        </script><footer class="footer-area d-print-none bg-gray mt-5 pt-5">
            <div class="container-xl">
                <div class="row mb-3">
                    <div class="col-6 col-sm-6 col-md-3">
                        <h5 class="text-muted mb-3">Top categories</h5>
                        <ul class="list-unstyled text-small">
                            <li class="mb-2">
                                <a class="link-secondary footer-hover-link" href="http://localhost/onlinecource/home/courses?category=java">
                                    Java                  <!-- <span class="fw-700 text-end">()</span> -->
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-6 col-sm-6 col-md-3">
                        <h5 class="text-muted mb-3">Useful links</h5>
                        <ul class="list-unstyled text-small">
                            <li class="mb-2"><a class="link-secondary footer-hover-link" href="http://localhost/onlinecource/blog">Blog</a></li>
                            <li class="mb-2"><a class="link-secondary footer-hover-link" href="http://localhost/onlinecource/home/courses">All courses</a></li>
                            <li class="mb-2"><a class="link-secondary footer-hover-link" href="http://localhost/onlinecource/home/sign_up">Sign up</a></li>
                            <li class="mb-2"><a class="link-secondary footer-hover-link" href="http://localhost/onlinecource/home/login">Login</a></li>
                        </ul>
                    </div>
                    <div class="col-6 col-sm-6 col-md-3">
                        <h5 class="text-muted mb-3">Help</h5>
                        <ul class="list-unstyled text-small">
                            <li class="mb-2"><a class="link-secondary footer-hover-link" href="http://localhost/onlinecource/home/about_us">About us</a></li>
                            <li class="mb-2"><a class="link-secondary footer-hover-link" href="http://localhost/onlinecource/home/privacy_policy">Privacy policy</a></li>
                            <li class="mb-2"><a class="link-secondary footer-hover-link" href="http://localhost/onlinecource/home/terms_and_condition">Terms and condition</a></li>
                            <li class="mb-2"><a class="link-secondary footer-hover-link" href="http://localhost/onlinecource/home/refund_policy">Refund policy</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-12 order-sm-first">

                        <span class="d-block mb-1 mt-2 fw-600" style="font-size: 14.5px; line-height: 28px">Study any topic, anytime. explore thousands of courses for the lowest price ever!</span>

                        <ul class="footer-social-link">
                            <li class="mb-1">
                                <a href="https://facebook.com"><i class="fab fa-facebook-f"></i></a>
                            </li>
                            <li class="mb-1">
                                <a href="https://twitter.com"><i class="fab fa-twitter"></i></a>
                            </li>
                            <li class="mb-1">
                                <a href="https://linkedin.com"><i class="fab fa-linkedin"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <section class="border-top">
                <div class="container-xl">
                    <div class="row mt-3 py-1">
                        <div class="col-6 col-sm-6 col-md-3 text-muted text-13px">
                            &copy; 2021 Academy, All rights reserved        </div>

                        <div class="col-6 col-sm-6 col-md-3 d-none d-md-block"></div>
                        <div class="col-6 col-sm-6 col-md-3 d-none d-md-block"></div>
                        <div class="col-6 col-sm-6 col-md-3 text-center text-md-start">
                            <select class="language_selector" onchange="switch_language(this.value)">
                                <option value="english" selected>English</option>
                            </select>
                        </div>
                    </div>
                </div>
            </section>
        </footer>

        <script type="text/javascript">
            function switch_language(language) {
                $.ajax({
                    url: 'http://localhost/onlinecource/home/site_language',
                    type: 'post',
                    data: {language: language},
                    success: function (response) {
                        setTimeout(function () {
                            location.reload();
                        }, 500);
                    }
                });
            }
        </script>



        <!-- PAYMENT MODAL -->
        <!-- Modal -->

        <!-- Modal -->
        <div class="modal fade multi-step" id="EditRatingModal" tabindex="-1" role="dialog" aria-hidden="true" reset-on-close="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content edit-rating-modal">
                    <div class="modal-header">
                        <h5 class="modal-title step-1" data-step="1">Step 1</h5>
                        <h5 class="modal-title step-2" data-step="2">Step 2</h5>
                        <h5 class="m-progress-stats modal-title">
                            &nbsp;of&nbsp;<span class="m-progress-total"></span>
                        </h5>

                        <button type="button" class="close" data-bs-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="m-progress-bar-wrapper">
                        <div class="m-progress-bar">
                        </div>
                    </div>
                    <div class="modal-body step step-1">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="modal-rating-box">
                                        <h4 class="rating-title">How would you rate this course overall?</h4>
                                        <fieldset class="your-rating">

                                            <input type="radio" id="star5" name="rating" value="5" />
                                            <label class = "full" for="star5"></label>

                                            <!-- <input type="radio" id="star4half" name="rating" value="4 and a half" />
                                            <label class="half" for="star4half"></label> -->

                                            <input type="radio" id="star4" name="rating" value="4" />
                                            <label class = "full" for="star4"></label>

                                            <!-- <input type="radio" id="star3half" name="rating" value="3 and a half" />
                                            <label class="half" for="star3half"></label> -->

                                            <input type="radio" id="star3" name="rating" value="3" />
                                            <label class = "full" for="star3"></label>

                                            <!-- <input type="radio" id="star2half" name="rating" value="2 and a half" />
                                            <label class="half" for="star2half"></label> -->

                                            <input type="radio" id="star2" name="rating" value="2" />
                                            <label class = "full" for="star2"></label>

                                            <!-- <input type="radio" id="star1half" name="rating" value="1 and a half" />
                                            <label class="half" for="star1half"></label> -->

                                            <input type="radio" id="star1" name="rating" value="1" />
                                            <label class = "full" for="star1"></label>

                                            <!-- <input type="radio" id="starhalf" name="rating" value="half" />
                                            <label class="half" for="starhalf"></label> -->

                                        </fieldset>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="modal-course-preview-box">
                                        <div class="card">
                                            <img class="card-img-top img-fluid" id = "course_thumbnail_1" alt="">
                                            <div class="card-body">
                                                <h5 class="card-title" class = "course_title_for_rating" id = "course_title_1"></h5>
                                                <p class="card-text" id = "instructor_details">

                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-body step step-2">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="modal-rating-comment-box">
                                        <h4 class="rating-title">Write a public review</h4>
                                        <textarea id = "review_of_a_course" name = "review_of_a_course" placeholder="Describe your experience what you got out of the course and other helpful highlights. What did the instructor do well and what could use some improvement?" maxlength="65000" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="modal-course-preview-box">
                                        <div class="card">
                                            <img class="card-img-top img-fluid" id = "course_thumbnail_2" alt="">
                                            <div class="card-body">
                                                <h5 class="card-title" class = "course_title_for_rating" id = "course_title_2"></h5>
                                                <p class="card-text">
                                                    -
                                                    Minh Kỳ                    </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="course_id" id = "course_id_for_rating" value="">
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary next step step-1" data-step="1" onclick="sendEvent(2)">Next</button>
                        <button type="button" class="btn btn-primary previous step step-2 mr-auto" data-step="2" onclick="sendEvent(1)">Previous</button>
                        <button type="button" class="btn btn-primary publish step step-2" onclick="publishRating($('#course_id_for_rating').val())" id = "">Publish</button>
                    </div>
                </div>
            </div>
        </div><!-- Modal -->


        <script src="http://localhost/onlinecource/assets/frontend/default/js/vendor/modernizr-3.5.0.min.js"></script>
        <script src="http://localhost/onlinecource/assets/frontend/default/js/vendor/jquery-3.2.1.min.js"></script>
        <script src="http://localhost/onlinecource/assets/frontend/default/js/popper.min.js"></script>
        <script src="http://localhost/onlinecource/assets/frontend/default/js/bootstrap.min.js"></script>

        <script src="http://localhost/onlinecource/assets/frontend/default/js/slick.min.js"></script>
        <script src="http://localhost/onlinecource/assets/frontend/default/js/jquery.webui-popover.min.js"></script>


        <script src="http://localhost/onlinecource/assets/frontend/default/js/main.js"></script>
        <script src="http://localhost/onlinecource/assets/global/toastr/toastr.min.js"></script>
        <script src="http://localhost/onlinecource/assets/frontend/default/js/jquery.form.min.js"></script>
        <script src="http://localhost/onlinecource/assets/frontend/default/js/jQuery.tagify.js"></script>

        <!-- SHOW TOASTR NOTIFIVATION -->


        <script type="text/javascript">
                            $(function () {
                                $('[data-bs-toggle="tooltip"]').tooltip()
                            });
                            if ($('.tagify').height()) {
                                $('.tagify').tagify();
                            }
        </script><script type="text/javascript">
            function showAjaxModal(url)
            {
                // SHOWING AJAX PRELOADER IMAGE
                jQuery('#modal_ajax .modal-body').html('<div class="w-100 text-center pt-5"><img class="mt-5 mb-5" width="80px" src="http://localhost/onlinecource/assets/global/gif/page-loader-2.gif"></div>');

                // LOADING THE AJAX MODAL
                jQuery('#modal_ajax').modal('show', {backdrop: 'true'});

                // SHOW AJAX RESPONSE ON REQUEST SUCCESS
                $.ajax({
                    url: url,
                    success: function (response)
                    {
                        jQuery('#modal_ajax .modal-body').html(response);
                    }
                });
            }

            function lesson_preview(url, title) {
                // SHOWING AJAX PRELOADER IMAGE
                jQuery('.lesson_preview_header').html(title);
                jQuery('#lesson_preview .modal-body').html('<div class="w-100 text-center pt-5"><img class="mt-5 mb-5" width="80px" src="http://localhost/onlinecource/assets/global/gif/page-loader-2.gif"></div>');

                // LOADING THE AJAX MODAL
                jQuery('#lesson_preview').modal('show', {backdrop: 'true'});

                // SHOW AJAX RESPONSE ON REQUEST SUCCESS
                $.ajax({
                    url: url,
                    success: function (response)
                    {
                        jQuery('#lesson_preview .modal-body').html(response);
                    }
                });
            }
        </script>

        <!-- (Ajax Modal)-->
        <div class="modal fade" id="modal_ajax">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-bs-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body" style="overflow:auto;">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-bs-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="lesson_preview" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content course-preview-modal">
                    <div class="modal-header">
                        <h5 class="lesson_preview_header"></h5>
                        <button type="button" class="close" data-bs-dismiss="modal" onclick="window.location.reload()">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                    </div>
                </div>
            </div>
        </div>




        <script type="text/javascript">
            function confirm_modal(delete_url)
            {
                jQuery('#modal-4').modal('show', {backdrop: 'static'});
                document.getElementById('delete_link').setAttribute('href', delete_url);
            }
        </script>

        <!-- (Normal Modal)-->
        <div class="modal fade" id="modal-4">
            <div class="modal-dialog">
                <div class="modal-content" style="margin-top:100px;">

                    <div class="modal-header">
                        <h4 class="modal-title text-center">Are you sure ?</h4>
                        <button type="button" class="btn btn-outline-secondary px-1 py-0" data-bs-dismiss="modal" aria-hidden="true"><i class="fas fa-times-circle"></i></button>
                    </div>


                    <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
                        <a href="#" class="btn btn-danger btn-yes" id="delete_link">Yes</a>
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>


        <script type="text/javascript">
            function async_modal() {
                const asyncModal = new Promise(function (resolve, reject) {
                    $('#modal-4').modal('show');
                    $('#modal-4 .btn-yes').click(function () {
                        resolve(true);
                    });
                    $('#modal-4 .btn-cancel').click(function () {
                        resolve(false);
                    });
                });
                return asyncModal;
            }
        </script>
        <script type="text/javascript">
            function toggleRatingView(course_id) {
                $('#course_info_view_' + course_id).toggle();
                $('#course_rating_view_' + course_id).toggle();
                $('#edit_rating_btn_' + course_id).toggle();
                $('#cancel_rating_btn_' + course_id).toggle();
            }

            function publishRating(course_id) {
                var review = $('#review_of_a_course_' + course_id).val();
                var starRating = 0;
                starRating = $('#star_rating_of_course_' + course_id).val();
                if (starRating > 0) {
                    $.ajax({
                        type: 'POST',
                        url: 'http://localhost/onlinecource/home/rate_course',
                        data: {course_id: course_id, review: review, starRating: starRating},
                        success: function (response) {
                            location.reload();
                        }
                    });
                } else {

                }
            }

            function isTouchDevice() {
                return (('ontouchstart' in window) ||
                        (navigator.maxTouchPoints > 0) ||
                        (navigator.msMaxTouchPoints > 0));
            }

            function viewMore(element, visibility) {
                if (visibility == "hide") {
                    $(element).parent(".view-more-parent").addClass("expanded");
                    $(element).remove();
                } else if ($(element).hasClass("view-more")) {
                    $(element).parent(".view-more-parent").addClass("expanded has-hide");
                    $(element)
                            .removeClass("view-more")
                            .addClass("view-less")
                            .html("- View less");
                } else if ($(element).hasClass("view-less")) {
                    $(element).parent(".view-more-parent").removeClass("expanded has-hide");
                    $(element)
                            .removeClass("view-less")
                            .addClass("view-more")
                            .html("+ View more");
                }
            }

            function redirect_to(url) {
                if (!isTouchDevice() && $(window).width() > 767) {
                    window.location.replace(url);
                }
            }

            //Event call after loading page
            document.addEventListener('DOMContentLoaded', function () {
                setTimeout(function () {
                    $('.animated-loader').hide();
                    $('.shown-after-loading').show();
                });
            }, false);


            function check_action(e, url) {
                var tag = $(e).prop("tagName").toLowerCase();
                if (tag == 'a') {
                    return true;
                } else if (tag != 'a' && url) {
                    $(location).attr('href', url);
                    return false;
                } else {
                    return true;
                }
            }
        </script>
    </body>