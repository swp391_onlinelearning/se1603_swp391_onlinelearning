<%-- 
    Document   : CourseDetail
    Created on : Jun 18, 2022, 3:55:52 PM
    Author     : genni
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>${course.title}</title>


        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5.0, minimum-scale=0.86">
        <meta name="author" content="Creativeitem" />

        <meta name="keywords" content=""/>
        <meta name="description" content="Reactjs" />

        <!--Social sharing content-->
        <meta property="og:title" content="300 bài code thiếu nhi" />
        <meta property="og:image" content="./img/course_thumbnail_default_1.jpg">
        <meta property="og:url" content="http://localhost/home/course/300-b%C3%A0i-code-thi%E1%BA%BFu-nhi/1" />
        <meta property="og:type" content="Learning management system" />
        <!--Social sharing content-->

        <link name="favicon" type="image/x-icon" href="http://localhost/uploads/syste./imghttp://localhost/assets/frontend/default/img/icons/favicon.ico">
        <link rel="apple-touch-icon" href="./img/icon.png">


        <!--Drips icons-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dripicons/2.0.0/webfont.min.css" integrity="sha512-pi7KSLdGMxSE62WWJ62B1R5/H7WNnIsj2f51MikplRt31K0uCZ1lfPSw/0Jb1flSz6Ed2YLSlox6Uulf7CaFiA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dripicons/2.0.0/webfont.min.css" integrity="sha512-pi7KSLdGMxSE62WWJ62B1R5/H7WNnIsj2f51MikplRt31K0uCZ1lfPSw/0Jb1flSz6Ed2YLSlox6Uulf7CaFiA==" crossorigin="anonymous" referrerpolicy="no-referrer" />

        <!--Material Design Icon-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/2.0.46/css/materialdesignicons.min.css" integrity="sha512-HmBTsbqKSDy0wIk8SGSCj68xUg8b22mGtXx8cXF64qcmnQnJepz6Aq37X43gF/WhbvqPcx68GoiaWu8wE8/y4g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/2.0.46/css/materialdesignicons.css" integrity="sha512-OSd9YjMibKZ0CN1njc/7H/IK8zmRevGk+n/Z+jAyClgFZ+sOvQKzmeJKBKW313Ln+630pAlcCMjX1dzTDGe4+w==" crossorigin="anonymous" referrerpolicy="no-referrer" />


        <!-- font awesome 5 -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.3.1/css/all.min.css" integrity="sha512-KulI0psuJQK8UMpOeiMLDXJtGOZEBm8RZNTyBBHIWqoXoPMFcw+L5AEo0YMpsW8BfiuWrdD1rH6GWGgQBF59Lg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="./css/fontawesome-all.min.css">

        <link rel="stylesheet" href="./css/bootstrap.min.css">

        <link rel="stylesheet" href="./css/main.css">
        <link rel="stylesheet" href="./css/responsive.css">
        <link rel="stylesheet" href="./css/custom.css">
        <link rel="stylesheet" href="./css/custom.css">
        <link rel="stylesheet" href="./css/tagify.css">
        <link rel="stylesheet" href="./toastr.css">
        <script src="./js/jquery-3.3.1.min.js"></script>
    </head>
    <body class="gray-bg">
        <section class="menu-area bg-white">
    <div class="container-xl">
        <nav class="navbar navbar-expand-lg bg-white">

            <ul class="mobile-header-buttons">
                <li><a class="mobile-nav-trigger" href="#mobile-primary-nav">http://localhost/menu<span></span></a></li>
                <li><a class="mobile-search-trigger" href="#mobile-search">http://localhost/search<span></span></a></li>
            </ul>

            <a href="Home" class="navbar-brand" href="#"><img src="./img/logo-dark.png" alt="" height="35"></a>

            <div class="main-nav-wrap">
                <div class="mobile-overlay"></div>
                <style type="text/css">
                    @media only screen and (max-width: 767px) {
                        .category.corner-triangle.top-left.pb-0.is-hidden{
                            display: none !important;
                        }
                        .sub-category.is-hidden{
                            display: none !important;
                        }
                    }
                </style>

                
            </div>

            <form class="inline-form me-auto" action="http://localhost/home/search" method="get">
                <div class="input-group search-box mobile-search">
                    <input type="text" name = 'query' class="form-control" placeholder="Search for courses">
                    <div class="input-group-append">
                        <button class="btn" type="submit"><i class="fas fa-search"></i></button>
                    </div>
                </div>
            </form>
               
            <c:if test="${sessionScope.acc.role == 1}">
                <div class="instructor-box menu-icon-box ms-auto">
                    <div class="icon">
                        <a href="Dashboard" style="border: 1px solid transparent; margin: 0px; font-size: 14px; width: max-content; border-radius: 5px; max-height: 40px; line-height: 40px; padding: 0px 10px;">Administrator</a>
                    </div>
                </div>
            </c:if>
            
            <c:if test="${sessionScope.acc != null}">
                <div class="instructor-box menu-icon-box">
                        <div class="icon">
                            <a href="MyCourse?id=${sessionScope.acc.id}" style="border: 1px solid transparent; margin: 0px;     padding: 0px 10px; font-size: 14px; width: max-content; border-radius: 5px; height: 40px; line-height: 40px;">My courses</a>
                        </div>
                    </div>
            </c:if>
                
            <div class="cart-box menu-icon-box" id = "cart_items">
                <div class="icon">
                    <a href="Cart"><i class="fas fa-shopping-cart"></i></a>
                    <span class="number">0</span>
                </div>

                <!-- Cart Dropdown goes here -->
                <div class="dropdown course-list-dropdown corner-triangle top-right" style="display: none;"> <!-- Just remove the display none from the css to make it work -->
                    <div class="list-wrapper">
                        <div class="item-list">
                            <ul>
                            </ul>
                        </div>
                        <div class="dropdown-footer">
                            <div class="cart-total-price clearfix">
                                <span>Total:</span>
                                <div class="float-end">
                                    <span class="current-price">$0</span>
                                    <!-- <span class="original-price">$94.99</span> -->
                                </div>
                            </div>
                            <a href = "Cart">Go to cart</a>
                        </div>
                    </div>
                    
                </div>      </div>

            <span class="signin-box-move-desktop-helper"></span>
            <c:if test="${sessionScope.acc == null}">
                <div class="sign-in-box btn-group">
                    <a href="Login" class="btn btn-sign-in">Log in</a>
                    <a href="Register" class="btn btn-sign-up">Sign up</a>
                </div> <!--  sign-in-box end -->
            </c:if>

            <c:if test="${sessionScope.acc != null}">
                
                <div class="sign-in-box btn-group">
                   Hello! ${sessionScope.acc.last_name}
                   <input type="hidden" id="uid" name="uid" value="${sessionScope.acc.id}">
                    <a href="Logout" class="btn btn-sign-up">Logout</a>
                </div> <!--  sign-in-box end -->
            </c:if>
        </nav>
    </div>
</section>
        <section class="course-header-area">
            <div class="container">
                <div class="row align-items-end">
                    <div class="col-lg-8">
                        <div class="course-header-wrap">
                            <h1 class="title">${course.title}</h1>
                            <p class="subtitle">${course.short_description}</p>
                            <div class="rating-row">
                                <span class="course-badge best-seller">Beginner</span>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <span class="d-inline-block average-rating">0</span><span>(0 Ratings)</span>
                                <span class="enrolled-num">
                                    0 Students enrolled            </span>
                                <span class="comment"><i class="fas fa-comment"></i>English</span>
                            </div>
                            <div class="created-row">
                                <span class="created-by">
                                    Created by                              <a class="text-14px fw-600 text-decoration-none" href="http://localhost/home/instructor_page/1">Tien Nguyen</a>
                                </span>
                                <br>
                                <span class="last-updated-date d-inline-block mt-3">Last updated Wed, 15-Jun-2022</span>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">

                    </div>
                </div>
            </div>
        </section>

        <section class="course-content-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 order-last order-lg-first radius-10 mt-4 bg-white p-30-40">

                        <div class="description-box view-more-parent">
                            <div class="description-title">Course overview</div>
                            <div class="description-content-wrap">  
                                <div class="description-content">
                                    <p>${course.description}</p>            
                                </div>
                            </div>
                        </div>

                        <h4 class="py-3">Section</h4>
                        <div class="what-you-get-box">
                            <c:forEach items="${ListS}" var="o">
                                <ul class="what-you-get__items">
                                    <li> Section ${o.section} : ${o.title}?</li>
                                </ul>
                            </c:forEach>
                       
                        </div>
                        
                        
                   

                    

                        
                    </div>

                    <div class="col-lg-4 order-first order-lg-last">
                        <div class="course-sidebar natural">
                            <div class="preview-video-box">
                                <a data-bs-toggle="modal" data-bs-target="#CoursePreviewModal">
                                    <img src="${course.thumbnail}" alt="" class="w-100">
                                    <span class="preview-text">Preview this course</span>
                                    <span class="play-btn"></span>
                                </a>
                            </div>
                            <div class="course-sidebar-text-box">
                                <div class="price text-center">
                                    <span class="current-price"><span class="current-price">$10</span></span>
                                    <input type="hidden" id="total_price_of_checking_out" value="$10">
                                </div>



                                <div class="buy-btns">
                                    <button class="btn btn-buy-now" type="button"><a href="AddCart?id=${course.id}">Add to cart</a> </button>
                                </div>


                                <div class="includes">
                                    <div class="title"><b>Includes:</b></div>
                                    <ul>
                                        
                                        <li><i class="far fa-file"></i>0 Lessons</li>
                                        <li><i class="fas fa-mobile-alt"></i>Access on mobile and tv</li>
                                        <li><i class="far fa-compass"></i>Full lifetime access</li>
                                       
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Modal -->
        <div class="modal fade" id="CoursePreviewModal" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content course-preview-modal">
                    <div class="modal-header">
                        <h5 class="modal-title"><span>Course preview:</span>300 bài code thiếu nhi</h5>
                        <button type="button" class="close" data-bs-dismiss="modal" onclick="pausePreview()" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="course-preview-video-wrap">
                            <div class="embed-responsive embed-responsive-16by9">
                                <!------------- PLYR.IO ------------>
                                <link rel="stylesheet" href="./js/plyr.css">
                                <video poster="./img/course_thumbnail_default_1.jpg" id="player" playsinline controls>
                                    <h4></h4>
                                </video>

                                <style media="screen">
                                    .plyr__video-wrapper {
                                        height: 450px;
                                    }
                                </style>

                                <script src="./js/plyr/plyr.js"></script>
                                <script>
                            const player = new Plyr('#player');
                                </script>
                                <!------------- PLYR.IO ------------>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->

        <style media="screen">
            .embed-responsive-16by9::before {
                padding-top: 0px;
            }
        </style>
        <script type="text/javascript">
            function handleCartItems(elem) {
                url1 = 'http://localhost/home/handleCartItems';
                url2 = 'http://localhost/home/refreshWishList';
                $.ajax({
                    url: url1,
                    type: 'POST',
                    data: {
                        course_id: elem.id
                    },
                    success: function (response) {
                        $('#cart_items').html(response);
                        if ($(elem).hasClass('active')) {
                            $(elem).removeClass('active')
                            $(elem).text("Add to cart");
                        } else {
                            $(elem).addClass('active');
                            $(elem).addClass('active');
                            $(elem).text("Added to cart");
                        }
                        $.ajax({
                            url: url2,
                            type: 'POST',
                            success: function (response) {
                                $('#wishlist_items').html(response);
                            }
                        });
                    }
                });
            }

            function handleBuyNow(elem) {

                url1 = 'http://localhost/home/handleCartItemForBuyNowButton';
                url2 = 'http://localhost/home/refreshWishList';
                urlToRedirect = 'http://localhost/home/shopping_cart';
                var explodedArray = elem.id.split("_");
                var course_id = explodedArray[1];

                $.ajax({
                    url: url1,
                    type: 'POST',
                    data: {
                        course_id: course_id
                    },
                    success: function (response) {
                        $('#cart_items').html(response);
                        $.ajax({
                            url: url2,
                            type: 'POST',
                            success: function (response) {
                                $('#wishlist_items').html(response);
                                toastr.success('Please wait....');
                                setTimeout(
                                        function () {
                                            window.location.replace(urlToRedirect);
                                        }, 1000);
                            }
                        });
                    }
                });
            }

            function handleEnrolledButton() {
                $.ajax({
                    url: 'http://localhost/home/isLoggedIn?url_history=aHR0cDovL2xvY2FsaG9zdC9ob21lL2NvdXJzZS8zMDAtYiVDMyVBMGktY29kZS10aGklRTElQkElQkZ1LW5oaS8x',
                    success: function (response) {
                        if (!response) {
                            window.location.replace("http://localhost/login");
                        }
                    }
                });
            }

            function handleAddToWishlist(elem) {
                $.ajax({
                    url: 'http://localhost/home/isLoggedIn?url_history=aHR0cDovL2xvY2FsaG9zdC9ob21lL2NvdXJzZS8zMDAtYiVDMyVBMGktY29kZS10aGklRTElQkElQkZ1LW5oaS8x',
                    success: function (response) {
                        if (!response) {
                            window.location.replace("http://localhost/login");
                        } else {
                            $.ajax({
                                url: 'http://localhost/home/handleWishList',
                                type: 'POST',
                                data: {
                                    course_id: elem.id
                                },
                                success: function (response) {
                                    if ($(elem).hasClass('active')) {
                                        $(elem).removeClass('active');
                                        $(elem).text("Add to wishlist");
                                    } else {
                                        $(elem).addClass('active');
                                        $(elem).text("Added to wishlist");
                                    }
                                    $('#wishlist_items').html(response);
                                }
                            });
                        }
                    }
                });
            }

            function pausePreview() {
                player.pause();
            }

            $('.course-compare').click(function (e) {
                e.preventDefault()
                var redirect_to = $(this).attr('redirect_to');
                window.location.replace(redirect_to);
            });

            function go_course_playing_page(course_id, lesson_id) {
                var course_playing_url = "http://localhost/home/lesson/300-bài-code-thiếu-nhi/" + course_id + '/' + lesson_id;

                $.ajax({
                    url: 'http://localhost/home/go_course_playing_page/' + course_id,
                    type: 'POST',
                    success: function (response) {
                        if (response == 1) {
                            window.location.replace(course_playing_url);
                        }
                    }
                });
            }
        </script><footer class="footer-area d-print-none bg-gray mt-5 pt-5">
            <div class="container-xl">
                <div class="row mb-3">
                    <div class="col-6 col-sm-6 col-md-3">
                        <h5 class="text-muted mb-3">Top categories</h5>
                        <ul class="list-unstyled text-small">
                            <li class="mb-2">
                                <a class="link-secondary footer-hover-link" href="http://localhost/home/courses?category=react">
                                    React                  <!-- <span class="fw-700 text-end">()</span> -->
                                </a>
                            </li>
                            <li class="mb-2">
                                <a class="link-secondary footer-hover-link" href="http://localhost/home/courses?category=linh-tịnh">
                                    Linh tịnh                  <!-- <span class="fw-700 text-end">()</span> -->
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-6 col-sm-6 col-md-3">
                        <h5 class="text-muted mb-3">Useful links</h5>
                        <ul class="list-unstyled text-small">
                            <li class="mb-2"><a class="link-secondary footer-hover-link" href="./blog.html">Blog</a></li>
                            <li class="mb-2"><a class="link-secondary footer-hover-link" href="./user_view_all_course.html">All courses</a></li>
                            <li class="mb-2"><a class="link-secondary footer-hover-link" href="./signup.html">Sign up</a></li>
                            <li class="mb-2"><a class="link-secondary footer-hover-link" href="./signin.html">Login</a></li>
                        </ul>
                    </div>
                    <div class="col-6 col-sm-6 col-md-3">
                        <h5 class="text-muted mb-3">Help</h5>
                        <ul class="list-unstyled text-small">
                            <li class="mb-2"><a class="link-secondary footer-hover-link" href="http://localhost/home/about_us">About us</a></li>
                            <li class="mb-2"><a class="link-secondary footer-hover-link" href="http://localhost/home/privacy_policy">Privacy policy</a></li>
                            <li class="mb-2"><a class="link-secondary footer-hover-link" href="http://localhost/home/terms_and_condition">Terms and condition</a></li>
                            <li class="mb-2"><a class="link-secondary footer-hover-link" href="http://localhost/home/refund_policy">Refund policy</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-12 order-sm-first">
                        <img src="./img/logo-dark.png" width="130">
                        <span class="d-block mb-1 mt-2 fw-600" style="font-size: 14.5px; line-height: 28px">Study any topic, anytime. explore thousands of courses for the lowest price ever!</span>

                        <ul class="footer-social-link">
                            <li class="mb-1">
                                <a href="https://facebook.com"><i class="fab fa-facebook-f"></i></a>
                            </li>
                            <li class="mb-1">
                                <a href="https://twitter.com"><i class="fab fa-twitter"></i></a>
                            </li>
                            <li class="mb-1">
                                <a href="https://linkedin.com"><i class="fab fa-linkedin"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <section class="border-top">
                <div class="container-xl">
                    <div class="row mt-3 py-1">
                        <div class="col-6 col-sm-6 col-md-3 text-muted text-13px">
                            &copy; 2021 NewSite, All rights reserved        </div>

                        <div class="col-6 col-sm-6 col-md-3 d-none d-md-block"></div>
                        <div class="col-6 col-sm-6 col-md-3 d-none d-md-block"></div>
                        <div class="col-6 col-sm-6 col-md-3 text-center text-md-start">
                            <select class="language_selector" onchange="switch_language(this.value)">
                                <option value="english" selected>English</option>
                            </select>
                        </div>
                    </div>
                </div>
            </section>
        </footer>

        <script type="text/javascript">
            function switch_language(language) {
                $.ajax({
                    url: 'http://localhost/home/site_language',
                    type: 'post',
                    data: {language: language},
                    success: function (response) {
                        setTimeout(function () {
                            location.reload(); }, 500);
                    }
                });
            }
        </script>



        <!-- PAYMENT MODAL -->
        <!-- Modal -->

        <!-- Modal -->
        <div class="modal fade multi-step" id="EditRatingModal" tabindex="-1" role="dialog" aria-hidden="true" reset-on-close="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content edit-rating-modal">
                    <div class="modal-header">
                        <h5 class="modal-title step-1" data-step="1">Step 1</h5>
                        <h5 class="modal-title step-2" data-step="2">Step 2</h5>
                        <h5 class="m-progress-stats modal-title">
                            &nbsp;of&nbsp;<span class="m-progress-total"></span>
                        </h5>

                        <button type="button" class="close" data-bs-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="m-progress-bar-wrapper">
                        <div class="m-progress-bar">
                        </div>
                    </div>
                    <div class="modal-body step step-1">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="modal-rating-box">
                                        <h4 class="rating-title">How would you rate this course overall?</h4>
                                        <fieldset class="your-rating">

                                            <input type="radio" id="star5" name="rating" value="5" />
                                            <label class = "full" for="star5"></label>

                                            <!-- <input type="radio" id="star4half" name="rating" value="4 and a half" />
                                            <label class="half" for="star4half"></label> -->

                                            <input type="radio" id="star4" name="rating" value="4" />
                                            <label class = "full" for="star4"></label>

                                            <!-- <input type="radio" id="star3half" name="rating" value="3 and a half" />
                                            <label class="half" for="star3half"></label> -->

                                            <input type="radio" id="star3" name="rating" value="3" />
                                            <label class = "full" for="star3"></label>

                                            <!-- <input type="radio" id="star2half" name="rating" value="2 and a half" />
                                            <label class="half" for="star2half"></label> -->

                                            <input type="radio" id="star2" name="rating" value="2" />
                                            <label class = "full" for="star2"></label>

                                            <!-- <input type="radio" id="star1half" name="rating" value="1 and a half" />
                                            <label class="half" for="star1half"></label> -->

                                            <input type="radio" id="star1" name="rating" value="1" />
                                            <label class = "full" for="star1"></label>

                                            <!-- <input type="radio" id="starhalf" name="rating" value="half" />
                                            <label class="half" for="starhalf"></label> -->

                                        </fieldset>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="modal-course-preview-box">
                                        <div class="card">
                                            <img class="card-img-top img-fluid" id = "course_thumbnail_1" alt="">
                                            <div class="card-body">
                                                <h5 class="card-title" class = "course_title_for_rating" id = "course_title_1"></h5>
                                                <p class="card-text" id = "instructor_details">

                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-body step step-2">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="modal-rating-comment-box">
                                        <h4 class="rating-title">Write a public review</h4>
                                        <textarea id = "review_of_a_course" name = "review_of_a_course" placeholder="Describe your experience what you got out of the course and other helpful highlights. What did the instructor do well and what could use some improvement?" maxlength="65000" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="modal-course-preview-box">
                                        <div class="card">
                                            <img class="card-img-top img-fluid" id = "course_thumbnail_2" alt="">
                                            <div class="card-body">
                                                <h5 class="card-title" class = "course_title_for_rating" id = "course_title_2"></h5>
                                                <p class="card-text">
                                                    -
                                                    Tien Nguyen                    </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="course_id" id = "course_id_for_rating" value="">
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary next step step-1" data-step="1" onclick="sendEvent(2)">Next</button>
                        <button type="button" class="btn btn-primary previous step step-2 mr-auto" data-step="2" onclick="sendEvent(1)">Previous</button>
                        <button type="button" class="btn btn-primary publish step step-2" onclick="publishRating($('#course_id_for_rating').val())" id = "">Publish</button>
                    </div>
                </div>
            </div>
        </div><!-- Modal -->


        <script src="./js/modernizr-3.5.0.min.js"></script>
        <script src="./js/jquery-3.2.1.min.js"></script>
        <script src="./js/popper.min.js"></script>
        <script src="./js/bootstrap.min.js"></script>



        <script src="./js/main.js"></script>
        <script src="./toastr.min.js"></script>
        <script src="./jquery.form.min.js"></script>
        <script src="./js/jQuery.tagify.js"></script>

        <!-- SHOW TOASTR NOTIFIVATION -->


        <script type="text/javascript">
                            $(function () {
                                $('[data-bs-toggle="tooltip"]').tooltip()
                            });
                            if ($('.tagify').height()) {
                                $('.tagify').tagify();
                            }
        </script><script type="text/javascript">
            function showAjaxModal(url)
            {
                // SHOWING AJAX PRELOADER IMAGE
                jQuery('#modal_ajax .modal-body').html('<div class="w-100 text-center pt-5"><img class="mt-5 mb-5" width="80px" src="http://localhost/assets/global/gif/page-loader-2.gif"></div>');

                // LOADING THE AJAX MODAL
                jQuery('#modal_ajax').modal('show', {backdrop: 'true'});

                // SHOW AJAX RESPONSE ON REQUEST SUCCESS
                $.ajax({
                    url: url,
                    success: function (response)
                    {
                        jQuery('#modal_ajax .modal-body').html(response);
                    }
                });
            }

            function lesson_preview(url, title) {
                // SHOWING AJAX PRELOADER IMAGE
                jQuery('.lesson_preview_header').html(title);
                jQuery('#lesson_preview .modal-body').html('<div class="w-100 text-center pt-5"><img class="mt-5 mb-5" width="80px" src="http://localhost/assets/global/gif/page-loader-2.gif"></div>');

                // LOADING THE AJAX MODAL
                jQuery('#lesson_preview').modal('show', {backdrop: 'true'});

                // SHOW AJAX RESPONSE ON REQUEST SUCCESS
                $.ajax({
                    url: url,
                    success: function (response)
                    {
                        jQuery('#lesson_preview .modal-body').html(response);
                    }
                });
            }
        </script>

        <!-- (Ajax Modal)-->
        <div class="modal fade" id="modal_ajax">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-bs-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body" style="overflow:auto;">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-bs-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="lesson_preview" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content course-preview-modal">
                    <div class="modal-header">
                        <h5 class="lesson_preview_header"></h5>
                        <button type="button" class="close" data-bs-dismiss="modal" onclick="window.location.reload()">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                    </div>
                </div>
            </div>
        </div>




        <script type="text/javascript">
            function confirm_modal(delete_url)
            {
                jQuery('#modal-4').modal('show', {backdrop: 'static'});
                document.getElementById('delete_link').setAttribute('href', delete_url);
            }
        </script>

        <!-- (Normal Modal)-->
        <div class="modal fade" id="modal-4">
            <div class="modal-dialog">
                <div class="modal-content" style="margin-top:100px;">

                    <div class="modal-header">
                        <h4 class="modal-title text-center">Are you sure ?</h4>
                        <button type="button" class="btn btn-outline-secondary px-1 py-0" data-bs-dismiss="modal" aria-hidden="true"><i class="fas fa-times-circle"></i></button>
                    </div>


                    <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
                        <a href="#" class="btn btn-danger btn-yes" id="delete_link">Yes</a>
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>


        <script type="text/javascript">
            function async_modal() {
                const asyncModal = new Promise(function (resolve, reject) {
                    $('#modal-4').modal('show');
                    $('#modal-4 .btn-yes').click(function () {
                        resolve(true);
                    });
                    $('#modal-4 .btn-cancel').click(function () {
                        resolve(false);
                    });
                });
                return asyncModal;
            }
        </script>
        <script type="text/javascript">
            function toggleRatingView(course_id) {
                $('#course_info_view_' + course_id).toggle();
                $('#course_rating_view_' + course_id).toggle();
                $('#edit_rating_btn_' + course_id).toggle();
                $('#cancel_rating_btn_' + course_id).toggle();
            }

            function publishRating(course_id) {
                var review = $('#review_of_a_course_' + course_id).val();
                var starRating = 0;
                starRating = $('#star_rating_of_course_' + course_id).val();
                if (starRating > 0) {
                    $.ajax({
                        type: 'POST',
                        url: 'http://localhost/home/rate_course',
                        data: {course_id: course_id, review: review, starRating: starRating},
                        success: function (response) {
                            location.reload();
                        }
                    });
                } else {

                }
            }

            function isTouchDevice() {
                return (('ontouchstart' in window) ||
                        (navigator.maxTouchPoints > 0) ||
                        (navigator.msMaxTouchPoints > 0));
            }

            function viewMore(element, visibility) {
                if (visibility == "hide") {
                    $(element).parent(".view-more-parent").addClass("expanded");
                    $(element).remove();
                } else if ($(element).hasClass("view-more")) {
                    $(element).parent(".view-more-parent").addClass("expanded has-hide");
                    $(element)
                            .removeClass("view-more")
                            .addClass("view-less")
                            .html("- View less");
                } else if ($(element).hasClass("view-less")) {
                    $(element).parent(".view-more-parent").removeClass("expanded has-hide");
                    $(element)
                            .removeClass("view-less")
                            .addClass("view-more")
                            .html("+ View more");
                }
            }


        </script>
    </body>
</html>
