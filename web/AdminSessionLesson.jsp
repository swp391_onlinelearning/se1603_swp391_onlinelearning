<%-- 
    Document   : AdminSessionLesson
    Created on : Jul 13, 2022, 11:57:52 PM
    Author     : genni
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<!DOCTYPE html>
<html>
    <head>
        <title>Edit course | Academy Learning Club</title>
        <!-- all the meta tags -->
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- all the css files -->
        <link rel="shortcut icon" href="./img/favicon.png">
        <!-- third party css -->
        <link href="./css/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
        <link href="./css/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
        <link href="./css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="./css/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
        <link href="./css/select.bootstrap4.css" rel="stylesheet" type="text/css" />
        <link href="./css/summernote-bs4.css" rel="stylesheet" type="text/css" />
        <link href="./css/fullcalendar.min.css" rel="stylesheet" type="text/css" />
        <link href="./css/dropzone.css" rel="stylesheet" type="text/css" />
        <!-- third party css end -->


        <!--Drips icons-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dripicons/2.0.0/webfont.min.css" integrity="sha512-pi7KSLdGMxSE62WWJ62B1R5/H7WNnIsj2f51MikplRt31K0uCZ1lfPSw/0Jb1flSz6Ed2YLSlox6Uulf7CaFiA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dripicons/2.0.0/webfont.min.css" integrity="sha512-pi7KSLdGMxSE62WWJ62B1R5/H7WNnIsj2f51MikplRt31K0uCZ1lfPSw/0Jb1flSz6Ed2YLSlox6Uulf7CaFiA==" crossorigin="anonymous" referrerpolicy="no-referrer" />

        <!--Material Design Icon-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/2.0.46/css/materialdesignicons.min.css" integrity="sha512-HmBTsbqKSDy0wIk8SGSCj68xUg8b22mGtXx8cXF64qcmnQnJepz6Aq37X43gF/WhbvqPcx68GoiaWu8wE8/y4g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/2.0.46/css/materialdesignicons.css" integrity="sha512-OSd9YjMibKZ0CN1njc/7H/IK8zmRevGk+n/Z+jAyClgFZ+sOvQKzmeJKBKW313Ln+630pAlcCMjX1dzTDGe4+w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/3.9.97/css/materialdesignicons.min.css" integrity="sha512-PhzMnIL3KJonoPVmEDTBYz7rxxne7E3Lc5NekqcT3nxSLRTN2h2bJKStWoy0RfS31Jd6nBguC32sL6iK1k2OXw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/3.9.97/css/materialdesignicons.css" integrity="sha512-rUWddgvLngNoYen5mwmKwYV4MR6YVFeOrgj4V3mx/0IcguSrE54SSIuPtUsYNBrLCsQxx/bBbcm3GWhXVJXhxA==" crossorigin="anonymous" referrerpolicy="no-referrer" />

        <!-- App css -->
        <link href="./css/app.min.css" rel="stylesheet" type="text/css" />
        <link href="./css/icons.min.css" rel="stylesheet" type="text/css" />

        <link href="./css/backend/main.css" rel="stylesheet" type="text/css" />

        <!-- font awesome 5 -->
        <link href="./css/backend/fontawesome-all.min.css" rel="stylesheet" type="text/css" />
        <link href="./css/fontawesome-iconpicker.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="./css/bootstrap-tagsinput.css">

        <script src="./js/jquery-3.3.1.min.js" charset="utf-8"></script>
        <script src="./js/onDomChange.js"></script></head>
    <body data-layout="detached">
      <c:if test="${sessionScope.acc == null}">
        <c:redirect url="Home"/>  
        </c:if>
         <c:if test="${sessionScope.acc.role != 1}">
            <c:redirect url="Home"/>  
        </c:if>
        <!-- Topbar Start -->
        <div class="navbar-custom topnav-navbar topnav-navbar-dark">
            <div class="container-fluid">
                <!-- LOGO -->
                <a href="http://localhost/Admin" class="topnav-logo" style = "min-width: unset;">
                    <span class="topnav-logo-lg">
                        <img src="./img/logo-light.png" alt="" height="40">
                    </span>
                    <span class="topnav-logo-sm">
                        <img src="./img/logo-light-sm.png" alt="" height="40">
                    </span>
                </a>

                <ul class="list-unstyled topbar-right-menu float-right mb-0">
                    <li class="dropdown notification-list">
                        <a class="nav-link dropdown-toggle arrow-none" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                            <i class="dripicons-view-apps noti-icon"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated dropdown-lg p-0 mt-5 border-top-0" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-278px, 70px, 0px);">

                            <div class="rounded-top py-3 border-bottom bg-primary">
                                <h4 class="text-center text-white">Quick actions</h4>
                            </div>

                            <div class="row row-paddingless" style="padding-left: 15px; padding-right: 15px;">
                                <!--begin:Item-->
                                <div class="col-6 p-0 border-bottom border-right">
                                    <a href="AdminAddCourse" class="d-block text-center py-3 bg-hover-light">
                                        <i class="dripicons-archive text-20"></i>
                                        <span class="w-100 d-block text-muted">Add course</span>
                                    </a>
                                </div>



                                <div class="col-6 p-0 border-right">
                                    <a href="AdminAddUser" class="d-block text-center py-3 bg-hover-light" >
                                        <i class="dripicons-user text-20"></i>
                                        <span class="w-100 d-block text-muted">Add student</span>
                                    </a>
                                </div>


                            </div>
                        </div>
                    </li>
                    <li class="dropdown notification-list">
                        <a class="nav-link dropdown-toggle nav-user arrow-none mr-0" data-toggle="dropdown" id="topbar-userdrop"
                           href="#" role="button" aria-haspopup="true" aria-expanded="false">
                            <span class="account-user-avatar">
                                <img src="./img/placeholder.png" alt="user-image" class="rounded-circle">
                            </span>
                            <span  style="color: #fff;">
                                <span class="account-user-name">${sessionScope.acc.last_name}</span>
                                <span class="account-position">
                                    Admin                    </span>
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated topbar-dropdown-menu profile-dropdown"
                             aria-labelledby="topbar-userdrop">
                            <!-- item-->
                            <div class=" dropdown-header noti-title">
                                <h6 class="text-overflow m-0">Welcome !</h6>
                            </div>

                            <!-- Logout-->
                            <a href="Logout" class="dropdown-item notify-item">
                                <i class="mdi mdi-logout mr-1"></i>
                                <span>Logout</span>
                            </a>

                        </div>
                    </li>
                </ul>
                <a class="button-menu-mobile disable-btn">
                    <div class="lines">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </a>
                <div class="visit_website">
                    <h4 style="color: #fff; float: left;" class="d-none d-md-inline-block"> NewSite</h4>
                    <a href="/Home" target="" class="btn btn-outline-light ml-3 d-none d-md-inline-block">Visit website</a>
                </div>
            </div>
        </div>
        <!-- end Topbar -->    <div class="container-fluid">
            <div class="wrapper">
                <!-- BEGIN CONTENT -->
                <!-- SIDEBAR -->
                <!-- ========== Left Sidebar Start ========== -->
                <div class="left-side-menu left-side-menu-detached">
                    <div class="leftbar-user">
                        <a href="javascript: void(0);">
                            <img src="./img/placeholder.png" alt="user-image" height="42" class="rounded-circle shadow-sm">
                            <span class="leftbar-user-name">${sessionScope.acc.last_name}</span>
                        </a>
                    </div>

                    <!--- Sidemenu -->
                    <ul class="metismenu side-nav side-nav-light">

                        <li class="side-nav-title side-nav-item">Navigation</li>

                        <li class="side-nav-item ">
                            <a href="Dashboard" class="side-nav-link">
                                <i class="dripicons-view-apps"></i>
                                <span>Dashboard</span>
                            </a>
                        </li>

                        <li class="side-nav-item ">
                            <a href="javascript: void(0);" class="side-nav-link ">
                                <i class="dripicons-archive"></i>
                                <span> Courses </span>
                                <span class="menu-arrow"></span>
                            </a>
                            <ul class="side-nav-second-level" aria-expanded="false">
                                <li class="">
                                    <a href="/AdminCourse">Manage courses</a>
                                </li>

                                <li class="">
                                    <a href="/AdminAddCourse">Add new course</a>
                                </li>

                      


                            </ul>
                        </li>


                        <li class="side-nav-item ">

                        </li>

                        <li class="side-nav-item">

                            <ul class="side-nav-second-level" aria-expanded="false">
                                <li class=""> <a href="http://localhost/admin/admin_revenue">Admin revenue</a> </li>
                                <li class="">
                                    <a href="http://localhost/admin/instructor_revenue">
                                        Instructor revenue							</a>
                                </li>
                            </ul>
                        </li>

                        <li class="side-nav-item ">
                            <a href="javascript: void(0);" class="side-nav-link ">
                                <i class="dripicons-box"></i>
                                <span> Users </span>
                                <span class="menu-arrow"></span>
                            </a>
                            <ul class="side-nav-second-level" aria-expanded="false">
                                <li class="side-nav-item ">
                                    <a href="/AdminUser">View List</a>
                                </li>



                                <li class="side-nav-item ">
                                    <a href="/AdminAddUser">Add new</a>
                                </li>
                            </ul>
                        </li>





                        <li class="side-nav-item">
                            <a href="javascript: void(0);" class="side-nav-link  active ">
                                <i class="dripicons-blog"></i>
                                <span> Blog </span>
                                <span class="menu-arrow"></span>
                            </a>
                            <ul class="side-nav-second-level" aria-expanded="false">
                                <li class="">
                                    <a href="/AdminBlogList">All blogs</a>
                                </li>
                                <li class="">
                                    <a href="/AddBlog">Add blogs</a>
                                </li>
                                <li class="">
                                    <a href="/AdminBlogCategory">Blog category</a>
                                </li>
                                 <li class="">
                                    <a href="/AdminBlogCategortAdd">Add category</a>
                                </li>   

                            </ul>
                        </li>


                    </ul>
                </div>         <!-- PAGE CONTAINER-->
                <div class="content-page">
                    <div class="content">
                        <!-- BEGIN PlACE PAGE CONTENT HERE -->
                        <div class="row ">
                            <div class="col-xl-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="page-title"> <i class="mdi mdi-apple-keyboard-command title_icon"></i> Course: ${course.title}</h4>
                                    </div> <!-- end card body-->
                                </div> <!-- end card -->
                            </div><!-- end col-->
                        </div>

                        <div class="row">
                            <div class="col-xl-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <h4 class="header-title my-1">Course manager</h4>
                                            </div>
                                            <div class="col-md-6">
                                                
                                                <a href="./AdminCourse" class="alignToTitle btn btn-outline-secondary btn-rounded btn-sm my-1"> <i class=" mdi mdi-keyboard-backspace"></i> Back to course list</a>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xl-12">
                                                <form class="required-form" action="http://localhost/admin/course_actions/edit/1" method="post" enctype="multipart/form-data" autocomplete="off">
                                                    <div id="basicwizard">
                                                        <ul class="nav nav-pills nav-justified form-wizard-header">
                                                            <li class="nav-item">
                                                                <a href="#curriculum" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                                                    <i class="mdi mdi-account-circle"></i>
                                                                    <span class="d-none d-sm-inline">Curriculum</span>
                                                                </a>
                                                            </li>






                                                            <li class="w-100 bg-white pb-3">
                                                                <!--ajax page loader-->
                                                                <div class="ajax_loader w-100">
                                                                    <div class="ajax_loaderBar"></div>
                                                                </div>
                                                                <!--end ajax page loader-->
                                                            </li>
                                                        </ul>

                                                        <div class="tab-content b-0 mb-0">
                                                            <div class="tab-pane" id="curriculum">
                                                                <div class="row justify-content-center">
                                                                    <div class="col-xl-12 mb-4 text-center mt-3">
                                                                        <a href="AddSection?courseid=${course.id}" class="btn btn-outline-primary btn-rounded btn-sm ml-1"><i class="mdi mdi-plus"></i> Add section</a>
                                                                        <a href="AddLession?id=${course.id}" class="btn btn-outline-primary btn-rounded btn-sm ml-1"><i class="mdi mdi-plus"></i> Add lesson</a>
                                                                    </div>

                                                                    <div class="col-xl-8">
                                                                        <div class="row">
                                                                            <c:forEach items="${ListS}" var="o">
                                                                                <div class="col-xl-12">
                                                                                    <div class="card bg-light text-seconday on-hover-action mb-5" id="section-${o.id}">
                                                                                        <div class="card-body">
                                                                                            <h5 class="card-title" style="min-height: 45px;"><span class="font-weight-light">Section ${o.section}</span>: ${o.title}                            <div class="row justify-content-center alignToTitle float-right display-none" id="widgets-of-section-${o.id}" style="display: none;">
                                                                                                    <button type="button" class="btn btn-outline-secondary btn-rounded btn-sm ml-1" name="button" onclick="showAjaxModal('http://localhost/modal/popup/section_edit/1/1', 'Update section')"><i class="mdi mdi-pencil-outline"></i> Edit section</button>
                                                                                                    <a href="DeleteSection?id=${o.id}&course=${course.id}"><button type="button" class="btn btn-outline-secondary btn-rounded btn-sm ml-1" name="button"><i class="mdi mdi-window-close"></i> Delete section</button></a>
                                                                                                </div>
                                                                                            </h5>
                                                                                            <div class="clearfix"></div>
                                                                                            <c:forEach items="${ListL}" var="c">
                                                                                                <c:if test="${o.section == c.sectionID}">
                                                                                                    <div class="col-md-12">
                                                                                                        <!-- Portlet card -->
                                                                                                        <div class="card text-secondary on-hover-action mb-2" id="lesson-${c.id}">
                                                                                                            <div class="card-body thinner-card-body">
                                                                                                                <div class="card-widgets display-none" id="widgets-of-lesson-${c.id}" style="display: none;">
                                                                                                                    <a href="UpdateLession?id=${course.id}&lession=${c.id}"><i class="mdi mdi-pencil-outline"></i></a>
                                                                                                                    <a href="DeleteLession?id=${c.id}&course=${course.id}"><i class="mdi mdi-window-close"></i></a>
                                                                                                                </div>
                                                                                                                <h5 class="card-title mb-0">
                                                                                                                    <span class="font-weight-light">
                                                                                                                        <img src="http://localhost/assets/backend/lesson_icon/video.png" alt="" height="16">
                                                                                                                        Lesson                                        </span>: ${c.title}                                 </h5>
                                                                                                            </div>
                                                                                                        </div> <!-- end card-->
                                                                                                    </div>
                                                                                                </c:if>
                                                                                            </c:forEach>
                                                                                            
                                                                                          
                                                                                        </div> <!-- end card-body-->
                                                                                    </div> <!-- end card-->
                                                                                </div>
                                                                            </c:forEach>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <!-- LIVE CLASS CODE BASE -->

                                                            <!-- Jitsi live class CODE BASE -->
                                                            <!-- LIVE CLASS CODE BASE -->

                                                            <!-- ASSIGNMENT CODE BASE -->

                                                            <!-- NOTICEBOARD CODE BASE -->
                                                            <!-- NOTICEBOARD CODE BASE -->

                                                            <!-- COURSE ANALYTICS CODE BASE -->
                                                            <!-- COURSE ANALYTICS CODE BASE -->

                                                            <div class="tab-pane" id="basic">
                                                                <div class="row justify-content-center">
                                                                    <div class="col-xl-8">

                                                                        <div class="form-group row mb-3">
                                                                            <label class="col-md-2 col-form-label" for="existing_instructors">Instructor of this course</label>
                                                                            <div class="col-md-10">
                                                                                <div>
                                                                                    <img class="rounded-circle" src="./img/placeholder.png" height="30px" alt="">
                                                                                    <span style="font-weight: 700; font-size: 15px; vertical-align: sub; margin-left: 6px;">
                                                                                        Tien Nguyen                                                                </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group row mb-3">
                                                                            <label class="col-md-2 col-form-label" for="new_instructor">Add new instructor</label>
                                                                            <div class="col-md-10">
                                                                                <select class="select2 form-control select2-multiple" data-toggle="select2" multiple="multiple" data-placeholder="Choose ..." name="new_instructors[]">
                                                                                    <option value="1">Tien Nguyen ( nguyentien05072001@gmail.com )</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group row mb-3">
                                                                            <label class="col-md-2 col-form-label" for="course_title">Course title<span class="required">*</span></label>
                                                                            <div class="col-md-10">
                                                                                <input type="text" class="form-control" id="course_title" name="title" placeholder="Enter course title" value="300 bài code thiếu nhi" required>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group row mb-3">
                                                                            <label class="col-md-2 col-form-label" for="short_description">Short description</label>
                                                                            <div class="col-md-10">
                                                                                <textarea name="short_description" id="short_description" class="form-control">Đây là 300 bài code thiếu nhi</textarea>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group row mb-3">
                                                                            <label class="col-md-2 col-form-label" for="description">Description</label>
                                                                            <div class="col-md-10">
                                                                                <textarea name="description" id="description" class="form-control"><p>No desc</p></textarea>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group row mb-3">
                                                                            <label class="col-md-2 col-form-label" for="sub_category_id">Category<span class="required">*</span></label>
                                                                            <div class="col-md-10">
                                                                                <select class="form-control select2" data-toggle="select2" name="sub_category_id" id="sub_category_id" required>
                                                                                    <option value="">Select a category</option>
                                                                                    <optgroup label="asdasdasd">
                                                                                        <option value="2" selected>React</option>
                                                                                        <option value="3" >Linh tịnh</option>
                                                                                    </optgroup>
                                                                                </select>
                                                                                <small class="text-muted">Select sub category</small>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group row mb-3">
                                                                            <label class="col-md-2 col-form-label" for="level">Level</label>
                                                                            <div class="col-md-10">
                                                                                <select class="form-control select2" data-toggle="select2" name="level" id="level">
                                                                                    <option value="beginner" selected>Beginner</option>
                                                                                    <option value="advanced" >Advanced</option>
                                                                                    <option value="intermediate" >Intermediate                                                        </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group row mb-3">
                                                                            <label class="col-md-2 col-form-label" for="language_made_in">Language made in</label>
                                                                            <div class="col-md-10">
                                                                                <select class="form-control select2" data-toggle="select2" name="language_made_in" id="language_made_in">
                                                                                    <option value="english" selected>English</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group row mb-3">
                                                                            <label class="col-md-2 col-form-label" for="enable_drip_content">Enable drip content</label>
                                                                            <div class="col-md-10 pt-2">
                                                                                <input type="checkbox" name="enable_drip_content" value="1" id="enable_drip_content" data-switch="primary" >
                                                                                <label for="enable_drip_content" data-on-label="On" data-off-label="Off"></label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group row mb-3">
                                                                            <div class="offset-md-2 col-md-10">
                                                                                <div class="custom-control custom-checkbox">
                                                                                    <input type="checkbox" class="custom-control-input" name="is_top_course" id="is_top_course" value="1" >
                                                                                    <label class="custom-control-label" for="is_top_course">Check if this course is top course</label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div> <!-- end col -->
                                                                </div> <!-- end row -->
                                                            </div> <!-- end tab pane -->

                                                            <div class="tab-pane" id="requirements">
                                                                <div class="row justify-content-center">
                                                                    <div class="col-xl-8">
                                                                        <div class="form-group row mb-3">
                                                                            <label class="col-md-2 col-form-label" for="requirements">Requirements</label>
                                                                            <div class="col-md-10">
                                                                                <div id="requirement_area">
                                                                                    <div class="d-flex mt-2">
                                                                                        <div class="flex-grow-1 px-3">
                                                                                            <div class="form-group">
                                                                                                <input type="text" class="form-control" name="requirements[]" id="requirements" placeholder="Provide requirements" value="No requirement">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="">
                                                                                            <button type="button" class="btn btn-success btn-sm" style="" name="button" onclick="appendRequirement()"> <i class="fa fa-plus"></i> </button>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div id="blank_requirement_field">
                                                                                        <div class="d-flex mt-2">
                                                                                            <div class="flex-grow-1 px-3">
                                                                                                <div class="form-group">
                                                                                                    <input type="text" class="form-control" name="requirements[]" id="requirements" placeholder="Provide requirements">
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="">
                                                                                                <button type="button" class="btn btn-danger btn-sm" style="margin-top: 0px;" name="button" onclick="removeRequirement(this)"> <i class="fa fa-minus"></i> </button>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="tab-pane" id="outcomes">
                                                                <div class="row justify-content-center">
                                                                    <div class="col-xl-8">
                                                                        <div class="form-group row mb-3">
                                                                            <label class="col-md-2 col-form-label" for="outcomes">Outcomes</label>
                                                                            <div class="col-md-10">
                                                                                <div id="outcomes_area">
                                                                                    <div class="d-flex mt-2">
                                                                                        <div class="flex-grow-1 px-3">
                                                                                            <div class="form-group">
                                                                                                <input type="text" class="form-control" name="outcomes[]" placeholder="Provide outcomes" value="Master React">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="">
                                                                                            <button type="button" class="btn btn-success btn-sm" name="button" onclick="appendOutcome()"> <i class="fa fa-plus"></i> </button>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div id="blank_outcome_field">
                                                                                        <div class="d-flex mt-2">
                                                                                            <div class="flex-grow-1 px-3">
                                                                                                <div class="form-group">
                                                                                                    <input type="text" class="form-control" name="outcomes[]" id="outcomes" placeholder="Provide outcomes">
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="">
                                                                                                <button type="button" class="btn btn-danger btn-sm" style="margin-top: 0px;" name="button" onclick="removeOutcome(this)"> <i class="fa fa-minus"></i> </button>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="tab-pane" id="pricing">
                                                                <div class="row justify-content-center">
                                                                    <div class="col-xl-8">
                                                                        <div class="form-group row mb-3">
                                                                            <div class="offset-md-2 col-md-10">
                                                                                <div class="custom-control custom-checkbox">
                                                                                    <input type="checkbox" class="custom-control-input" name="is_free_course" id="is_free_course" value="1"  onclick="togglePriceFields(this.id)">
                                                                                    <label class="custom-control-label" for="is_free_course">Check if this is a free course</label>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="paid-course-stuffs">
                                                                            <div class="form-group row mb-3">
                                                                                <label class="col-md-2 col-form-label" for="price">Course price ($)</label>
                                                                                <div class="col-md-10">
                                                                                    <input type="number" class="form-control" id="price" name="price" min="0" placeholder="Enter course course price" value="10">
                                                                                </div>
                                                                            </div>

                                                                            <div class="form-group row mb-3">
                                                                                <div class="offset-md-2 col-md-10">
                                                                                    <div class="custom-control custom-checkbox">
                                                                                        <input type="checkbox" class="custom-control-input" name="discount_flag" id="discount_flag" value="1" >
                                                                                        <label class="custom-control-label" for="discount_flag">Check if this course has discount</label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="form-group row mb-3">
                                                                                <label class="col-md-2 col-form-label" for="discounted_price">Discounted price ($)</label>
                                                                                <div class="col-md-10">
                                                                                    <input type="number" class="form-control" name="discounted_price" id="discounted_price" onkeyup="calculateDiscountPercentage(this.value)" value="2" min="0">
                                                                                    <small class="text-muted">This course has <span id="discounted_percentage" class="text-danger">0%</span> Discount</small>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div> <!-- end col -->
                                                                </div> <!-- end row -->
                                                            </div> <!-- end tab-pane -->
                                                            <div class="tab-pane" id="media">
                                                                <div class="row justify-content-center">

                                                                    <div class="col-xl-8">
                                                                        <div class="form-group row mb-3">
                                                                            <label class="col-md-2 col-form-label" for="course_overview_provider">Course overview provider</label>
                                                                            <div class="col-md-10">
                                                                                <select class="form-control select2" data-toggle="select2" name="course_overview_provider" id="course_overview_provider">
                                                                                    <option value="youtube" selected>Youtube</option>
                                                                                    <option value="vimeo" >Vimeo</option>
                                                                                    <option value="html5" >Html5</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div> <!-- end col -->

                                                                    <div class="col-xl-8">
                                                                        <div class="form-group row mb-3">
                                                                            <label class="col-md-2 col-form-label" for="course_overview_url">Course overview url</label>
                                                                            <div class="col-md-10">
                                                                                <input type="text" class="form-control" name="course_overview_url" id="course_overview_url" placeholder="E.g: https://www.youtube.com/watch?v=oBtf8Yglw2w" value="https://www.youtube.com/watch?v=psQ9AQqMVb0">
                                                                            </div>
                                                                        </div>
                                                                    </div> <!-- end col -->

                                                                    <!-- Course media content edit file starts -->
                                                                    <div class="col-xl-8">
                                                                        <div class="form-group row mb-3">
                                                                            <label class="col-md-2 col-form-label" for="course_thumbnail_label">Course thumbnail</label>
                                                                            <div class="col-md-10">
                                                                                <div class="wrapper-image-preview" style="margin-left: -6px;">
                                                                                    <div class="box" style="width: 250px;">
                                                                                        <div class="js--image-preview" style="background-image: url(./img/course_thumbnail_default_1.jpg); background-color: #F5F5F5;"></div>
                                                                                        <div class="upload-options">
                                                                                            <label for="course_thumbnail" class="btn"> <i class="mdi mdi-camera"></i> Course thumbnail <br> <small>(600 X 600)</small> </label>
                                                                                            <input id="course_thumbnail" style="visibility:hidden;" type="file" class="image-upload" name="course_thumbnail" accept="image/*">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!-- Course media content edit file ends -->
                                                                </div> <!-- end row -->
                                                            </div>
                                                            <div class="tab-pane" id="seo">
                                                                <div class="row justify-content-center">
                                                                    <div class="col-xl-8">
                                                                        <div class="form-group row mb-3">
                                                                            <label class="col-md-2 col-form-label" for="website_keywords">Meta keywords</label>
                                                                            <div class="col-md-10">
                                                                                <input type="text" class="form-control bootstrap-tag-input" id="meta_keywords" name="meta_keywords" data-role="tagsinput" style="width: 100%;" value="" placeholder="Write a keyword and then press enter button" . />
                                                                            </div>
                                                                        </div>
                                                                    </div> <!-- end col -->
                                                                    <div class="col-xl-8">
                                                                        <div class="form-group row mb-3">
                                                                            <label class="col-md-2 col-form-label" for="meta_description">Meta description</label>
                                                                            <div class="col-md-10">
                                                                                <textarea name="meta_description" class="form-control">Reactjs</textarea>
                                                                            </div>
                                                                        </div>
                                                                    </div> <!-- end col -->
                                                                </div> <!-- end row -->
                                                            </div>
                                                            <div class="tab-pane" id="finish">
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-center">
                                                                            <h2 class="mt-0"><i class="mdi mdi-check-all"></i></h2>
                                                                            <h3 class="mt-0">Thank you !</h3>

                                                                            <p class="w-75 mb-2 mx-auto">You are just one click away</p>

                                                                            <div class="mb-3 mt-3">
                                                                                <button type="button" class="btn btn-primary text-center" onclick="checkRequiredFields()">Submit</button>
                                                                            </div>
                                                                        </div>
                                                                    </div> <!-- end col -->
                                                                </div> <!-- end row -->
                                                            </div>


                                                        </div> <!-- tab-content -->
                                                    </div> <!-- end #progressbarwizard-->
                                                </form>
                                            </div>
                                        </div><!-- end row-->
                                    </div> <!-- end card-body-->
                                </div> <!-- end card-->
                            </div>
                        </div>

                        <script type="text/javascript">
                            $(document).ready(function () {
                                initSummerNote(['#description']);
                                togglePriceFields('is_free_course');
                            });
                        </script>

                        <script type="text/javascript">
                            var blank_outcome = jQuery('#blank_outcome_field').html();
                            var blank_requirement = jQuery('#blank_requirement_field').html();
                            jQuery(document).ready(function () {
                                jQuery('#blank_outcome_field').hide();
                                jQuery('#blank_requirement_field').hide();
                                calculateDiscountPercentage($('#discounted_price').val());
                            });

                            function appendOutcome() {
                                jQuery('#outcomes_area').append(blank_outcome);
                            }

                            function removeOutcome(outcomeElem) {
                                jQuery(outcomeElem).parent().parent().remove();
                            }

                            function appendRequirement() {
                                jQuery('#requirement_area').append(blank_requirement);
                            }

                            function removeRequirement(requirementElem) {
                                jQuery(requirementElem).parent().parent().remove();
                            }

                            function ajax_get_sub_category(category_id) {
                                $.ajax({
                                    url: 'http://localhost/admin/ajax_get_sub_category/' + category_id,
                                    success: function (response) {
                                        jQuery('#sub_category_id').html(response);
                                    }
                                });
                            }

                            function priceChecked(elem) {
                                if (jQuery('#discountCheckbox').is(':checked')) {

                                    jQuery('#discountCheckbox').prop("checked", false);
                                } else {

                                    jQuery('#discountCheckbox').prop("checked", true);
                                }
                            }

                            function topCourseChecked(elem) {
                                if (jQuery('#isTopCourseCheckbox').is(':checked')) {

                                    jQuery('#isTopCourseCheckbox').prop("checked", false);
                                } else {

                                    jQuery('#isTopCourseCheckbox').prop("checked", true);
                                }
                            }

                            function isFreeCourseChecked(elem) {

                                if (jQuery('#' + elem.id).is(':checked')) {
                                    $('#price').prop('required', false);
                                } else {
                                    $('#price').prop('required', true);
                                }
                            }

                            function calculateDiscountPercentage(discounted_price) {
                                if (discounted_price > 0) {
                                    var actualPrice = jQuery('#price').val();
                                    if (actualPrice > 0) {
                                        var reducedPrice = actualPrice - discounted_price;
                                        var discountedPercentage = (reducedPrice / actualPrice) * 100;
                                        if (discountedPercentage > 0) {
                                            jQuery('#discounted_percentage').text(discountedPercentage.toFixed(2) + "%");

                                        } else {
                                            jQuery('#discounted_percentage').text('0%');
                                        }
                                    }
                                }
                            }

                            $('.on-hover-action').mouseenter(function () {
                                var id = this.id;
                                $('#widgets-of-' + id).show();
                            });
                            $('.on-hover-action').mouseleave(function () {
                                var id = this.id;
                                $('#widgets-of-' + id).hide();
                            });
                        </script>                    <!-- END PLACE PAGE CONTENT HERE -->
                    </div>
                </div>
                <!-- END CONTENT -->
            </div>
        </div>
        <!-- all the js files -->
        <!-- bundle -->
        <script src="./js/backend/app.min.js"></script>
        <!-- third party js -->
        <script src="./js/vendor/Chart.bundle.min.js"></script>
        <script src="./js/vendor/jquery-jvectormap-1.2.2.min.js"></script>
        <script src="./js/vendor/jquery-jvectormap-world-mill-en.js"></script>
        <script src="./js/vendor/jquery.dataTables.min.js"></script>
        <script src="./js/vendor/dataTables.bootstrap4.js"></script>
        <script src="./js/vendor/dataTables.responsive.min.js"></script>
        <script src="./js/vendor/responsive.bootstrap4.min.js"></script>
        <script src="./js/vendor/dataTables.buttons.min.js"></script>
        <script src="./js/vendor/buttons.bootstrap4.min.js"></script>
        <script src="./js/vendor/buttons.html5.min.js"></script>
        <script src="./js/vendor/buttons.flash.min.js"></script>
        <script src="./js/vendor/buttons.print.min.js"></script>
        <script src="./js/vendor/dataTables.keyTable.min.js"></script>
        <script src="./js/vendor/dataTables.select.min.js"></script>
        <script src="./js/vendor/summernote-bs4.min.js"></script>
        <script src="./js/vendor/fullcalendar.min.js"></script>
        <script src="./js/pages/demo.summernote.js"></script>
        <script src="./js/vendor/dropzone.js"></script>
        <script src="./js/page/datatable-initializer.js"></script>
        <script src="./js/backend/font-awesome-icon-picker/fontawesome-iconpicker.min.js" charset="utf-8"></script>
        <script src="./js/vendor/bootstrap-tagsinput.min.js" charset="utf-8"></script>
        <script src="./js/backend/bootstrap-tagsinput.min.js"></script>
        <script src="./js/vendor/dropzone.min.js" charset="utf-8"></script>
        <script src="./js/ui/component.fileupload.js" charset="utf-8"></script>
        <script src="./js/page/demo.form-wizard.js"></script>
        <!-- dragula js-->
        <script src="./js/vendor/dragula.min.js"></script>
        <!-- component js -->
        <script src="./js/backend/component.dragula.js"></script>

        <script src="./js/backend/custom.js"></script>

        <!-- Dashboard chart's data is coming from this file -->
        <script type="text/javascript">
                            !function (o) {
                                "use strict";
                                var t = function () {
                                    this.$body = o("body"), this.charts = []
                                };
                                t.prototype.respChart = function (r, a, n, e) {
                                    Chart.defaults.global.defaultFontColor = "#8391a2", Chart.defaults.scale.gridLines.color = "#8391a2";
                                    var i = r.get(0).getContext("2d"),
                                            s = o(r).parent();
                                    return function () {
                                        var t;
                                        switch (r.attr("width", o(s).width()), a) {
                                            case "Line":
                                                t = new Chart(i, {
                                                    type: "line",
                                                    data: n,
                                                    options: e
                                                });
                                                break;
                                            case "Doughnut":
                                                t = new Chart(i, {
                                                    type: "doughnut",
                                                    data: n,
                                                    options: e
                                                })
                                        }
                                        return t
                                    }()
                                }, t.prototype.initCharts = function () {
                                    var t = [];
                                    if (0 < o("#task-area-chart").length) {
                                        t.push(this.respChart(o("#task-area-chart"), "Line", {
                                            labels: [
                                                "January",
                                                "February",
                                                "March",
                                                "April",
                                                "May",
                                                "June",
                                                "July",
                                                "August",
                                                "September",
                                                "October",
                                                "November",
                                                "December",
                                            ],
                                            datasets: [{
                                                    label: "This year",
                                                    backgroundColor: "rgba(114, 124, 245, 0.3)",
                                                    borderColor: "#727cf5",
                                                    data: [
                                                        "0",
                                                        "0",
                                                        "0",
                                                        "0",
                                                        "0",
                                                        "0",
                                                        "0",
                                                        "0",
                                                        "0",
                                                        "0",
                                                        "0",
                                                        "0",
                                                    ]
                                                }]
                                        }, {
                                            maintainAspectRatio: !1,
                                            legend: {
                                                display: !1
                                            },
                                            tooltips: {
                                                intersect: !1
                                            },
                                            hover: {
                                                intersect: !0
                                            },
                                            plugins: {
                                                filler: {
                                                    propagate: !1
                                                }
                                            },
                                            scales: {
                                                xAxes: [{
                                                        reverse: !0,
                                                        gridLines: {
                                                            color: "rgba(0,0,0,0.05)"
                                                        }
                                                    }],
                                                yAxes: [{
                                                        ticks: {
                                                            stepSize: 10,
                                                            display: !1
                                                        },
                                                        min: 10,
                                                        max: 100,
                                                        display: !0,
                                                        borderDash: [5, 5],
                                                        gridLines: {
                                                            color: "rgba(0,0,0,0)",
                                                            fontColor: "#fff"
                                                        }
                                                    }]
                                            }
                                        }))
                                    }
                                    if (0 < o("#project-status-chart").length) {
                                        t.push(this.respChart(o("#project-status-chart"), "Doughnut", {
                                            labels: ["Active course", "Pending course"],
                                            datasets: [{
                                                    data: [4, 0],
                                                    backgroundColor: ["#0acf97", "#FFC107"],
                                                    borderColor: "transparent",
                                                    borderWidth: "2"
                                                }]
                                        }, {
                                            maintainAspectRatio: !1,
                                            cutoutPercentage: 80,
                                            legend: {
                                                display: !1
                                            }
                                        }))
                                    }
                                    return t
                                }, t.prototype.init = function () {
                                    var r = this;
                                    Chart.defaults.global.defaultFontFamily = '-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen-Sans,Ubuntu,Cantarell,"Helvetica Neue",sans-serif', r.charts = this.initCharts(), o(window).on("resize", function (t) {
                                        o.each(r.charts, function (t, r) {
                                            try {
                                                r.destroy()
                                            } catch (t) {
                                            }
                                        }), r.charts = r.initCharts()
                                    })
                                }, o.ChartJs = new t, o.ChartJs.Constructor = t
                            }(window.jQuery),
                                    function (t) {
                                        "use strict";
                                        window.jQuery.ChartJs.init()
                                    }();

        </script>

        <script type="text/javascript">
            $(document).ready(function () {
                $(function () {
                    $('.icon-picker').iconpicker();
                });
            });
        </script>

        <!-- Toastr and alert notifications scripts -->
        <script type="text/javascript">
            function notify(message) {
                $.NotificationApp.send("Heads up!", message, "top-right", "rgba(0,0,0,0.2)", "info");
            }

            function success_notify(message) {
                $.NotificationApp.send("Congratulations!", message, "top-right", "rgba(0,0,0,0.2)", "success");
            }

            function error_notify(message) {
                $.NotificationApp.send("Oh snap!", message, "top-right", "rgba(0,0,0,0.2)", "error");
            }

            function error_required_field() {
                $.NotificationApp.send("Oh snap!", "Please fill all the required fields", "top-right", "rgba(0,0,0,0.2)", "error");
            }
        </script>



        <script type="text/javascript">
            function showAjaxModal(url, header)
            {
                // SHOWING AJAX PRELOADER IMAGE
                jQuery('#scrollable-modal .modal-body').html('<div style="width: 100px; height: 100px; line-height: 100px; padding: 0px; text-align: center; margin-left: auto; margin-right: auto;"><div class="spinner-border text-secondary" role="status"></div></div>');
                jQuery('#scrollable-modal .modal-title').html('loading...');
                // LOADING THE AJAX MODAL
                jQuery('#scrollable-modal').modal('show', {backdrop: 'true'});

                // SHOW AJAX RESPONSE ON REQUEST SUCCESS
                $.ajax({
                    url: url,
                    success: function (response)
                    {
                        jQuery('#scrollable-modal .modal-body').html(response);
                        jQuery('#scrollable-modal .modal-title').html(header);
                    }
                });
            }
            function showLargeModal(url, header)
            {
                // SHOWING AJAX PRELOADER IMAGE
                jQuery('#large-modal .modal-body').html('<div style="width: 100px; height: 100px; line-height: 100px; padding: 0px; text-align: center; margin-left: auto; margin-right: auto;"><div class="spinner-border text-secondary" role="status"></div></div>');
                jQuery('#large-modal .modal-title').html('...');
                // LOADING THE AJAX MODAL
                jQuery('#large-modal').modal('show', {backdrop: 'true'});

                // SHOW AJAX RESPONSE ON REQUEST SUCCESS
                $.ajax({
                    url: url,
                    success: function (response)
                    {
                        jQuery('#large-modal .modal-body').html(response);
                        jQuery('#large-modal .modal-title').html(header);
                    }
                });
            }
        </script>

        <!-- (Large Modal)-->
        <div class="modal fade" id="large-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myLargeModalLabel">Large modal</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        ...
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>

        <!-- Scrollable modal -->
        <div class="modal fade" id="scrollable-modal" tabindex="-1" role="dialog" aria-labelledby="scrollableModalTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="scrollableModalTitle">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body ml-2 mr-2">

                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>

        <script type="text/javascript">
            function confirm_modal(delete_url)
            {
                jQuery('#alert-modal').modal('show', {backdrop: 'static'});
                document.getElementById('update_link').setAttribute('href', delete_url);
            }
        </script>

        <!-- Info Alert Modal -->
        <div id="alert-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-body p-4">
                        <div class="text-center">
                            <i class="dripicons-information h1 text-info"></i>
                            <h4 class="mt-2">Heads up!</h4>
                            <p class="mt-3">Are you sure?</p>
                            <button type="button" class="btn btn-info my-2" data-dismiss="modal">Cancel</button>
                            <a href="#" id="update_link" class="btn btn-danger my-2">Continue</a>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <script type="text/javascript">

            function ajax_confirm_modal(delete_url, elem_id)
            {
                jQuery('#ajax-alert-modal').modal('show', {backdrop: 'static'});
                $("#appent_link_a").bind("click", function () {
                    delete_by_ajax_calling(delete_url, elem_id);
                });
            }

            function delete_by_ajax_calling(delete_url, elem_id) {
                $.ajax({
                    url: delete_url,
                    success: function (response) {
                        var response = JSON.parse(response);
                        if (response.status == 'success') {
                            $('#' + elem_id).fadeOut(600);
                            $.NotificationApp.send("Success!", response.message, "top-right", "rgba(0,0,0,0.2)", "success");
                        } else {
                            $.NotificationApp.send("Oh snap!", response.message, "top-right", "rgba(0,0,0,0.2)", "error");
                        }
                    }
                });
            }
        </script>

        <!-- Info Alert Modal -->
        <div id="ajax-alert-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-body p-4">
                        <div class="text-center" id="appent_link">
                            <i class="dripicons-information h1 text-info"></i>
                            <h4 class="mt-2">Heads up!</h4>
                            <p class="mt-3">Are you sure?</p>
                            <button type="button" class="btn btn-info my-2" data-dismiss="modal">Cancel</button>
                            <a id="appent_link_a" href="javascript:;" class="btn btn-danger my-2" data-dismiss="modal">Continue</a>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->    <script type="text/javascript">
            function togglePriceFields(elem) {
                if ($("#" + elem).is(':checked')) {
                    $('.paid-course-stuffs').slideUp();
                } else
                    $('.paid-course-stuffs').slideDown();
            }
        </script>


        <script type="text/javascript">
            function switch_language(language) {
                $.ajax({
                    url: 'http://localhost/home/site_language',
                    type: 'post',
                    data: {language: language},
                    success: function (response) {
                        setTimeout(function () {
                            location.reload();
                        }, 500);
                    }
                });
            }
        </script></body>
</html>

