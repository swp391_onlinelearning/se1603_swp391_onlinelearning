<%-- 
    Document   : UserAllCourse
    Created on : Jun 18, 2022, 8:00:31 PM
    Author     : genni
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Courses | NewSite</title>


        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5.0, minimum-scale=0.86">
        <meta name="author" content="Creativeitem" />

        <meta name="keywords" content="LMS,Learning Management System,Creativeitem,demo,hello,How are you"/>
        <meta name="description" content="Study any topic, anytime. explore thousands of courses for the lowest price ever!" />

        <!--Social sharing content-->
        <meta property="og:title" content="Courses" />
        <meta property="og:image" content="./img/home-banner.jpg">
        <meta property="og:url" content="http://localhost/home/courses" />
        <meta property="og:type" content="Learning management system" />
        <!--Social sharing content-->


        <!--Drips icons-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dripicons/2.0.0/webfont.min.css" integrity="sha512-pi7KSLdGMxSE62WWJ62B1R5/H7WNnIsj2f51MikplRt31K0uCZ1lfPSw/0Jb1flSz6Ed2YLSlox6Uulf7CaFiA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dripicons/2.0.0/webfont.min.css" integrity="sha512-pi7KSLdGMxSE62WWJ62B1R5/H7WNnIsj2f51MikplRt31K0uCZ1lfPSw/0Jb1flSz6Ed2YLSlox6Uulf7CaFiA==" crossorigin="anonymous" referrerpolicy="no-referrer" />

        <!--Material Design Icon-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/2.0.46/css/materialdesignicons.min.css" integrity="sha512-HmBTsbqKSDy0wIk8SGSCj68xUg8b22mGtXx8cXF64qcmnQnJepz6Aq37X43gF/WhbvqPcx68GoiaWu8wE8/y4g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/2.0.46/css/materialdesignicons.css" integrity="sha512-OSd9YjMibKZ0CN1njc/7H/IK8zmRevGk+n/Z+jAyClgFZ+sOvQKzmeJKBKW313Ln+630pAlcCMjX1dzTDGe4+w==" crossorigin="anonymous" referrerpolicy="no-referrer" />


        <link name="favicon" type="image/x-icon" href="./img/favicon.png" rel="shortcut icon" />
        <link rel="favicon" href="./img/icons/favicon.ico">
        <link rel="apple-touch-icon" href="./img/icon.png">


        <!-- font awesome 5 -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.3.1/css/all.min.css" integrity="sha512-KulI0psuJQK8UMpOeiMLDXJtGOZEBm8RZNTyBBHIWqoXoPMFcw+L5AEo0YMpsW8BfiuWrdD1rH6GWGgQBF59Lg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="./css/fontawesome-all.min.css">

        <link rel="stylesheet" href="./css/bootstrap.min.css">

        <link rel="stylesheet" href="./css/main.css">
        <link rel="stylesheet" href="./css/responsive.css">
        <link rel="stylesheet" href="./css/custom.css">
        <link rel="stylesheet" href="./css/custom.css">
        <link rel="stylesheet" href="./css/tagify.css">
        <link rel="stylesheet" href="./toastr.css">
        <script src="./js/jquery-3.3.1.min.js"></script>
    </head>
    <body class="gray-bg">
        <section class="menu-area bg-white">
            <div class="container-xl">
                <nav class="navbar navbar-expand-lg navbar-light">

                    <ul class="mobile-header-buttons">
                        <li><a class="mobile-nav-trigger" href="#mobile-primary-nav">Menu<span></span></a></li>
                        <li><a class="mobile-search-trigger" href="#mobile-search">Search<span></span></a></li>
                    </ul>

                    <a href="http://localhost/" class="navbar-brand" href="#">
                        <img src="./img/logo-dark.png" alt="" height="35">
                    </a>

                    <div class="main-nav-wrap">
                        <div class="mobile-overlay"></div>
                        <style type="text/css">
                            @media only screen and (max-width: 767px) {
                                .category.corner-triangle.top-left.pb-0.is-hidden{
                                    display: none !important;
                                }
                                .sub-category.is-hidden{
                                    display: none !important;
                                }
                            }
                        </style>

                        <ul class="mobile-main-nav">
                            <div class="mobile-menu-helper-top"></div>
                            <li class="has-children text-nowrap fw-bold">
                                <a href="">
                                    <i class="fas fa-th d-inline text-20px"></i>
                                    <span class="fw-500">Categories</span>
                                    <span class="has-sub-category"><i class="fas fa-angle-right"></i></span>
                                </a>

                                <ul class="category corner-triangle top-left is-hidden pb-0" >
                                    <li class="go-back"><a href=""><i class="fas fa-angle-left"></i>Menu</a></li>

                                    <li class="has-children">
                                        <a href="javascript:;" class="py-2 text-wrap" onclick="redirect_to('http://localhost/home/courses?category=asdasdasd')">
                                            <span class="icon"><i class="asdasdasd"></i></span>
                                            <span>asdasdasd</span>
                                            <span class="has-sub-category"><i class="fas fa-angle-right"></i></span>
                                        </a>
                                        <ul class="sub-category is-hidden">
                                            <li class="go-back-menu"><a href=""><i class="fas fa-angle-left"></i>Menu</a></li>
                                            <li class="go-back"><a href="">
                                                    <i class="fas fa-angle-left"></i>
                                                    <span class="icon"><i class="asdasdasd"></i></span>
                                                    asdasdasd              </a></li>
                                            <li><a class="text-wrap" href="http://localhost/home/courses?category=react">React</a></li>
                                            <li><a class="text-wrap" href="http://localhost/home/courses?category=linh-tịnh">Linh tịnh</a></li>
                                        </ul>
                                    </li>
                                    <li class="all-category-devided mb-0 p-0">
                                        <a href="http://localhost/home/courses" class="py-3">
                                            <span class="icon"><i class="fa fa-align-justify"></i></span>
                                            <span>All courses</span>
                                        </a>
                                    </li>


                                </ul>
                            </li>

                            <div class="mobile-menu-helper-bottom"></div>
                        </ul>
                    </div>


                    <form class="inline-form" action="http://localhost/home/search" method="get" style="width: 100%;">
                        <div class="input-group search-box mobile-search">
                            <input type="text" name='query' class="form-control" placeholder="Search for courses">
                            <div class="input-group-append">
                                <button class="btn" type="submit"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </form>

                    <div class="instructor-box menu-icon-box ms-md-4">
                        <div class="icon">
                            <a href="http://localhost/user" style="border: 1px solid transparent; margin: 0px;     padding: 0px 10px; font-size: 14px; width: max-content; border-radius: 5px; height: 40px; line-height: 40px;">Instructor</a>
                        </div>
                    </div>

                    <div class="instructor-box menu-icon-box">
                        <div class="icon">
                            <a href="http://localhost/home/my_courses" style="border: 1px solid transparent; margin: 0px;     padding: 0px 10px; font-size: 14px; width: max-content; border-radius: 5px; height: 40px; line-height: 40px;">My courses</a>
                        </div>
                    </div>

                    <div class="wishlist-box menu-icon-box" id="wishlist_items">
                        <div class="icon">
                            <a href=""><i class="far fa-heart"></i></a>
                            <span class="number">0</span>
                        </div>
                        <div class="dropdown course-list-dropdown corner-triangle top-right">
                            <div class="list-wrapper">
                                <div class="item-list">
                                    <ul>
                                    </ul>
                                </div>
                                <div class="dropdown-footer">
                                    <a href="http://localhost/home/my_wishlist">Go to wishlist</a>
                                </div>
                            </div>
                            <div class="empty-box text-center d-none">
                                <p>Your wishlist is empty.</p>
                                <a href="">Explore courses</a>
                            </div>
                        </div>            </div>

                    <div class="cart-box menu-icon-box" id="cart_items">
                        <div class="icon">
                            <a href="http://localhost/home/shopping_cart"><i class="fas fa-shopping-cart"></i></a>
                            <span class="number">1</span>
                        </div>

                        <!-- Cart Dropdown goes here -->
                        <div class="dropdown course-list-dropdown corner-triangle top-right" style="display: none;"> <!-- Just remove the display none from the css to make it work -->
                            <div class="list-wrapper">
                                <div class="item-list">
                                    <ul>
                                        <li>
                                            <div class="item clearfix">
                                                <div class="item-image">
                                                    <a href="">
                                                        <img src="./img/course_thumbnail_default_1.jpg" alt="" class="img-fluid">
                                                    </a>
                                                </div>
                                                <div class="item-details">
                                                    <a href="">
                                                        <div class="course-name">300 bài code thiếu nhi</div>
                                                        <div class="instructor-name">Tien Nguyen</div>
                                                        <div class="item-price">
                                                            <span class="current-price">$10</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="dropdown-footer">
                                    <div class="cart-total-price clearfix">
                                        <span>Total:</span>
                                        <div class="float-end">
                                            <span class="current-price">$10</span>
                                            <!-- <span class="original-price">$94.99</span> -->
                                        </div>
                                    </div>
                                    <a href = "./shopping_cart.html">Go to cart</a>
                                </div>
                            </div>
                            <div class="empty-box text-center d-none">
                                <p>Your cart is empty.</p>
                                <a href="">Keep Shopping</a>
                            </div>
                        </div>            </div>



                    <div class="user-box menu-icon-box">
                        <div class="icon">
                            <a href="javascript::">
                                <img src="./img/placeholder.png" alt="" class="img-fluid">
                            </a>
                        </div>
                        <div class="dropdown user-dropdown corner-triangle top-right radius-10">
                            <ul class="user-dropdown-menu radius-10">

                                <li class="dropdown-user-info">
                                    <a href="javascript:;" class="radius-top-10">
                                        <div class="clearfix">
                                            <div class="user-image float-start">
                                                <img src="./img/placeholder.png" alt="">
                                            </div>
                                            <div class="user-details">
                                                <div class="user-name">
                                                    <span class="hi">Hi,</span>
                                                    Pho DayHanh                                        </div>
                                                <div class="user-email">
                                                    <span class="email">abcdef@gmail.com</span>
                                                    <span class="welcome">Welcome back</span>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </li>

                                <li class="user-dropdown-menu-item"><a href="http://localhost/home/my_courses"><i class="far fa-gem"></i>My courses</a></li>
                                <li class="user-dropdown-menu-item"><a href="http://localhost/home/my_wishlist"><i class="far fa-heart"></i>My wishlist</a></li>
                                <li class="user-dropdown-menu-item"><a href="http://localhost/home/my_messages"><i class="far fa-envelope"></i>My messages</a></li>
                                <li class="user-dropdown-menu-item"><a href="http://localhost/home/purchase_history"><i class="fas fa-shopping-cart"></i>Purchase history</a></li>
                                <li class="user-dropdown-menu-item"><a href="http://localhost/home/profile/user_profile"><i class="fas fa-user"></i>User profile</a></li>

                                <li class="dropdown-user-logout user-dropdown-menu-item radius-bottom-10"><a class="radius-bottom-10 py-3" href="http://localhost/login/logout"><i class="fas fa-sign-out-alt"></i> Log out</a></li>
                            </ul>
                        </div>
                    </div>



                    <span class="signin-box-move-desktop-helper"></span>
                    <div class="sign-in-box btn-group d-none">

                        <button type="button" class="btn btn-sign-in" data-toggle="modal" data-target="#signInModal">Log In</button>

                        <button type="button" class="btn btn-sign-up" data-toggle="modal" data-target="#signUpModal">Sign Up</button>

                    </div> <!--  sign-in-box end -->


                </nav>
            </div>
        </section><link rel="stylesheet" type="text/css" href="./css/purecookie.css" async />

        <div class="cookieConsentContainer" id="cookieConsentContainer" style="opacity: .9; display: block; display: none;">
            <!-- <div class="cookieTitle">
              <a>Cookies.</a>
            </div> -->
            <div class="cookieDesc">
                <p>
                    This website uses cookies to personalize content and analyse traffic in order to offer you a better experience.      <a class="link-cookie-policy" href="http://localhost/home/cookie_policy">Cookie policy</a>
                </p>
            </div>
            <div class="cookieButton">
                <a onclick="cookieAccept();">Accept</a>
            </div>
        </div>
        <script>
            $(document).ready(function () {
                if (localStorage.getItem("accept_cookie_academy")) {
                    //localStorage.removeItem("accept_cookie_academy");
                } else {
                    $('#cookieConsentContainer').fadeIn(1000);
                }
            });

            function cookieAccept() {
                if (typeof (Storage) !== "undefined") {
                    localStorage.setItem("accept_cookie_academy", true);
                    localStorage.setItem("accept_cookie_time", "06/15/2022");
                    $('#cookieConsentContainer').fadeOut(1200);
                }
            }
        </script>

        <section class="category-header-area" style="background-image: url('./img/course_page_banner.png');">
            <div class="image-placeholder-1"></div>
            <div class="container-lg breadcrumb-container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item display-6 fw-bold">
                            <a href="http://localhost/home/courses">
                                All courses                </a>
                        </li>
                        <li class="breadcrumb-item active text-light display-6 fw-bold">
                            All category            </li>
                    </ol>
                </nav>
            </div>
        </section>


        <section class="category-course-list-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 filter-area">
                        <div class="card border-0 radius-10">
                            <div id="collapseFilter" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body p-0">
                                    <div class="filter_type px-4 pt-4">
                                        <h5 class="fw-700 mb-4">Categories</h5>
                                        <ul>
                                            <li class="">
                                                <div class="text-15px fw-700 d-flex">
                                                    <input type="radio" id="category_all" name="sub_category" class="categories custom-radio" value="all" onclick="filter(this)" checked>
                                                    <label for="category_all">All category</label>
                                                    <div class="ms-auto">(2)</div>
                                                </div>
                                            </li>
                                            <li class="mt-3">
                                                <div class="text-15px fw-700  d-flex">
                                                    <input type="radio" id="category-1" name="sub_category" class="categories custom-radio" value="asdasdasd" onclick="filter(this)" >
                                                    <label for="category-1">asdasdasd</label>
                                                    <div class="ms-auto">(2)</div>
                                                </div>
                                            </li>
                                            <li class="ms-3">
                                                <div class=" d-flex">
                                                    <input type="radio" id="sub_category-2" name="sub_category" class="categories custom-radio" value="react" onclick="filter(this)" >
                                                    <label for="sub_category-2">React</label>
                                                    <div class="ms-auto">(1)</div>
                                                </div>
                                            </li>
                                            <li class="ms-3">
                                                <div class=" d-flex">
                                                    <input type="radio" id="sub_category-3" name="sub_category" class="categories custom-radio" value="linh-tịnh" onclick="filter(this)" >
                                                    <label for="sub_category-3">Linh tịnh</label>
                                                    <div class="ms-auto">(1)</div>
                                                </div>
                                            </li>
                                        </ul>
                                        <a href="javascript:;" class="text-13px fw-500" id="city-toggle-btn" onclick="showToggle(this, 'hidden-categories')"></a>
                                    </div>
                                    <hr>
                                    <div class="filter_type px-4">
                                        <div class="form-group">
                                            <h5 class="fw-700 mb-3">Price</h5>
                                            <ul>
                                                <li>
                                                    <div class="">
                                                        <input type="radio" id="price_all" name="price" class="prices custom-radio" value="all" onclick="filter(this)" checked>
                                                        <label for="price_all">All</label>
                                                    </div>
                                                    <div class="">
                                                        <input type="radio" id="price_free" name="price" class="prices custom-radio" value="free" onclick="filter(this)" >
                                                        <label for="price_free">Free</label>
                                                    </div>
                                                    <div class="">
                                                        <input type="radio" id="price_paid" name="price" class="prices custom-radio" value="paid" onclick="filter(this)" >
                                                        <label for="price_paid">Paid</label>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="filter_type px-4">
                                        <h5 class="fw-700 mb-3">Level</h5>
                                        <ul>
                                            <li>
                                                <div class="">
                                                    <input type="radio" id="all" name="level" class="level custom-radio" value="all" onclick="filter(this)" checked>
                                                    <label for="all">All</label>
                                                </div>
                                                <div class="">
                                                    <input type="radio" id="beginner" name="level" class="level custom-radio" value="beginner" onclick="filter(this)" >
                                                    <label for="beginner">Beginner</label>
                                                </div>
                                                <div class="">
                                                    <input type="radio" id="advanced" name="level" class="level custom-radio" value="advanced" onclick="filter(this)" >
                                                    <label for="advanced">Advanced</label>
                                                </div>
                                                <div class="">
                                                    <input type="radio" id="intermediate" name="level" class="level custom-radio" value="intermediate" onclick="filter(this)" >
                                                    <label for="intermediate">Intermediate</label>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <hr>
                                    <div class="filter_type px-4">
                                        <h5 class="fw-700 mb-3">Language</h5>
                                        <ul>
                                            <li>
                                                <div class="">
                                                    <input type="radio" id="all_language" name="language" class="languages custom-radio" value="all" onclick="filter(this)" checked>
                                                    <label for="all_language">All</label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="">
                                                    <input type="radio" id="language_english" name="language" class="languages custom-radio" value="english" onclick="filter(this)" >
                                                    <label for="language_english">English</label>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <hr>
                                    <div class="filter_type px-4">
                                        <h5 class="fw-700 mb-3">Ratings</h5>
                                        <ul>
                                            <li>
                                                <div class="">
                                                    <input type="radio" id="all_rating" name="rating" class="ratings custom-radio" value="all" onclick="filter(this)" checked>
                                                    <label for="all_rating">All</label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="">
                                                    <input type="radio" id="rating_1" name="rating" class="ratings custom-radio" value="1" onclick="filter(this)" >
                                                    <label for="rating_1">
                                                        <i class="fas fa-star" style="color: #f4c150;"></i>
                                                        <i class="far fa-star" style="color: #dedfe0;"></i>
                                                        <i class="far fa-star" style="color: #dedfe0;"></i>
                                                        <i class="far fa-star" style="color: #dedfe0;"></i>
                                                        <i class="far fa-star" style="color: #dedfe0;"></i>
                                                    </label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="">
                                                    <input type="radio" id="rating_2" name="rating" class="ratings custom-radio" value="2" onclick="filter(this)" >
                                                    <label for="rating_2">
                                                        <i class="fas fa-star" style="color: #f4c150;"></i>
                                                        <i class="fas fa-star" style="color: #f4c150;"></i>
                                                        <i class="far fa-star" style="color: #dedfe0;"></i>
                                                        <i class="far fa-star" style="color: #dedfe0;"></i>
                                                        <i class="far fa-star" style="color: #dedfe0;"></i>
                                                    </label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="">
                                                    <input type="radio" id="rating_3" name="rating" class="ratings custom-radio" value="3" onclick="filter(this)" >
                                                    <label for="rating_3">
                                                        <i class="fas fa-star" style="color: #f4c150;"></i>
                                                        <i class="fas fa-star" style="color: #f4c150;"></i>
                                                        <i class="fas fa-star" style="color: #f4c150;"></i>
                                                        <i class="far fa-star" style="color: #dedfe0;"></i>
                                                        <i class="far fa-star" style="color: #dedfe0;"></i>
                                                    </label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="">
                                                    <input type="radio" id="rating_4" name="rating" class="ratings custom-radio" value="4" onclick="filter(this)" >
                                                    <label for="rating_4">
                                                        <i class="fas fa-star" style="color: #f4c150;"></i>
                                                        <i class="fas fa-star" style="color: #f4c150;"></i>
                                                        <i class="fas fa-star" style="color: #f4c150;"></i>
                                                        <i class="fas fa-star" style="color: #f4c150;"></i>
                                                        <i class="far fa-star" style="color: #dedfe0;"></i>
                                                    </label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="">
                                                    <input type="radio" id="rating_5" name="rating" class="ratings custom-radio" value="5" onclick="filter(this)" >
                                                    <label for="rating_5">
                                                        <i class="fas fa-star" style="color: #f4c150;"></i>
                                                        <i class="fas fa-star" style="color: #f4c150;"></i>
                                                        <i class="fas fa-star" style="color: #f4c150;"></i>
                                                        <i class="fas fa-star" style="color: #f4c150;"></i>
                                                        <i class="fas fa-star" style="color: #f4c150;"></i>
                                                    </label>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-9">
                        <div class="row category-filter-box mx-0" >
                            <div class="col-md-6">
                                <button class="btn py-1 px-2 mx-2 btn-light" onclick="toggleLayout('grid')"><i class="fas fa-th-large"></i></button>
                                <button class="btn py-1 px-2 mx-2 btn-danger" onclick="toggleLayout('list')"><i class="fas fa-list"></i></button>
                                <span class="text-12px fw-700 text-muted">Showing 2 Of 2 Results</span>
                            </div>
                            <div class="col-md-6 text-end filter-sort-by">
                                <!-- <span>Sort by : </span>
                                <div class="dropdown d-inline-block">
                                    <button class="btn bg-background dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                        Newest
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuButton1">
                                        <li><a class="dropdown-item text-12px fw-500" href="#">Action</a></li>
                                        <li><a class="dropdown-item text-12px fw-500" href="#">Another action</a></li>
                                    </ul>
                                </div> -->
                            </div>
                        </div>
                        <div class="category-course-list">
                            <ul>
                                <li>
                                    <div class="course-box-2">
                                        <div class="course-image">
                                            <a href="http://localhost:8080/Course?id=1">
                                                <img src="./img/course_thumbnail_default_1.jpg" alt="" class="img-fluid">
                                            </a>
                                        </div>
                                        <div class="course-details">
                                            <a href="http://localhost:8080/Course?id=1" class="course-title">300 bài code thiếu nhi</a>

                                            <div class="course-subtitle d-none d-md-block">
                                                Đây là 300 bài code thiếu nhi                    </div>

                                            <div class="course-meta">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <span class=""><i class="fas fa-play-circle"></i>
                                                            0 Lessons                                    </span>
                                                        <span class=""><i class="far fa-clock"></i>
                                                            00:00:00 Hours                                    </span>
                                                        <span class=""><i class="fas fa-closed-captioning"></i>English</span>
                                                        <span class=""><i class="fa fa-level-up"></i>Beginner</span>
                                                        <button class="brn-compare-sm" onclick="event.stopPropagation(); $(location).attr('href', 'http://localhost:8080/Course?id=1');"><i class="fas fa-balance-scale"></i> Compare</button>
                                                    </div>
                                                </div>


                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="floating-user d-inline-block">
                                                            <img src="./img/placeholder.png" width="30px" data-bs-toggle="tooltip" data-bs-placement="top" title="Tien Nguyen" onclick="event.stopPropagation(); $(location).attr('href', 'http://localhost:8080/Course?id=1');">
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="course-price-rating">
                                            <div class="course-price">
                                                <span class="current-price">$10</span>
                                            </div>
                                            <div class="rating">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <span class="d-inline-block average-rating">0</span>
                                            </div>
                                            <div class="rating-number">
                                                0 Ratings                    </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="course-box-2">
                                        <div class="course-image">
                                            <a href="http://localhost:8080/Course?id=1">
                                                <img src="./img/course_thumbnail_default_2.jpg" alt="" class="img-fluid">
                                            </a>
                                        </div>
                                        <div class="course-details">
                                            <a href="http://localhost:8080/Course?id=1" class="course-title">Học để đa nghi</a>

                                            <div class="course-subtitle d-none d-md-block">
                                                Thà ta phụ thiên hạ chứ ko để thiên hạ phụ ta...                    </div>

                                            <div class="course-meta">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <span class=""><i class="fas fa-play-circle"></i>
                                                            0 Lessons                                    </span>
                                                        <span class=""><i class="far fa-clock"></i>
                                                            00:00:00 Hours                                    </span>
                                                        <span class=""><i class="fas fa-closed-captioning"></i>English</span>
                                                        <span class=""><i class="fa fa-level-up"></i>Intermediate</span>
                                                        <button class="brn-compare-sm" onclick="event.stopPropagation(); $(location).attr('href', 'http://localhost:8080/Course?id=1');"><i class="fas fa-balance-scale"></i> Compare</button>
                                                    </div>
                                                </div>


                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="floating-user d-inline-block">
                                                            <img src="./img/placeholder.png" width="30px" data-bs-toggle="tooltip" data-bs-placement="top" title="Tien Nguyen" onclick="event.stopPropagation(); $(location).attr('href', 'http://localhost:8080/Course?id=1');">
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="course-price-rating">
                                            <div class="course-price">
                                                <span class="current-price">$100</span>
                                            </div>
                                            <div class="rating">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <span class="d-inline-block average-rating">0</span>
                                            </div>
                                            <div class="rating-number">
                                                0 Ratings                    </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>                                    </div>
                        <nav>
                        </nav>
                    </div>
                </div>
            </div>
        </section>

        <script type="text/javascript">
            function get_url() {
                var urlPrefix = 'http://localhost/home/courses?'
                var urlSuffix = "";
                var slectedCategory = "";
                var selectedPrice = "";
                var selectedLevel = "";
                var selectedLanguage = "";
                var selectedRating = "";

                // Get selected category
                $('.categories:checked').each(function () {
                    slectedCategory = $(this).attr('value');
                });

                // Get selected price
                $('.prices:checked').each(function () {
                    selectedPrice = $(this).attr('value');
                });

                // Get selected difficulty Level
                $('.level:checked').each(function () {
                    selectedLevel = $(this).attr('value');
                });

                // Get selected difficulty Level
                $('.languages:checked').each(function () {
                    selectedLanguage = $(this).attr('value');
                });

                // Get selected rating
                $('.ratings:checked').each(function () {
                    selectedRating = $(this).attr('value');
                });

                urlSuffix = "category=" + slectedCategory + "&&price=" + selectedPrice + "&&level=" + selectedLevel + "&&language=" + selectedLanguage + "&&rating=" + selectedRating;
                var url = urlPrefix + urlSuffix;
                return url;
            }

            function filter() {
                var url = get_url();
                window.location.replace(url);
                //console.log(url);
            }

            function toggleLayout(layout) {
                $.ajax({
                    type: 'POST',
                    url: 'http://localhost/home/set_layout_to_session',
                    data: {
                        layout: layout
                    },
                    success: function (response) {
                        location.reload();
                    }
                });
            }

            function showToggle(elem, selector) {
                $('.' + selector).slideToggle(50);
                $('.' + selector).toggleClass("d-flex");
                if ($(elem).text() === "Show more") {
                    $(elem).text('Show less');
                } else {
                    $(elem).text('Show more');
                }
            }

            $('.course-compare').click(function (e) {
                e.preventDefault()
                var redirect_to = $(this).attr('redirect_to');
                window.location.replace(redirect_to);
            });
        </script><footer class="footer-area d-print-none bg-gray mt-5 pt-5">
            <div class="container-xl">
                <div class="row mb-3">
                    <div class="col-6 col-sm-6 col-md-3">
                        <h5 class="text-muted mb-3">Top categories</h5>
                        <ul class="list-unstyled text-small">
                            <li class="mb-2">
                                <a class="link-secondary footer-hover-link" href="http://localhost/home/courses?category=react">
                                    React                  <!-- <span class="fw-700 text-end">()</span> -->
                                </a>
                            </li>
                            <li class="mb-2">
                                <a class="link-secondary footer-hover-link" href="http://localhost/home/courses?category=linh-tịnh">
                                    Linh tịnh                  <!-- <span class="fw-700 text-end">()</span> -->
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-6 col-sm-6 col-md-3">
                        <h5 class="text-muted mb-3">Useful links</h5>
                        <ul class="list-unstyled text-small">
                            <li class="mb-2"><a class="link-secondary footer-hover-link" href="./blog.html">Blog</a></li>
                            <li class="mb-2"><a class="link-secondary footer-hover-link" href="./user_view_all_course.html">All courses</a></li>
                            <li class="mb-2"><a class="link-secondary footer-hover-link" href="./signup.html">Sign up</a></li>
                            <li class="mb-2"><a class="link-secondary footer-hover-link" href="./signin.html">Login</a></li>
                        </ul>
                    </div>
                    <div class="col-6 col-sm-6 col-md-3">
                        <h5 class="text-muted mb-3">Help</h5>
                        <ul class="list-unstyled text-small">
                            <li class="mb-2"><a class="link-secondary footer-hover-link" href="http://localhost/home/about_us">About us</a></li>
                            <li class="mb-2"><a class="link-secondary footer-hover-link" href="http://localhost/home/privacy_policy">Privacy policy</a></li>
                            <li class="mb-2"><a class="link-secondary footer-hover-link" href="http://localhost/home/terms_and_condition">Terms and condition</a></li>
                            <li class="mb-2"><a class="link-secondary footer-hover-link" href="http://localhost/home/refund_policy">Refund policy</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-12 order-sm-first">
                        <img src="./img/logo-dark.png" width="130">
                        <span class="d-block mb-1 mt-2 fw-600" style="font-size: 14.5px; line-height: 28px">Study any topic, anytime. explore thousands of courses for the lowest price ever!</span>

                        <ul class="footer-social-link">
                            <li class="mb-1">
                                <a href="https://facebook.com"><i class="fab fa-facebook-f"></i></a>
                            </li>
                            <li class="mb-1">
                                <a href="https://twitter.com"><i class="fab fa-twitter"></i></a>
                            </li>
                            <li class="mb-1">
                                <a href="https://linkedin.com"><i class="fab fa-linkedin"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <section class="border-top">
                <div class="container-xl">
                    <div class="row mt-3 py-1">
                        <div class="col-6 col-sm-6 col-md-3 text-muted text-13px">
                            &copy; 2021 NewSite, All rights reserved        </div>

                        <div class="col-6 col-sm-6 col-md-3 d-none d-md-block"></div>
                        <div class="col-6 col-sm-6 col-md-3 d-none d-md-block"></div>
                        <div class="col-6 col-sm-6 col-md-3 text-center text-md-start">
                            <select class="language_selector" onchange="switch_language(this.value)">
                                <option value="english" selected>English</option>
                            </select>
                        </div>
                    </div>
                </div>
            </section>
        </footer>

        <script type="text/javascript">
            function switch_language(language) {
                $.ajax({
                    url: 'http://localhost/home/site_language',
                    type: 'post',
                    data: {language: language},
                    success: function (response) {
                        setTimeout(function () {
                            location.reload();
                        }, 500);
                    }
                });
            }
        </script>



        <!-- PAYMENT MODAL -->
        <!-- Modal -->

        <!-- Modal -->
        <div class="modal fade multi-step" id="EditRatingModal" tabindex="-1" role="dialog" aria-hidden="true" reset-on-close="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content edit-rating-modal">
                    <div class="modal-header">
                        <h5 class="modal-title step-1" data-step="1">Step 1</h5>
                        <h5 class="modal-title step-2" data-step="2">Step 2</h5>
                        <h5 class="m-progress-stats modal-title">
                            &nbsp;of&nbsp;<span class="m-progress-total"></span>
                        </h5>

                        <button type="button" class="close" data-bs-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="m-progress-bar-wrapper">
                        <div class="m-progress-bar">
                        </div>
                    </div>
                    <div class="modal-body step step-1">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="modal-rating-box">
                                        <h4 class="rating-title">How would you rate this course overall?</h4>
                                        <fieldset class="your-rating">

                                            <input type="radio" id="star5" name="rating" value="5" />
                                            <label class = "full" for="star5"></label>

                                            <!-- <input type="radio" id="star4half" name="rating" value="4 and a half" />
                                            <label class="half" for="star4half"></label> -->

                                            <input type="radio" id="star4" name="rating" value="4" />
                                            <label class = "full" for="star4"></label>

                                            <!-- <input type="radio" id="star3half" name="rating" value="3 and a half" />
                                            <label class="half" for="star3half"></label> -->

                                            <input type="radio" id="star3" name="rating" value="3" />
                                            <label class = "full" for="star3"></label>

                                            <!-- <input type="radio" id="star2half" name="rating" value="2 and a half" />
                                            <label class="half" for="star2half"></label> -->

                                            <input type="radio" id="star2" name="rating" value="2" />
                                            <label class = "full" for="star2"></label>

                                            <!-- <input type="radio" id="star1half" name="rating" value="1 and a half" />
                                            <label class="half" for="star1half"></label> -->

                                            <input type="radio" id="star1" name="rating" value="1" />
                                            <label class = "full" for="star1"></label>

                                            <!-- <input type="radio" id="starhalf" name="rating" value="half" />
                                            <label class="half" for="starhalf"></label> -->

                                        </fieldset>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="modal-course-preview-box">
                                        <div class="card">
                                            <img class="card-img-top img-fluid" id = "course_thumbnail_1" alt="">
                                            <div class="card-body">
                                                <h5 class="card-title" class = "course_title_for_rating" id = "course_title_1"></h5>
                                                <p class="card-text" id = "instructor_details">

                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-body step step-2">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="modal-rating-comment-box">
                                        <h4 class="rating-title">Write a public review</h4>
                                        <textarea id = "review_of_a_course" name = "review_of_a_course" placeholder="Describe your experience what you got out of the course and other helpful highlights. What did the instructor do well and what could use some improvement?" maxlength="65000" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="modal-course-preview-box">
                                        <div class="card">
                                            <img class="card-img-top img-fluid" id = "course_thumbnail_2" alt="">
                                            <div class="card-body">
                                                <h5 class="card-title" class = "course_title_for_rating" id = "course_title_2"></h5>
                                                <p class="card-text">
                                                    -
                                                    Tien Nguyen                    </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="course_id" id = "course_id_for_rating" value="">
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary next step step-1" data-step="1" onclick="sendEvent(2)">Next</button>
                        <button type="button" class="btn btn-primary previous step step-2 mr-auto" data-step="2" onclick="sendEvent(1)">Previous</button>
                        <button type="button" class="btn btn-primary publish step step-2" onclick="publishRating($('#course_id_for_rating').val())" id = "">Publish</button>
                    </div>
                </div>
            </div>
        </div><!-- Modal -->


        <script src="./js/modernizr-3.5.0.min.js"></script>
        <script src="./js/jquery-3.2.1.min.js"></script>
        <script src="./js/popper.min.js"></script>
        <script src="./js/bootstrap.min.js"></script>



        <script src="./js/main.js"></script>
        <script src="./toastr.min.js"></script>
        <script src="./jquery.form.min.js"></script>
        <script src="./js/jQuery.tagify.js"></script>

        <!-- SHOW TOASTR NOTIFIVATION -->


        <script type="text/javascript">
                    $(function () {
                        $('[data-bs-toggle="tooltip"]').tooltip()
                    });
                    if ($('.tagify').height()) {
                        $('.tagify').tagify();
                    }
        </script><script type="text/javascript">
            function showAjaxModal(url)
            {
                // SHOWING AJAX PRELOADER IMAGE
                jQuery('#modal_ajax .modal-body').html('<div class="w-100 text-center pt-5"><img class="mt-5 mb-5" width="80px" src="http://localhost/assets/global/gif/page-loader-2.gif"></div>');

                // LOADING THE AJAX MODAL
                jQuery('#modal_ajax').modal('show', {backdrop: 'true'});

                // SHOW AJAX RESPONSE ON REQUEST SUCCESS
                $.ajax({
                    url: url,
                    success: function (response)
                    {
                        jQuery('#modal_ajax .modal-body').html(response);
                    }
                });
            }

            function lesson_preview(url, title) {
                // SHOWING AJAX PRELOADER IMAGE
                jQuery('.lesson_preview_header').html(title);
                jQuery('#lesson_preview .modal-body').html('<div class="w-100 text-center pt-5"><img class="mt-5 mb-5" width="80px" src="http://localhost/assets/global/gif/page-loader-2.gif"></div>');

                // LOADING THE AJAX MODAL
                jQuery('#lesson_preview').modal('show', {backdrop: 'true'});

                // SHOW AJAX RESPONSE ON REQUEST SUCCESS
                $.ajax({
                    url: url,
                    success: function (response)
                    {
                        jQuery('#lesson_preview .modal-body').html(response);
                    }
                });
            }
        </script>

        <!-- (Ajax Modal)-->
        <div class="modal fade" id="modal_ajax">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-bs-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body" style="overflow:auto;">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-bs-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="lesson_preview" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content course-preview-modal">
                    <div class="modal-header">
                        <h5 class="lesson_preview_header"></h5>
                        <button type="button" class="close" data-bs-dismiss="modal" onclick="window.location.reload()">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                    </div>
                </div>
            </div>
        </div>




        <script type="text/javascript">
            function confirm_modal(delete_url)
            {
                jQuery('#modal-4').modal('show', {backdrop: 'static'});
                document.getElementById('delete_link').setAttribute('href', delete_url);
            }
        </script>

        <!-- (Normal Modal)-->
        <div class="modal fade" id="modal-4">
            <div class="modal-dialog">
                <div class="modal-content" style="margin-top:100px;">

                    <div class="modal-header">
                        <h4 class="modal-title text-center">Are you sure ?</h4>
                        <button type="button" class="btn btn-outline-secondary px-1 py-0" data-bs-dismiss="modal" aria-hidden="true"><i class="fas fa-times-circle"></i></button>
                    </div>


                    <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
                        <a href="#" class="btn btn-danger btn-yes" id="delete_link">Yes</a>
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>


        <script type="text/javascript">
            function async_modal() {
                const asyncModal = new Promise(function (resolve, reject) {
                    $('#modal-4').modal('show');
                    $('#modal-4 .btn-yes').click(function () {
                        resolve(true);
                    });
                    $('#modal-4 .btn-cancel').click(function () {
                        resolve(false);
                    });
                });
                return asyncModal;
            }
        </script>
        <script type="text/javascript">
            function toggleRatingView(course_id) {
                $('#course_info_view_' + course_id).toggle();
                $('#course_rating_view_' + course_id).toggle();
                $('#edit_rating_btn_' + course_id).toggle();
                $('#cancel_rating_btn_' + course_id).toggle();
            }

            function publishRating(course_id) {
                var review = $('#review_of_a_course_' + course_id).val();
                var starRating = 0;
                starRating = $('#star_rating_of_course_' + course_id).val();
                if (starRating > 0) {
                    $.ajax({
                        type: 'POST',
                        url: 'http://localhost/home/rate_course',
                        data: {course_id: course_id, review: review, starRating: starRating},
                        success: function (response) {
                            location.reload();
                        }
                    });
                } else {

                }
            }

            function isTouchDevice() {
                return (('ontouchstart' in window) ||
                        (navigator.maxTouchPoints > 0) ||
                        (navigator.msMaxTouchPoints > 0));
            }

            function viewMore(element, visibility) {
                if (visibility == "hide") {
                    $(element).parent(".view-more-parent").addClass("expanded");
                    $(element).remove();
                } else if ($(element).hasClass("view-more")) {
                    $(element).parent(".view-more-parent").addClass("expanded has-hide");
                    $(element)
                            .removeClass("view-more")
                            .addClass("view-less")
                            .html("- View less");
                } else if ($(element).hasClass("view-less")) {
                    $(element).parent(".view-more-parent").removeClass("expanded has-hide");
                    $(element)
                            .removeClass("view-less")
                            .addClass("view-more")
                            .html("+ View more");
                }
            }

            function redirect_to(url) {
                if (!isTouchDevice() && $(window).width() > 767) {
                    window.location.replace(url);
                }
            }

        //Event call after loading page
            document.addEventListener('DOMContentLoaded', function () {
                setTimeout(function () {
                    $('.animated-loader').hide();
                    $('.shown-after-loading').show();
                });
            }, false);


            function check_action(e, url) {
                var tag = $(e).prop("tagName").toLowerCase();
                if (tag == 'a') {
                    return true;
                } else if (tag != 'a' && url) {
                    $(location).attr('href', url);
                    return false;
                } else {
                    return true;
                }
            }
        </script>
    </body>
</html>
