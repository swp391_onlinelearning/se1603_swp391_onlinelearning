USE [master]
GO
/****** Object:  Database [OnlineLearn3]    Script Date: 6/30/2022 9:58:03 AM ******/
CREATE DATABASE [OnlineLearn3]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'OnlineLearn3', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\OnlineLearn3.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'OnlineLearn3_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\OnlineLearn3_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [OnlineLearn3] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [OnlineLearn3].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [OnlineLearn3] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [OnlineLearn3] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [OnlineLearn3] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [OnlineLearn3] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [OnlineLearn3] SET ARITHABORT OFF 
GO
ALTER DATABASE [OnlineLearn3] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [OnlineLearn3] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [OnlineLearn3] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [OnlineLearn3] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [OnlineLearn3] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [OnlineLearn3] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [OnlineLearn3] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [OnlineLearn3] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [OnlineLearn3] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [OnlineLearn3] SET  DISABLE_BROKER 
GO
ALTER DATABASE [OnlineLearn3] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [OnlineLearn3] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [OnlineLearn3] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [OnlineLearn3] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [OnlineLearn3] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [OnlineLearn3] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [OnlineLearn3] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [OnlineLearn3] SET RECOVERY FULL 
GO
ALTER DATABASE [OnlineLearn3] SET  MULTI_USER 
GO
ALTER DATABASE [OnlineLearn3] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [OnlineLearn3] SET DB_CHAINING OFF 
GO
ALTER DATABASE [OnlineLearn3] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [OnlineLearn3] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [OnlineLearn3] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [OnlineLearn3] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'OnlineLearn3', N'ON'
GO
ALTER DATABASE [OnlineLearn3] SET QUERY_STORE = OFF
GO
USE [OnlineLearn3]
GO
/****** Object:  Table [dbo].[Blog_category]    Script Date: 6/30/2022 9:58:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Blog_category](
	[blog_category_id] [int] NOT NULL,
	[title] [varchar](255) NOT NULL,
 CONSTRAINT [PK_BLOG_CATEGORY] PRIMARY KEY CLUSTERED 
(
	[blog_category_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[blog_comments]    Script Date: 6/30/2022 9:58:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[blog_comments](
	[id] [int] NOT NULL,
	[blog_id] [int] NOT NULL,
	[user_id] [int] NOT NULL,
	[comment] [varchar](255) NOT NULL,
 CONSTRAINT [PK_BLOG_COMMENTS] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Blogs]    Script Date: 6/30/2022 9:58:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Blogs](
	[blog_id] [int] NOT NULL,
	[blog_category_id] [int] NOT NULL,
	[user_id] [int] NOT NULL,
	[title] [varchar](255) NOT NULL,
	[description] [varchar](max) NOT NULL,
	[thumbnail] [varchar](255) NOT NULL,
	[banner] [varchar](255) NOT NULL,
	[added_date] [varchar](255) NOT NULL,
	[short_description] [nvarchar](max) NULL,
 CONSTRAINT [PK_BLOGS] PRIMARY KEY CLUSTERED 
(
	[blog_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[course]    Script Date: 6/30/2022 9:58:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[course](
	[id] [int] NOT NULL,
	[title] [varchar](255) NOT NULL,
	[short_description] [varchar](255) NOT NULL,
	[description] [varchar](max) NOT NULL,
	[category_id] [int] NOT NULL,
	[price] [float] NOT NULL,
	[thumbnail] [varchar](255) NOT NULL,
	[is_free_course] [int] NOT NULL,
 CONSTRAINT [PK_COURSE] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[course_category]    Script Date: 6/30/2022 9:58:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[course_category](
	[id] [int] NOT NULL,
	[name] [varchar](255) NOT NULL,
	[thumbnail] [varchar](255) NOT NULL,
 CONSTRAINT [PK_COURSE_CATEGORY] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[enrol]    Script Date: 6/30/2022 9:58:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[enrol](
	[id] [int] NOT NULL,
	[user_id] [int] NOT NULL,
	[course_id] [int] NOT NULL,
	[date_added] [datetime] NOT NULL,
 CONSTRAINT [PK_ENROL] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[frontend_settings]    Script Date: 6/30/2022 9:58:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[frontend_settings](
	[id] [int] NOT NULL,
	[setting] [varchar](255) NOT NULL,
	[value] [varchar](255) NOT NULL,
 CONSTRAINT [PK_FRONTEND_SETTINGS] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lesson]    Script Date: 6/30/2022 9:58:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lesson](
	[id] [int] NOT NULL,
	[title] [varchar](255) NOT NULL,
	[course_id] [int] NOT NULL,
	[section_id] [int] NOT NULL,
	[video_url] [varchar](255) NOT NULL,
	[lesson_type] [varchar](255) NOT NULL,
	[attachment] [varchar](255) NOT NULL,
	[summary] [varchar](255) NOT NULL,
 CONSTRAINT [PK_LESSON] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[payment]    Script Date: 6/30/2022 9:58:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[payment](
	[id] [int] NOT NULL,
	[user_id] [int] NOT NULL,
	[course_id] [int] NOT NULL,
	[amount] [float] NOT NULL,
	[date_added] [datetime] NOT NULL,
 CONSTRAINT [PK_PAYMENT] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[question]    Script Date: 6/30/2022 9:58:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[question](
	[id] [int] NOT NULL,
	[quiz_id] [int] NOT NULL,
	[title] [varchar](255) NOT NULL,
	[type] [varchar](255) NOT NULL,
	[number_of_options] [int] NOT NULL,
	[options] [varchar](255) NOT NULL,
	[correct_answers] [varchar](255) NOT NULL,
 CONSTRAINT [PK_QUESTION] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[rating]    Script Date: 6/30/2022 9:58:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[rating](
	[id] [int] NOT NULL,
	[rating] [int] NOT NULL,
	[user_id] [int] NOT NULL,
	[course_id] [int] NOT NULL,
	[review] [varchar](255) NOT NULL,
 CONSTRAINT [PK_RATING] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 6/30/2022 9:58:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[id] [int] NOT NULL,
	[name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ROLE] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[section]    Script Date: 6/30/2022 9:58:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[section](
	[id] [int] NOT NULL,
	[title] [varchar](255) NOT NULL,
	[course_id] [int] NOT NULL,
 CONSTRAINT [PK_SECTION] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[settings]    Script Date: 6/30/2022 9:58:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[settings](
	[id] [int] NOT NULL,
	[seting] [varchar](255) NOT NULL,
	[value] [varchar](255) NOT NULL,
 CONSTRAINT [PK_SETTINGS] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 6/30/2022 9:58:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[id] [int] NOT NULL,
	[first_name] [varchar](50) NOT NULL,
	[last_name] [varchar](50) NOT NULL,
	[email] [varchar](50) NOT NULL,
	[password] [varchar](255) NOT NULL,
	[role_id] [int] NOT NULL,
	[status] [int] NOT NULL,
	[image] [varchar](255) NOT NULL,
 CONSTRAINT [PK_USER] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Blog_category] ([blog_category_id], [title]) VALUES (1, N'News')
INSERT [dbo].[Blog_category] ([blog_category_id], [title]) VALUES (2, N'Update')
INSERT [dbo].[Blog_category] ([blog_category_id], [title]) VALUES (7, N'Test 2')
GO
INSERT [dbo].[Blogs] ([blog_id], [blog_category_id], [user_id], [title], [description], [thumbnail], [banner], [added_date], [short_description]) VALUES (1, 1, 0, N'Test news', N'mo ta dai', N'https://s.dou.ua/img/announces/java_1.jpg', N'https://s.dou.ua/img/announces/java_1.jpg', N'6/5/2022', N'Mo ta ngan')
INSERT [dbo].[Blogs] ([blog_id], [blog_category_id], [user_id], [title], [description], [thumbnail], [banner], [added_date], [short_description]) VALUES (2, 1, 0, N'Test news 2', N'mo ta dai', N'https://www.nilebits.com/wp-content/uploads/2020/11/How-to-generate-thumbnail-image-in-C.NET_.png', N'https://www.nilebits.com/wp-content/uploads/2020/11/How-to-generate-thumbnail-image-in-C.NET_.png', N'6/5/2022', N'Mo ta ngan 2')
INSERT [dbo].[Blogs] ([blog_id], [blog_category_id], [user_id], [title], [description], [thumbnail], [banner], [added_date], [short_description]) VALUES (3, 1, 0, N'Test news 3', N'mo ta dai ', N'https://www.lilengine.co/sites/default/files/styles/banner_image_1300x400/public/stockart/2017-09/code-1839406_1920.jpg?itok=ScSCiDtP', N'https://www.lilengine.co/sites/default/files/styles/banner_image_1300x400/public/stockart/2017-09/code-1839406_1920.jpg?itok=ScSCiDtP', N'6/5/2022', N'Mo ta ngan 3')
GO
INSERT [dbo].[course] ([id], [title], [short_description], [description], [category_id], [price], [thumbnail], [is_free_course]) VALUES (1, N'java', N'Mo ta ngan java', N'<span style="color: rgb(77, 81, 86); font-family: arial, sans-serif;">Java là m&#7897;t ngôn ng&#7919; l&#7853;p trình h&#432;&#7899;ng &#273;&#7889;i t&#432;&#7907;ng, d&#7921;a trên l&#7899;p &#273;&#432;&#7907;c thi&#7871;t k&#7871; &#273;&#7875; có càng ít ph&#7909; thu&#7897;c th&#7921;c thi càng t&#7889;t.</span>', 1, 2000, N'null', 1)
INSERT [dbo].[course] ([id], [title], [short_description], [description], [category_id], [price], [thumbnail], [is_free_course]) VALUES (4, N'C#', N'C# mo ta ngan', N'C# mo ta dai', 1, 20000, N'null', 1)
GO
INSERT [dbo].[course_category] ([id], [name], [thumbnail]) VALUES (1, N'Code', N'Null')
INSERT [dbo].[course_category] ([id], [name], [thumbnail]) VALUES (2, N'Design', N'null')
GO
INSERT [dbo].[enrol] ([id], [user_id], [course_id], [date_added]) VALUES (0, 1, 1, CAST(N'2008-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[lesson] ([id], [title], [course_id], [section_id], [video_url], [lesson_type], [attachment], [summary]) VALUES (1, N'java là gì', 1, 1, N'null', N'null', N'null', N'1')
INSERT [dbo].[lesson] ([id], [title], [course_id], [section_id], [video_url], [lesson_type], [attachment], [summary]) VALUES (2, N'Câu h?i java ', 1, 1, N'null', N'null', N'null', N'null')
INSERT [dbo].[lesson] ([id], [title], [course_id], [section_id], [video_url], [lesson_type], [attachment], [summary]) VALUES (3, N'Java co b?n', 1, 2, N'null', N'null', N'null', N'1')
GO
INSERT [dbo].[Role] ([id], [name]) VALUES (1, N'Admin')
INSERT [dbo].[Role] ([id], [name]) VALUES (2, N'User')
GO
INSERT [dbo].[section] ([id], [title], [course_id]) VALUES (1, N'Bài 1: Java là gì?', 1)
INSERT [dbo].[section] ([id], [title], [course_id]) VALUES (2, N'Bài 2: java co b?n?', 1)
GO
INSERT [dbo].[User] ([id], [first_name], [last_name], [email], [password], [role_id], [status], [image]) VALUES (0, N'Minh123', N'Ky', N'gennikmk@gmail.com', N'123123', 2, 1, N'aaa')
INSERT [dbo].[User] ([id], [first_name], [last_name], [email], [password], [role_id], [status], [image]) VALUES (1, N'ky', N'minh123', N'gennikemk@gmail.com', N'123123!', 1, 1, N'null')
INSERT [dbo].[User] ([id], [first_name], [last_name], [email], [password], [role_id], [status], [image]) VALUES (3, N'nguyen', N'tien', N'nguyentien222222@gmail.com', N'123123', 1, 1, N'null')
GO
ALTER TABLE [dbo].[blog_comments]  WITH CHECK ADD  CONSTRAINT [blog_comments_fk0] FOREIGN KEY([blog_id])
REFERENCES [dbo].[Blogs] ([blog_id])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[blog_comments] CHECK CONSTRAINT [blog_comments_fk0]
GO
ALTER TABLE [dbo].[Blogs]  WITH CHECK ADD  CONSTRAINT [Blogs_fk0] FOREIGN KEY([blog_category_id])
REFERENCES [dbo].[Blog_category] ([blog_category_id])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Blogs] CHECK CONSTRAINT [Blogs_fk0]
GO
ALTER TABLE [dbo].[Blogs]  WITH CHECK ADD  CONSTRAINT [Blogs_fk1] FOREIGN KEY([user_id])
REFERENCES [dbo].[User] ([id])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Blogs] CHECK CONSTRAINT [Blogs_fk1]
GO
ALTER TABLE [dbo].[course]  WITH CHECK ADD  CONSTRAINT [course_fk0] FOREIGN KEY([category_id])
REFERENCES [dbo].[course_category] ([id])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[course] CHECK CONSTRAINT [course_fk0]
GO
ALTER TABLE [dbo].[enrol]  WITH CHECK ADD  CONSTRAINT [enrol_fk0] FOREIGN KEY([user_id])
REFERENCES [dbo].[User] ([id])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[enrol] CHECK CONSTRAINT [enrol_fk0]
GO
ALTER TABLE [dbo].[enrol]  WITH CHECK ADD  CONSTRAINT [enrol_fk1] FOREIGN KEY([course_id])
REFERENCES [dbo].[course] ([id])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[enrol] CHECK CONSTRAINT [enrol_fk1]
GO
ALTER TABLE [dbo].[lesson]  WITH CHECK ADD  CONSTRAINT [lesson_fk0] FOREIGN KEY([course_id])
REFERENCES [dbo].[course] ([id])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[lesson] CHECK CONSTRAINT [lesson_fk0]
GO
ALTER TABLE [dbo].[lesson]  WITH CHECK ADD  CONSTRAINT [lesson_fk1] FOREIGN KEY([section_id])
REFERENCES [dbo].[section] ([id])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[lesson] CHECK CONSTRAINT [lesson_fk1]
GO
ALTER TABLE [dbo].[payment]  WITH CHECK ADD  CONSTRAINT [payment_fk0] FOREIGN KEY([user_id])
REFERENCES [dbo].[User] ([id])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[payment] CHECK CONSTRAINT [payment_fk0]
GO
ALTER TABLE [dbo].[payment]  WITH CHECK ADD  CONSTRAINT [payment_fk1] FOREIGN KEY([course_id])
REFERENCES [dbo].[course] ([id])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[payment] CHECK CONSTRAINT [payment_fk1]
GO
ALTER TABLE [dbo].[question]  WITH CHECK ADD  CONSTRAINT [question_fk0] FOREIGN KEY([quiz_id])
REFERENCES [dbo].[lesson] ([id])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[question] CHECK CONSTRAINT [question_fk0]
GO
ALTER TABLE [dbo].[rating]  WITH CHECK ADD  CONSTRAINT [rating_fk0] FOREIGN KEY([user_id])
REFERENCES [dbo].[User] ([id])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[rating] CHECK CONSTRAINT [rating_fk0]
GO
ALTER TABLE [dbo].[rating]  WITH CHECK ADD  CONSTRAINT [rating_fk1] FOREIGN KEY([course_id])
REFERENCES [dbo].[course] ([id])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[rating] CHECK CONSTRAINT [rating_fk1]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [User_fk0] FOREIGN KEY([role_id])
REFERENCES [dbo].[Role] ([id])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [User_fk0]
GO
USE [master]
GO
ALTER DATABASE [OnlineLearn3] SET  READ_WRITE 
GO
