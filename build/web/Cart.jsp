

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Shopping Cart | NewSite</title>


        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5.0, minimum-scale=0.86">
        <meta name="author" content="Creativeitem" />

        <meta name="keywords" content="LMS,Learning Management System,Creativeitem,demo,hello,How are you"/>
        <meta name="description" content="Study any topic, anytime. explore thousands of courses for the lowest price ever!" />

        <!--Social sharing content-->
        <meta property="og:title" content="Shopping cart" />
        <meta property="og:image" content="./img/home-banner.jpg">
        <meta property="og:url" content="http://localhost/home/shopping_cart" />
        <meta property="og:type" content="Learning management system" />
        <!--Social sharing content-->


        <!--Drips icons-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dripicons/2.0.0/webfont.min.css" integrity="sha512-pi7KSLdGMxSE62WWJ62B1R5/H7WNnIsj2f51MikplRt31K0uCZ1lfPSw/0Jb1flSz6Ed2YLSlox6Uulf7CaFiA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dripicons/2.0.0/webfont.min.css" integrity="sha512-pi7KSLdGMxSE62WWJ62B1R5/H7WNnIsj2f51MikplRt31K0uCZ1lfPSw/0Jb1flSz6Ed2YLSlox6Uulf7CaFiA==" crossorigin="anonymous" referrerpolicy="no-referrer" />

        <!--Material Design Icon-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/2.0.46/css/materialdesignicons.min.css" integrity="sha512-HmBTsbqKSDy0wIk8SGSCj68xUg8b22mGtXx8cXF64qcmnQnJepz6Aq37X43gF/WhbvqPcx68GoiaWu8wE8/y4g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/2.0.46/css/materialdesignicons.css" integrity="sha512-OSd9YjMibKZ0CN1njc/7H/IK8zmRevGk+n/Z+jAyClgFZ+sOvQKzmeJKBKW313Ln+630pAlcCMjX1dzTDGe4+w==" crossorigin="anonymous" referrerpolicy="no-referrer" />


        <link name="favicon" type="image/x-icon" href="./img/favicon.png" rel="shortcut icon" />
        <link rel="favicon" href="./js/favicon.ico">
        <link rel="apple-touch-icon" href="./img/icon.png">

        <!-- font awesome 5 -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.3.1/css/all.min.css" integrity="sha512-KulI0psuJQK8UMpOeiMLDXJtGOZEBm8RZNTyBBHIWqoXoPMFcw+L5AEo0YMpsW8BfiuWrdD1rH6GWGgQBF59Lg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="./css/fontawesome-all.min.css">

        <link rel="stylesheet" href="./css/bootstrap.min.css">

        <link rel="stylesheet" href="./css/main.css">
        <link rel="stylesheet" href="./css/responsive.css">
        <link rel="stylesheet" href="./css/custom.css">
        <link rel="stylesheet" href="./css/custom.css">
        <link rel="stylesheet" href="./css/tagify.css">
        <link rel="stylesheet" href="./toastr.css">
        <script src="./js/jquery-3.3.1.min.js"></script>
    </head>
    <body class="gray-bg">
        <section class="menu-area bg-white">
            <div class="container-xl">
                <nav class="navbar navbar-expand-lg navbar-light">

                    <ul class="mobile-header-buttons">
                        <li><a class="mobile-nav-trigger" href="#mobile-primary-nav">Menu<span></span></a></li>
                        <li><a class="mobile-search-trigger" href="#mobile-search">Search<span></span></a></li>
                    </ul>

                    <a href="http://localhost/" class="navbar-brand" href="#">
                        <img src="./img/logo-dark.png" alt="" height="35">
                    </a>

                    <div class="main-nav-wrap">
                        <div class="mobile-overlay"></div>
                        <style type="text/css">
                            @media only screen and (max-width: 767px) {
                                .category.corner-triangle.top-left.pb-0.is-hidden{
                                    display: none !important;
                                }
                                .sub-category.is-hidden{
                                    display: none !important;
                                }
                            }
                        </style>

                        <ul class="mobile-main-nav">
                            <div class="mobile-menu-helper-top"></div>
                            <li class="has-children text-nowrap fw-bold">
                                <a href="">
                                    <i class="fas fa-th d-inline text-20px"></i>
                                    <span class="fw-500">Categories</span>
                                    <span class="has-sub-category"><i class="fas fa-angle-right"></i></span>
                                </a>

                                <ul class="category corner-triangle top-left is-hidden pb-0" >
                                    <li class="go-back"><a href=""><i class="fas fa-angle-left"></i>Menu</a></li>

                                    <li class="has-children">
                                        <a href="javascript:;" class="py-2 text-wrap" onclick="redirect_to('http://localhost/home/courses?category=asdasdasd')">
                                            <span class="icon"><i class="asdasdasd"></i></span>
                                            <span>asdasdasd</span>
                                            <span class="has-sub-category"><i class="fas fa-angle-right"></i></span>
                                        </a>
                                        <ul class="sub-category is-hidden">
                                            <li class="go-back-menu"><a href=""><i class="fas fa-angle-left"></i>Menu</a></li>
                                            <li class="go-back"><a href="">
                                                    <i class="fas fa-angle-left"></i>
                                                    <span class="icon"><i class="asdasdasd"></i></span>
                                                    asdasdasd              </a></li>
                                            <li><a class="text-wrap" href="http://localhost/home/courses?category=react">React</a></li>
                                            <li><a class="text-wrap" href="http://localhost/home/courses?category=linh-tịnh">Linh tịnh</a></li>
                                        </ul>
                                    </li>
                                    <li class="all-category-devided mb-0 p-0">
                                        <a href="http://localhost/home/courses" class="py-3">
                                            <span class="icon"><i class="fa fa-align-justify"></i></span>
                                            <span>All courses</span>
                                        </a>
                                    </li>


                                </ul>
                            </li>

                            <div class="mobile-menu-helper-bottom"></div>
                        </ul>
                    </div>


                    <form class="inline-form" action="http://localhost/home/search" method="get" style="width: 100%;">
                        <div class="input-group search-box mobile-search">
                            <input type="text" name='query' class="form-control" placeholder="Search for courses">
                            <div class="input-group-append">
                                <button class="btn" type="submit"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </form>

                    <div class="instructor-box menu-icon-box ms-md-4">
                        <div class="icon">
                            <a href="http://localhost/user" style="border: 1px solid transparent; margin: 0px;     padding: 0px 10px; font-size: 14px; width: max-content; border-radius: 5px; height: 40px; line-height: 40px;">Instructor</a>
                        </div>
                    </div>

                    <div class="instructor-box menu-icon-box">
                        <div class="icon">
                            <a href="http://localhost/home/my_courses" style="border: 1px solid transparent; margin: 0px;     padding: 0px 10px; font-size: 14px; width: max-content; border-radius: 5px; height: 40px; line-height: 40px;">My courses</a>
                        </div>
                    </div>

                    <div class="wishlist-box menu-icon-box" id="wishlist_items">
                        <div class="icon">
                            <a href=""><i class="far fa-heart"></i></a>
                            <span class="number">1</span>
                        </div>
                        <div class="dropdown course-list-dropdown corner-triangle top-right">
                            <div class="list-wrapper">
                                <div class="item-list">
                                    <ul>
                                        <li>
                                            <div class="item clearfix">
                                                <div class="item-image">
                                                    <a href="">
                                                        <img src="./img/course_thumbnail_default_1.jpg" alt="" class="img-fluid">
                                                    </a>
                                                </div>
                                                <div class="item-details">
                                                    <a href="http://localhost/home/course/300-b%C3%A0i-code-thi%E1%BA%BFu-nhi/1">
                                                        <div class="course-name">300 bài code thiếu nhi</div>
                                                        <div class="instructor-name">
                                                            By                                                                             </div>

                                                        <div class="item-price">
                                                            <span class="current-price">$10</span>
                                                        </div>
                                                    </a>
                                                    <button type="button" id="1" onclick="handleCartItems(this)" class="addedToCart">
                                                        Added to cart                                        </button>
                                                </div>
                                                
                                                
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="dropdown-footer">
                                    <a href="http://localhost/home/my_wishlist">Go to wishlist</a>
                                </div>
                            </div>
                            <div class="empty-box text-center d-none">
                                <p>Your wishlist is empty.</p>
                                <a href="">Explore courses</a>
                            </div>
                        </div>            </div>

                    <div class="cart-box menu-icon-box" id="cart_items">
                        <div class="icon">
                            <a href="http://localhost/home/shopping_cart"><i class="fas fa-shopping-cart"></i></a>
                            <span class="number">1</span>
                        </div>

                        <!-- Cart Dropdown goes here -->
                                 </div>



                    <div class="user-box menu-icon-box">
                        <div class="icon">
                            <a href="javascript::">
                                <img src="./img/placeholder.png" alt="" class="img-fluid">
                            </a>
                        </div>
                        <div class="dropdown user-dropdown corner-triangle top-right radius-10">
                            <ul class="user-dropdown-menu radius-10">

                                <li class="dropdown-user-info">
                                    <a href="javascript:;" class="radius-top-10">
                                        <div class="clearfix">
                                            <div class="user-image float-start">
                                                <img src="./img/placeholder.png" alt="">
                                            </div>
                                            <div class="user-details">
                                                <div class="user-name">
                                                    <span class="hi">Hi,</span>
                                                    Pho DayHanh                                        </div>
                                                <div class="user-email">
                                                    <span class="email">abcdef@gmail.com</span>
                                                    <span class="welcome">Welcome back</span>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </li>

                                <li class="user-dropdown-menu-item"><a href="http://localhost/home/my_courses"><i class="far fa-gem"></i>My courses</a></li>
                                <li class="user-dropdown-menu-item"><a href="http://localhost/home/my_wishlist"><i class="far fa-heart"></i>My wishlist</a></li>
                                <li class="user-dropdown-menu-item"><a href="http://localhost/home/my_messages"><i class="far fa-envelope"></i>My messages</a></li>
                                <li class="user-dropdown-menu-item"><a href="http://localhost/home/purchase_history"><i class="fas fa-shopping-cart"></i>Purchase history</a></li>
                                <li class="user-dropdown-menu-item"><a href="http://localhost/home/profile/user_profile"><i class="fas fa-user"></i>User profile</a></li>

                                <li class="dropdown-user-logout user-dropdown-menu-item radius-bottom-10"><a class="radius-bottom-10 py-3" href="http://localhost/login/logout"><i class="fas fa-sign-out-alt"></i> Log out</a></li>
                            </ul>
                        </div>
                    </div>



                    <span class="signin-box-move-desktop-helper"></span>
                    <div class="sign-in-box btn-group d-none">

                        <button type="button" class="btn btn-sign-in" data-toggle="modal" data-target="#signInModal">Log In</button>

                        <button type="button" class="btn btn-sign-up" data-toggle="modal" data-target="#signUpModal">Sign Up</button>

                    </div> <!--  sign-in-box end -->


                </nav>
            </div>
        </section><link rel="stylesheet" type="text/css" href="http://localhost/assets/frontend/eu-cookie/purecookie.css" async />

        <div class="cookieConsentContainer" id="cookieConsentContainer" style="opacity: .9; display: block; display: none;">
            <!-- <div class="cookieTitle">
              <a>Cookies.</a>
            </div> -->
            <div class="cookieDesc">
                <p>
                    This website uses cookies to personalize content and analyse traffic in order to offer you a better experience.      <a class="link-cookie-policy" href="http://localhost/home/cookie_policy">Cookie policy</a>
                </p>
            </div>
            <div class="cookieButton">
                <a onclick="cookieAccept();">Accept</a>
            </div>
        </div>
        <script>
            $(document).ready(function () {
                if (localStorage.getItem("accept_cookie_academy")) {
                    //localStorage.removeItem("accept_cookie_academy");
                } else {
                    $('#cookieConsentContainer').fadeIn(1000);
                }
            });

            function cookieAccept() {
                if (typeof (Storage) !== "undefined") {
                    localStorage.setItem("accept_cookie_academy", true);
                    localStorage.setItem("accept_cookie_time", "06/16/2022");
                    $('#cookieConsentContainer').fadeOut(1200);
                }
            }
        </script>
        <section class="category-header-area" style="background-image: url('./img/shopping_cart.png');
                 background-size: contain;
                 background-repeat: no-repeat;
                 background-position-x: right;
                 background-color: #ec5252;">
            <div class="image-placeholder-1"></div>
            <div class="container-lg breadcrumb-container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item display-6 fw-bold">
                            <a href="http://localhost/home">
                                Home                </a>
                        </li>
                        <li class="breadcrumb-item active text-light display-6 fw-bold">
                            Shopping cart            </li>
                    </ol>
                </nav>
            </div>
        </section>

        <section class="cart-list-area">
            <div class="container">
                <div class="row" id="cart_items_details">
                    <div class="col-lg-9">
                        <div class="in-cart-box">
                            <div class="title">Courses in cart</div>
                            <div class="">
                                <ul class="cart-course-list">
                            
                                    <c:forEach items="${list}" var="o">
                                    <li>
                                        <input type="hidden" id="txtCourseID" name="txtCourseID" value="${o.id}">
                                        <div class="cart-course-wrapper">
                                            <div class="image d-none d-md-block">
                                                <a href="CourseDetail?id=${o.id}">
                                                    <img src="./img/course_thumbnail_default_1.jpg" alt="" class="img-fluid">
                                                </a>
                                            </div>
                                            <div class="details">
                                                <a href="CourseDetail?id=${o.id}">
                                                    <div class="name">${o.title}</div>
                                                </a>

                                                <div class="course-subtitle text-13px mt-2">
                                                    ${o.short_description}                                  </div>

                                                <div class="floating-user d-inline-block mt-2">
                                                    <img class=" cursor-pointer" src="./img/placeholder.png" width="30px" data-bs-toggle="tooltip" data-bs-placement="top" title="" onclick="event.stopPropagation(); $(location).attr('href', 'http://localhost/home/instructor_page/1');">
                                                </div>
                                            </div>
                                            <div class="move-remove text-center">
                                                <div id="1"><i class="fas fa-times-circle"></i><a href="CartRemove?id=${o.id}"> Remove</a></div>
                                                <!-- <div>Move to Wishlist</div> -->
                                            </div>
                                            <div class="price">
                                                <div class="current-price">
                                                    $${o.price}                                     </div>
                                            </div>
                                        </div>
                                    </li>
                                    </c:forEach>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 pt-1">
                        <h5 class="fw-700">Total:</h5>
                        <div class="cart-sidebar bg-white radius-10 py-4 px-3">
                            <span id="total_price_of_checking_out" hidden>10            </span>
                            <div class="total-price">$${sum}</div>
                            <div class="total-original-price">
                                <span class="original-price">
                                </span>
                                <!-- <span class="discount-rate">95% off</span> -->
                            </div>

                            
                            <button type="button" class="btn red w-100 radius-10 mb-3"><a href="OrderCart"> Checkout</a></button>
                        </div>
                    </div>        </div>
            </div>
        </section>
        <script src="./js/external/dg.js"></script>
        <script>
                                var dgFlow = new PAYPAL.apps.DGFlow({
                                    trigger: 'submitBtn'
                                });
                                dgFlow = top.dgFlow || top.opener.top.dgFlow;
                                dgFlow.closeFlow();
                                // top.close();
        </script>

        <script type="text/javascript">
            function removeFromCartList(elem) {
                url1 = 'http://localhost/home/handleCartItems';
                url2 = 'http://localhost/home/refreshWishList';
                url3 = 'http://localhost/home/refreshShoppingCart';
                $.ajax({
                    url: url1,
                    type: 'POST',
                    data: {
                        course_id: elem.id
                    },
                    success: function (response) {

                        $('#cart_items').html(response);
                        if ($(elem).hasClass('addedToCart')) {
                            $('.big-cart-button-' + elem.id).removeClass('addedToCart')
                            $('.big-cart-button-' + elem.id).text("Add to cart");
                        } else {
                            $('.big-cart-button-' + elem.id).addClass('addedToCart')
                            $('.big-cart-button-' + elem.id).text("Added to cart");
                        }

                        $.ajax({
                            url: url2,
                            type: 'POST',
                            success: function (response) {
                                $('#wishlist_items').html(response);
                            }
                        });

                        $.ajax({
                            url: url3,
                            type: 'POST',
                            success: function (response) {
                                $('#cart_items_details').html(response);
                            }
                        });
                    }
                });
            }

            function handleCheckOut() {
                $.ajax({
                    url: 'http://localhost/home/isLoggedIn?url_history=aHR0cDovL2xvY2FsaG9zdC9ob21lL3Nob3BwaW5nX2NhcnQ=',
                    success: function (response) {
                        if (!response) {
                            window.location.replace("http://localhost/login");
                        } else if ("10" > 0) {
                            // $('#paymentModal').modal('show');
                            //$('.total_price_of_checking_out').val($('#total_price_of_checking_out').text());
                            window.location.replace("http://localhost/home/payment");
                        } else {
                            toastr.error('There are no courses on your cart');
                        }
                    }
                });
            }

            function handleCartItems(elem) {
                var couponCode = $("#coupon-code").val();

                url1 = 'http://localhost/home/handleCartItems';
                url2 = 'http://localhost/home/refreshWishList';
                url3 = 'http://localhost/home/refreshShoppingCart';
                $.ajax({
                    url: url1,
                    type: 'POST',
                    data: {
                        course_id: elem.id
                    },
                    success: function (response) {
                        $('#cart_items').html(response);
                        if ($(elem).hasClass('addedToCart')) {
                            $('.big-cart-button-' + elem.id).removeClass('addedToCart')
                            $('.big-cart-button-' + elem.id).text("Add to cart");
                        } else {
                            $('.big-cart-button-' + elem.id).addClass('addedToCart')
                            $('.big-cart-button-' + elem.id).text("Added to cart");
                        }
                        $.ajax({
                            url: url2,
                            type: 'POST',
                            success: function (response) {
                                $('#wishlist_items').html(response);
                            }
                        });

                        $.ajax({
                            url: url3,
                            type: 'POST',
                            data: {
                                couponCode: couponCode
                            },
                            success: function (response) {
                                $('#cart_items_details').html(response);
                            }
                        });
                    }
                });
            }

            function applyCoupon() {
                $("#spinner").removeClass('hidden');
                var couponCode = $("#coupon-code").val();
                url3 = 'http://localhost/home/refreshShoppingCart';
                $.ajax({
                    url: url3,
                    type: 'POST',
                    data: {
                        couponCode: couponCode
                    },
                    success: function (response) {
                        $("#spinner").addClass('hidden');
                        $('#cart_items_details').html(response);
                    }
                });
            }
        </script><footer class="footer-area d-print-none bg-gray mt-5 pt-5">
            <div class="container-xl">
                <div class="row mb-3">
                    <div class="col-6 col-sm-6 col-md-3">
                        <h5 class="text-muted mb-3">Top categories</h5>
                        <ul class="list-unstyled text-small">
                            <li class="mb-2">
                                <a class="link-secondary footer-hover-link" href="http://localhost/home/courses?category=linh-tịnh">
                                    Linh tịnh                  <!-- <span class="fw-700 text-end">()</span> -->
                                </a>
                            </li>
                            <li class="mb-2">
                                <a class="link-secondary footer-hover-link" href="http://localhost/home/courses?category=react">
                                    React                  <!-- <span class="fw-700 text-end">()</span> -->
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-6 col-sm-6 col-md-3">
                        <h5 class="text-muted mb-3">Useful links</h5>
                        <ul class="list-unstyled text-small">
                            <li class="mb-2"><a class="link-secondary footer-hover-link" href="http://localhost/blog">Blog</a></li>
                            <li class="mb-2"><a class="link-secondary footer-hover-link" href="http://localhost/home/courses">All courses</a></li>
                            <li class="mb-2"><a class="link-secondary footer-hover-link" href="http://localhost/home/sign_up">Sign up</a></li>
                            <li class="mb-2"><a class="link-secondary footer-hover-link" href="http://localhost/home/login">Login</a></li>
                        </ul>
                    </div>
                    <div class="col-6 col-sm-6 col-md-3">
                        <h5 class="text-muted mb-3">Help</h5>
                        <ul class="list-unstyled text-small">
                            <li class="mb-2"><a class="link-secondary footer-hover-link" href="http://localhost/home/about_us">About us</a></li>
                            <li class="mb-2"><a class="link-secondary footer-hover-link" href="http://localhost/home/privacy_policy">Privacy policy</a></li>
                            <li class="mb-2"><a class="link-secondary footer-hover-link" href="http://localhost/home/terms_and_condition">Terms and condition</a></li>
                            <li class="mb-2"><a class="link-secondary footer-hover-link" href="http://localhost/home/refund_policy">Refund policy</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-12 order-sm-first">
                        <img src=./js/logo-dark.png" width="130">
                        <span class="d-block mb-1 mt-2 fw-600" style="font-size: 14.5px; line-height: 28px">Study any topic, anytime. explore thousands of courses for the lowest price ever!</span>

                        <ul class="footer-social-link">
                            <li class="mb-1">
                                <a href="https://facebook.com"><i class="fab fa-facebook-f"></i></a>
                            </li>
                            <li class="mb-1">
                                <a href="https://twitter.com"><i class="fab fa-twitter"></i></a>
                            </li>
                            <li class="mb-1">
                                <a href="https://linkedin.com"><i class="fab fa-linkedin"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <section class="border-top">
                <div class="container-xl">
                    <div class="row mt-3 py-1">
                        <div class="col-6 col-sm-6 col-md-3 text-muted text-13px">
                            &copy; 2021 NewSite, All rights reserved        </div>

                        <div class="col-6 col-sm-6 col-md-3 d-none d-md-block"></div>
                        <div class="col-6 col-sm-6 col-md-3 d-none d-md-block"></div>
                        <div class="col-6 col-sm-6 col-md-3 text-center text-md-start">
                            <select class="language_selector" onchange="switch_language(this.value)">
                                <option value="english" selected>English</option>
                            </select>
                        </div>
                    </div>
                </div>
            </section>
        </footer>

        <script type="text/javascript">
            function switch_language(language) {
                $.ajax({
                    url: 'http://localhost/home/site_language',
                    type: 'post',
                    data: {language: language},
                    success: function (response) {
                        setTimeout(function () {
                            location.reload();
                        }, 500);
                    }
                });
            }
        </script>



        <!-- PAYMENT MODAL -->
        <!-- Modal -->

        <!-- Modal -->
        <div class="modal fade multi-step" id="EditRatingModal" tabindex="-1" role="dialog" aria-hidden="true" reset-on-close="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content edit-rating-modal">
                    <div class="modal-header">
                        <h5 class="modal-title step-1" data-step="1">Step 1</h5>
                        <h5 class="modal-title step-2" data-step="2">Step 2</h5>
                        <h5 class="m-progress-stats modal-title">
                            &nbsp;of&nbsp;<span class="m-progress-total"></span>
                        </h5>

                        <button type="button" class="close" data-bs-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="m-progress-bar-wrapper">
                        <div class="m-progress-bar">
                        </div>
                    </div>
                    <div class="modal-body step step-1">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="modal-rating-box">
                                        <h4 class="rating-title">How would you rate this course overall?</h4>
                                        <fieldset class="your-rating">

                                            <input type="radio" id="star5" name="rating" value="5" />
                                            <label class = "full" for="star5"></label>

                                            <!-- <input type="radio" id="star4half" name="rating" value="4 and a half" />
                                            <label class="half" for="star4half"></label> -->

                                            <input type="radio" id="star4" name="rating" value="4" />
                                            <label class = "full" for="star4"></label>

                                            <!-- <input type="radio" id="star3half" name="rating" value="3 and a half" />
                                            <label class="half" for="star3half"></label> -->

                                            <input type="radio" id="star3" name="rating" value="3" />
                                            <label class = "full" for="star3"></label>

                                            <!-- <input type="radio" id="star2half" name="rating" value="2 and a half" />
                                            <label class="half" for="star2half"></label> -->

                                            <input type="radio" id="star2" name="rating" value="2" />
                                            <label class = "full" for="star2"></label>

                                            <!-- <input type="radio" id="star1half" name="rating" value="1 and a half" />
                                            <label class="half" for="star1half"></label> -->

                                            <input type="radio" id="star1" name="rating" value="1" />
                                            <label class = "full" for="star1"></label>

                                            <!-- <input type="radio" id="starhalf" name="rating" value="half" />
                                            <label class="half" for="starhalf"></label> -->

                                        </fieldset>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="modal-course-preview-box">
                                        <div class="card">
                                            <img class="card-img-top img-fluid" id = "course_thumbnail_1" alt="">
                                            <div class="card-body">
                                                <h5 class="card-title" class = "course_title_for_rating" id = "course_title_1"></h5>
                                                <p class="card-text" id = "instructor_details">

                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-body step step-2">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="modal-rating-comment-box">
                                        <h4 class="rating-title">Write a public review</h4>
                                        <textarea id = "review_of_a_course" name = "review_of_a_course" placeholder="Describe your experience what you got out of the course and other helpful highlights. What did the instructor do well and what could use some improvement?" maxlength="65000" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="modal-course-preview-box">
                                        <div class="card">
                                            <img class="card-img-top img-fluid" id = "course_thumbnail_2" alt="">
                                            <div class="card-body">
                                                <h5 class="card-title" class = "course_title_for_rating" id = "course_title_2"></h5>
                                                <p class="card-text">
                                                    -
                                                                        </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="course_id" id = "course_id_for_rating" value="">
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary next step step-1" data-step="1" onclick="sendEvent(2)">Next</button>
                        <button type="button" class="btn btn-primary previous step step-2 mr-auto" data-step="2" onclick="sendEvent(1)">Previous</button>
                        <button type="button" class="btn btn-primary publish step step-2" onclick="publishRating($('#course_id_for_rating').val())" id = "">Publish</button>
                    </div>
                </div>
            </div>
        </div><!-- Modal -->



        <script src="./js/modernizr-3.5.0.min.js"></script>
        <script src="./js/jquery-3.2.1.min.js"></script>
        <script src="./js/popper.min.js"></script>
        <script src="./js/bootstrap.min.js"></script>



        <script src="./js/main.js"></script>
        <script src="./toastr.min.js"></script>
        <script src="./jquery.form.min.js"></script>
        <script src="./js/jQuery.tagify.js"></script>

        <!-- SHOW TOASTR NOTIFIVATION -->


        <script type="text/javascript">
                            $(function () {
                                $('[data-bs-toggle="tooltip"]').tooltip()
                            });
                            if ($('.tagify').height()) {
                                $('.tagify').tagify();
                            }
        </script><script type="text/javascript">
            function showAjaxModal(url)
            {
                // SHOWING AJAX PRELOADER IMAGE
                jQuery('#modal_ajax .modal-body').html('<div class="w-100 text-center pt-5"><img class="mt-5 mb-5" width="80px" src="http://localhost/assets/global/gif/page-loader-2.gif"></div>');

                // LOADING THE AJAX MODAL
                jQuery('#modal_ajax').modal('show', {backdrop: 'true'});

                // SHOW AJAX RESPONSE ON REQUEST SUCCESS
                $.ajax({
                    url: url,
                    success: function (response)
                    {
                        jQuery('#modal_ajax .modal-body').html(response);
                    }
                });
            }

            function lesson_preview(url, title) {
                // SHOWING AJAX PRELOADER IMAGE
                jQuery('.lesson_preview_header').html(title);
                jQuery('#lesson_preview .modal-body').html('<div class="w-100 text-center pt-5"><img class="mt-5 mb-5" width="80px" src="http://localhost/assets/global/gif/page-loader-2.gif"></div>');

                // LOADING THE AJAX MODAL
                jQuery('#lesson_preview').modal('show', {backdrop: 'true'});

                // SHOW AJAX RESPONSE ON REQUEST SUCCESS
                $.ajax({
                    url: url,
                    success: function (response)
                    {
                        jQuery('#lesson_preview .modal-body').html(response);
                    }
                });
            }
        </script>

        <!-- (Ajax Modal)-->
        <div class="modal fade" id="modal_ajax">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-bs-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body" style="overflow:auto;">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-bs-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="lesson_preview" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content course-preview-modal">
                    <div class="modal-header">
                        <h5 class="lesson_preview_header"></h5>
                        <button type="button" class="close" data-bs-dismiss="modal" onclick="window.location.reload()">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                    </div>
                </div>
            </div>
        </div>




        <script type="text/javascript">
            function confirm_modal(delete_url)
            {
                jQuery('#modal-4').modal('show', {backdrop: 'static'});
                document.getElementById('delete_link').setAttribute('href', delete_url);
            }
        </script>

        <!-- (Normal Modal)-->
        <div class="modal fade" id="modal-4">
            <div class="modal-dialog">
                <div class="modal-content" style="margin-top:100px;">

                    <div class="modal-header">
                        <h4 class="modal-title text-center">Are you sure ?</h4>
                        <button type="button" class="btn btn-outline-secondary px-1 py-0" data-bs-dismiss="modal" aria-hidden="true"><i class="fas fa-times-circle"></i></button>
                    </div>


                    <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
                        <a href="#" class="btn btn-danger btn-yes" id="delete_link">Yes</a>
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>


        <script type="text/javascript">
            function async_modal() {
                const asyncModal = new Promise(function (resolve, reject) {
                    $('#modal-4').modal('show');
                    $('#modal-4 .btn-yes').click(function () {
                        resolve(true);
                    });
                    $('#modal-4 .btn-cancel').click(function () {
                        resolve(false);
                    });
                });
                return asyncModal;
            }
        </script>
        <script type="text/javascript">
            function toggleRatingView(course_id) {
                $('#course_info_view_' + course_id).toggle();
                $('#course_rating_view_' + course_id).toggle();
                $('#edit_rating_btn_' + course_id).toggle();
                $('#cancel_rating_btn_' + course_id).toggle();
            }

            function publishRating(course_id) {
                var review = $('#review_of_a_course_' + course_id).val();
                var starRating = 0;
                starRating = $('#star_rating_of_course_' + course_id).val();
                if (starRating > 0) {
                    $.ajax({
                        type: 'POST',
                        url: 'http://localhost/home/rate_course',
                        data: {course_id: course_id, review: review, starRating: starRating},
                        success: function (response) {
                            location.reload();
                        }
                    });
                } else {

                }
            }

            function isTouchDevice() {
                return (('ontouchstart' in window) ||
                        (navigator.maxTouchPoints > 0) ||
                        (navigator.msMaxTouchPoints > 0));
            }

            function viewMore(element, visibility) {
                if (visibility == "hide") {
                    $(element).parent(".view-more-parent").addClass("expanded");
                    $(element).remove();
                } else if ($(element).hasClass("view-more")) {
                    $(element).parent(".view-more-parent").addClass("expanded has-hide");
                    $(element)
                            .removeClass("view-more")
                            .addClass("view-less")
                            .html("- View less");
                } else if ($(element).hasClass("view-less")) {
                    $(element).parent(".view-more-parent").removeClass("expanded has-hide");
                    $(element)
                            .removeClass("view-less")
                            .addClass("view-more")
                            .html("+ View more");
                }
            }

            function redirect_to(url) {
                if (!isTouchDevice() && $(window).width() > 767) {
                    window.location.replace(url);
                }
            }

            //Event call after loading page
            document.addEventListener('DOMContentLoaded', function () {
                setTimeout(function () {
                    $('.animated-loader').hide();
                    $('.shown-after-loading').show();
                });
            }, false);


            function check_action(e, url) {
                var tag = $(e).prop("tagName").toLowerCase();
                if (tag == 'a') {
                    return true;
                } else if (tag != 'a' && url) {
                    $(location).attr('href', url);
                    return false;
                } else {
                    return true;
                }
            }
        </script>
    </body>
</html>
