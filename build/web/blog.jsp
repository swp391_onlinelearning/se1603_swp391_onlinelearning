
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Blog | NewSite</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5.0, minimum-scale=0.86">
        <meta name="author" content="Creativeitem" />

        <meta name="keywords" content="LMS,Learning Management System,Creativeitem,demo,hello,How are you"/>
        <meta name="description" content="We’re a leading marketplace platform for learning and teaching online. Explore some of our most popular content and learn something new." />

        <!--Social sharing content-->
        <meta property="og:title" content="Where possibilities begin" />
        <meta property="og:image" content="http://localhost/uploads/blog/page-banner/blog-page.png">
        <meta property="og:url" content="http://localhost/blog" />
        <meta property="og:type" content="Learning management system" />
        <!--Social sharing content-->

        <link name="favicon" type="image/x-icon" href="./img/favicon.png" rel="shortcut icon" />
        <link rel="favicon" href="./img/favicon.ico">
        <link rel="apple-touch-icon" href="./img/icon.png">


        <!-- font awesome 5 -->
        <link rel="stylesheet" href="./css/fontawesome-all.min.css">

        <link rel="stylesheet" href="./css/bootstrap.min.css">

        <link rel="stylesheet" href="./css/main.css">
        <link rel="stylesheet" href="./css/responsive.css">
        <link rel="stylesheet" href="./css/custom.css">
        <link rel="stylesheet" href="./css/custom.css">
        <link rel="stylesheet" href="./css/tagify.css">
        <link rel="stylesheet" href="./toastr.css">
        <script src="./js/jquery-3.3.1.min.js"></script>
    </head>
    <body class="gray-bg">
        <jsp:include page="header.jsp"/>

        <section class="" style="background-image: url('./img/blog-page.png'); background-size: cover; background-position: center; position: relative;">
            <div class="image-placeholder-2"></div>
            <div class="container-lg position-inherit py-5">
                <div class="row my-0 my-md-4">
                    <div class="col-lg-6">
                        <h1 class="display-4 fw-600 text-white">Where possibilities begin</h1>
                        <div class="text-17px text-white">We’re a leading marketplace platform for learning and teaching online. Explore some of our most popular content and learn something new.</div>
                    </div>
                </div>
            </div>
        </section>

        <section class="mt-5">
            <div class="container-xl">
                <div class="row">
                    <div class="col-lg-9">
                        <div class="row row-cols-1 row-cols-lg-3 row-cols-md-2 g-4">
                            <div class="col-12">
                                <h4 class="fw-700">Latest blogs</h4>
                            </div>
                            <c:forEach items="${listBlog}" var="o">
                                <div class="col">
                                    <div class="card radius-10">
                                        <img src="${o.thumbnail}" class="card-img-top radius-top-10">
                                        <div class="card-body pt-4">
                                            <p class="card-text">
                                                <small class="text-muted">Created by - Admin</a></small>
                                            </p>
                                            <h5 class="card-title"><a href="BlogDetail?id=${o.blog_id}">${o.title}</a></h5>
                                            <p class="card-text ellipsis-line-3">${o.short_description}</p>

                                            <a class="fw-600" href="BlogDetail?id=${o.blog_id}">More details</a>

                                            <p class="card-text mt-2 mb-0">
                                                <small class="text-muted text-12px">${o.added_date}</small>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>

                            <div class="col-12">
                                <a class="float-end btn btn-secondary px-3 fw-600" href="http://localhost/blogs">View all</a>
                            </div>
                        </div>

                    </div>

                    <div class="col-lg-3 py-3 radius-10 bg-white">
                        <div style="position: sticky; top: 20px;">

                            <h6 class="m-0 px-2 pt-5 pb-3 fw-700 border-bottom">Latest blogs</h6>
                            <div class="list-group">
                                <c:forEach items="${laster}" var="o">
                                    <a href="BlogDetail?id=${o.blog_id}" class="px-2 py-3 bg-transparent list-group-item list-group-item-action">
                                        <div class="pe-2">
                                            <img src="${o.thumbnail}" class="card-img-top" alt="TUOUTUI">
                                        </div>
                                        <div>
                                            <div class="d-flex w-100 justify-content-between">
                                                <h6 class="my-1">${o.title}</h6>
                                            </div>
                                            <small class="text-muted ellipsis-line-2">${o.short_description}</small>
                                            <p class="mt-2 mb-0 text-12px text-muted">${o.added_date}</p>
                                        </div>
                                    </a>
                                </c:forEach>
                                <a class="text-14px ps-2 my-2 text-muted" href="Blogs">All blogs</a>
                            </div>
                        </div>

                        <script type="text/javascript">
                            function show_submit_button(e) {
                                var search_string = $(e).val();
                                if (search_string) {
                                    $('#blog_search_button').show();
                                } else {
                                    $('#blog_search_button').hide();
                                }
                            }
                        </script>            </div>
                </div>
            </div>
        </section><footer class="footer-area d-print-none bg-gray mt-5 pt-5">
            <div class="container-xl">
                <div class="row mb-3">
                    <div class="col-6 col-sm-6 col-md-3">
                        <h5 class="text-muted mb-3">Top categories</h5>
                        <ul class="list-unstyled text-small">
                        </ul>
                    </div>
                    <div class="col-6 col-sm-6 col-md-3">
                        <h5 class="text-muted mb-3">Useful links</h5>
                        <ul class="list-unstyled text-small">
                            <li class="mb-2"><a class="link-secondary footer-hover-link" href="http://localhost/blog">Blog</a></li>
                            <li class="mb-2"><a class="link-secondary footer-hover-link" href="http://localhost/home/courses">All courses</a></li>
                            <li class="mb-2"><a class="link-secondary footer-hover-link" href="http://localhost/home/sign_up">Sign up</a></li>
                            <li class="mb-2"><a class="link-secondary footer-hover-link" href="http://localhost/home/login">Login</a></li>
                        </ul>
                    </div>
                    <div class="col-6 col-sm-6 col-md-3">
                        <h5 class="text-muted mb-3">Help</h5>
                        <ul class="list-unstyled text-small">
                            <li class="mb-2"><a class="link-secondary footer-hover-link" href="http://localhost/home/about_us">About us</a></li>
                            <li class="mb-2"><a class="link-secondary footer-hover-link" href="http://localhost/home/privacy_policy">Privacy policy</a></li>
                            <li class="mb-2"><a class="link-secondary footer-hover-link" href="http://localhost/home/terms_and_condition">Terms and condition</a></li>
                            <li class="mb-2"><a class="link-secondary footer-hover-link" href="http://localhost/home/refund_policy">Refund policy</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-12 order-sm-first">
                        <img src="" width="130">
                        <span class="d-block mb-1 mt-2 fw-600" style="font-size: 14.5px; line-height: 28px">Study any topic, anytime. explore thousands of courses for the lowest price ever!</span>

                        <ul class="footer-social-link">
                            <li class="mb-1">
                                <a href="https://facebook.com"><i class="fab fa-facebook-f"></i></a>
                            </li>
                            <li class="mb-1">
                                <a href="https://twitter.com"><i class="fab fa-twitter"></i></a>
                            </li>
                            <li class="mb-1">
                                <a href="https://linkedin.com"><i class="fab fa-linkedin"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <section class="border-top">
                <div class="container-xl">
                    <div class="row mt-3 py-1">
                        <div class="col-6 col-sm-6 col-md-3 text-muted text-13px">
                            &copy; 2021 NewSite, All rights reserved        </div>

                        <div class="col-6 col-sm-6 col-md-3 d-none d-md-block"></div>
                        <div class="col-6 col-sm-6 col-md-3 d-none d-md-block"></div>
                        <div class="col-6 col-sm-6 col-md-3 text-center text-md-start">
                            <select class="language_selector" onchange="switch_language(this.value)">
                                <option value="english" selected>English</option>
                            </select>
                        </div>
                    </div>
                </div>
            </section>
        </footer>

        <script type="text/javascript">
            function switch_language(language) {
                $.ajax({
                    url: 'http://localhost/home/site_language',
                    type: 'post',
                    data: {language: language},
                    success: function (response) {
                        setTimeout(function () {
                            location.reload();
                        }, 500);
                    }
                });
            }
        </script>



        <!-- PAYMENT MODAL -->
        <!-- Modal -->

        <!-- Modal -->
        <div class="modal fade multi-step" id="EditRatingModal" tabindex="-1" role="dialog" aria-hidden="true" reset-on-close="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content edit-rating-modal">
                    <div class="modal-header">
                        <h5 class="modal-title step-1" data-step="1">Step 1</h5>
                        <h5 class="modal-title step-2" data-step="2">Step 2</h5>
                        <h5 class="m-progress-stats modal-title">
                            &nbsp;of&nbsp;<span class="m-progress-total"></span>
                        </h5>

                        <button type="button" class="close" data-bs-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="m-progress-bar-wrapper">
                        <div class="m-progress-bar">
                        </div>
                    </div>
                    <div class="modal-body step step-1">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="modal-rating-box">
                                        <h4 class="rating-title">How would you rate this course overall?</h4>
                                        <fieldset class="your-rating">

                                            <input type="radio" id="star5" name="rating" value="5" />
                                            <label class = "full" for="star5"></label>

                                            <!-- <input type="radio" id="star4half" name="rating" value="4 and a half" />
                                            <label class="half" for="star4half"></label> -->

                                            <input type="radio" id="star4" name="rating" value="4" />
                                            <label class = "full" for="star4"></label>

                                            <!-- <input type="radio" id="star3half" name="rating" value="3 and a half" />
                                            <label class="half" for="star3half"></label> -->

                                            <input type="radio" id="star3" name="rating" value="3" />
                                            <label class = "full" for="star3"></label>

                                            <!-- <input type="radio" id="star2half" name="rating" value="2 and a half" />
                                            <label class="half" for="star2half"></label> -->

                                            <input type="radio" id="star2" name="rating" value="2" />
                                            <label class = "full" for="star2"></label>

                                            <!-- <input type="radio" id="star1half" name="rating" value="1 and a half" />
                                            <label class="half" for="star1half"></label> -->

                                            <input type="radio" id="star1" name="rating" value="1" />
                                            <label class = "full" for="star1"></label>

                                            <!-- <input type="radio" id="starhalf" name="rating" value="half" />
                                            <label class="half" for="starhalf"></label> -->

                                        </fieldset>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="modal-course-preview-box">
                                        <div class="card">
                                            <img class="card-img-top img-fluid" id = "course_thumbnail_1" alt="">
                                            <div class="card-body">
                                                <h5 class="card-title" class = "course_title_for_rating" id = "course_title_1"></h5>
                                                <p class="card-text" id = "instructor_details">

                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-body step step-2">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="modal-rating-comment-box">
                                        <h4 class="rating-title">Write a public review</h4>
                                        <textarea id = "review_of_a_course" name = "review_of_a_course" placeholder="Describe your experience what you got out of the course and other helpful highlights. What did the instructor do well and what could use some improvement?" maxlength="65000" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="modal-course-preview-box">
                                        <div class="card">
                                            <img class="card-img-top img-fluid" id = "course_thumbnail_2" alt="">
                                            <div class="card-body">
                                                <h5 class="card-title" class = "course_title_for_rating" id = "course_title_2"></h5>
                                                <p class="card-text">
                                                    -
                                                                        </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="course_id" id = "course_id_for_rating" value="">
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary next step step-1" data-step="1" onclick="sendEvent(2)">Next</button>
                        <button type="button" class="btn btn-primary previous step step-2 mr-auto" data-step="2" onclick="sendEvent(1)">Previous</button>
                        <button type="button" class="btn btn-primary publish step step-2" onclick="publishRating($('#course_id_for_rating').val())" id = "">Publish</button>
                    </div>
                </div>
            </div>
        </div><!-- Modal -->


        <script src="./js/modernizr-3.5.0.min.js"></script>
        <script src="./js/jquery-3.2.1.min.js"></script>
        <script src="./js/popper.min.js"></script>
        <script src="./js/bootstrap.min.js"></script>



        <script src="./js/main.js"></script>
        <script src="./js/toastr.min.js"></script>
        <script src="./js/jquery.form.min.js"></script>
        <script src="./js/jQuery.tagify.js"></script>

        <!-- SHOW TOASTR NOTIFIVATION -->


        <script type="text/javascript">
                            $(function () {
                                $('[data-bs-toggle="tooltip"]').tooltip()
                            });
                            if ($('.tagify').height()) {
                                $('.tagify').tagify();
                            }
        </script><script type="text/javascript">
            function showAjaxModal(url)
            {
                // SHOWING AJAX PRELOADER IMAGE
                jQuery('#modal_ajax .modal-body').html('<div class="w-100 text-center pt-5"><img class="mt-5 mb-5" width="80px" src="http://localhost/assets/global/gif/page-loader-2.gif"></div>');

                // LOADING THE AJAX MODAL
                jQuery('#modal_ajax').modal('show', {backdrop: 'true'});

                // SHOW AJAX RESPONSE ON REQUEST SUCCESS
                $.ajax({
                    url: url,
                    success: function (response)
                    {
                        jQuery('#modal_ajax .modal-body').html(response);
                    }
                });
            }

            function lesson_preview(url, title) {
                // SHOWING AJAX PRELOADER IMAGE
                jQuery('.lesson_preview_header').html(title);
                jQuery('#lesson_preview .modal-body').html('<div class="w-100 text-center pt-5"><img class="mt-5 mb-5" width="80px" src="http://localhost/assets/global/gif/page-loader-2.gif"></div>');

                // LOADING THE AJAX MODAL
                jQuery('#lesson_preview').modal('show', {backdrop: 'true'});

                // SHOW AJAX RESPONSE ON REQUEST SUCCESS
                $.ajax({
                    url: url,
                    success: function (response)
                    {
                        jQuery('#lesson_preview .modal-body').html(response);
                    }
                });
            }
        </script>

        <!-- (Ajax Modal)-->
        <div class="modal fade" id="modal_ajax">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-bs-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body" style="overflow:auto;">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-bs-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="lesson_preview" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content course-preview-modal">
                    <div class="modal-header">
                        <h5 class="lesson_preview_header"></h5>
                        <button type="button" class="close" data-bs-dismiss="modal" onclick="window.location.reload()">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                    </div>
                </div>
            </div>
        </div>




        <script type="text/javascript">
            function confirm_modal(delete_url)
            {
                jQuery('#modal-4').modal('show', {backdrop: 'static'});
                document.getElementById('delete_link').setAttribute('href', delete_url);
            }
        </script>

        <!-- (Normal Modal)-->
        <div class="modal fade" id="modal-4">
            <div class="modal-dialog">
                <div class="modal-content" style="margin-top:100px;">

                    <div class="modal-header">
                        <h4 class="modal-title text-center">Are you sure ?</h4>
                        <button type="button" class="btn btn-outline-secondary px-1 py-0" data-bs-dismiss="modal" aria-hidden="true"><i class="fas fa-times-circle"></i></button>
                    </div>


                    <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
                        <a href="#" class="btn btn-danger btn-yes" id="delete_link">Yes</a>
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>


        <script type="text/javascript">
            function async_modal() {
                const asyncModal = new Promise(function (resolve, reject) {
                    $('#modal-4').modal('show');
                    $('#modal-4 .btn-yes').click(function () {
                        resolve(true);
                    });
                    $('#modal-4 .btn-cancel').click(function () {
                        resolve(false);
                    });
                });
                return asyncModal;
            }
        </script>
        <script type="text/javascript">
            function toggleRatingView(course_id) {
                $('#course_info_view_' + course_id).toggle();
                $('#course_rating_view_' + course_id).toggle();
                $('#edit_rating_btn_' + course_id).toggle();
                $('#cancel_rating_btn_' + course_id).toggle();
            }

            function publishRating(course_id) {
                var review = $('#review_of_a_course_' + course_id).val();
                var starRating = 0;
                starRating = $('#star_rating_of_course_' + course_id).val();
                if (starRating > 0) {
                    $.ajax({
                        type: 'POST',
                        url: 'http://localhost/home/rate_course',
                        data: {course_id: course_id, review: review, starRating: starRating},
                        success: function (response) {
                            location.reload();
                        }
                    });
                } else {

                }
            }

            function isTouchDevice() {
                return (('ontouchstart' in window) ||
                        (navigator.maxTouchPoints > 0) ||
                        (navigator.msMaxTouchPoints > 0));
            }

            function viewMore(element, visibility) {
                if (visibility == "hide") {
                    $(element).parent(".view-more-parent").addClass("expanded");
                    $(element).remove();
                } else if ($(element).hasClass("view-more")) {
                    $(element).parent(".view-more-parent").addClass("expanded has-hide");
                    $(element)
                            .removeClass("view-more")
                            .addClass("view-less")
                            .html("- View less");
                } else if ($(element).hasClass("view-less")) {
                    $(element).parent(".view-more-parent").removeClass("expanded has-hide");
                    $(element)
                            .removeClass("view-less")
                            .addClass("view-more")
                            .html("+ View more");
                }
            }

            function redirect_to(url) {
                if (!isTouchDevice() && $(window).width() > 767) {
                    window.location.replace(url);
                }
            }

            //Event call after loading page
            document.addEventListener('DOMContentLoaded', function () {
                setTimeout(function () {
                    $('.animated-loader').hide();
                    $('.shown-after-loading').show();
                });
            }, false);


            function check_action(e, url) {
                var tag = $(e).prop("tagName").toLowerCase();
                if (tag == 'a') {
                    return true;
                } else if (tag != 'a' && url) {
                    $(location).attr('href', url);
                    return false;
                } else {
                    return true;
                }
            }
        </script>
    </body>
</html>
