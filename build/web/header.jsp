
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<section class="menu-area bg-white">
    <div class="container-xl">
        <nav class="navbar navbar-expand-lg bg-white">

            <ul class="mobile-header-buttons">
                <li><a class="mobile-nav-trigger" href="#mobile-primary-nav">http://localhost/menu<span></span></a></li>
                <li><a class="mobile-search-trigger" href="#mobile-search">http://localhost/search<span></span></a></li>
            </ul>

            <a href="http://localhost/" class="navbar-brand" href="#"><img src="" alt="" height="35"></a>

            <div class="main-nav-wrap">
                <div class="mobile-overlay"></div>
                <style type="text/css">
                    @media only screen and (max-width: 767px) {
                        .category.corner-triangle.top-left.pb-0.is-hidden{
                            display: none !important;
                        }
                        .sub-category.is-hidden{
                            display: none !important;
                        }
                    }
                </style>

                <ul class="mobile-main-nav">
                    <div class="mobile-menu-helper-top"></div>
                    <li class="has-children text-nowrap fw-bold">
                        <a href="">
                            <i class="fas fa-th d-inline text-20px"></i>
                            <span class="fw-500">Categories</span>
                            <span class="has-sub-category"><i class="fas fa-angle-right"></i></span>
                        </a>

                        <ul class="category corner-triangle top-left is-hidden pb-0" >
                            <li class="go-back"><a href=""><i class="fas fa-angle-left"></i>Menu</a></li>

                            <li class="all-category-devided mb-0 p-0">
                                <a href="http://localhost/home/courses" class="py-3">
                                    <span class="icon"><i class="fa fa-align-justify"></i></span>
                                    <span>All courses</span>
                                </a>
                            </li>


                        </ul>
                    </li>

                    <div class="mobile-menu-helper-bottom"></div>
                </ul>
            </div>

            <form class="inline-form me-auto" action="http://localhost/home/search" method="get">
                <div class="input-group search-box mobile-search">
                    <input type="text" name = 'query' class="form-control" placeholder="Search for courses">
                    <div class="input-group-append">
                        <button class="btn" type="submit"><i class="fas fa-search"></i></button>
                    </div>
                </div>
            </form>
               
            <c:if test="${sessionScope.acc.role == 1}">
                <div class="instructor-box menu-icon-box ms-auto">
                    <div class="icon">
                        <a href="Dashboard" style="border: 1px solid transparent; margin: 0px; font-size: 14px; width: max-content; border-radius: 5px; max-height: 40px; line-height: 40px; padding: 0px 10px;">Administrator</a>
                    </div>
                </div>
            </c:if>
            
            <div class="cart-box menu-icon-box" id = "cart_items">
                <div class="icon">
                    <a href="http://localhost/home/shopping_cart"><i class="fas fa-shopping-cart"></i></a>
                    <span class="number">0</span>
                </div>

                <!-- Cart Dropdown goes here -->
                <div class="dropdown course-list-dropdown corner-triangle top-right" style="display: none;"> <!-- Just remove the display none from the css to make it work -->
                    <div class="list-wrapper">
                        <div class="item-list">
                            <ul>
                            </ul>
                        </div>
                        <div class="dropdown-footer">
                            <div class="cart-total-price clearfix">
                                <span>Total:</span>
                                <div class="float-end">
                                    <span class="current-price">$0</span>
                                    <!-- <span class="original-price">$94.99</span> -->
                                </div>
                            </div>
                            <a href = "http://localhost/home/shopping_cart">Go to cart</a>
                        </div>
                    </div>
                    <div class="empty-box text-center d-none">
                        <p>Your cart is empty.</p>
                        <a href="">Keep Shopping</a>
                    </div>
                </div>      </div>

            <span class="signin-box-move-desktop-helper"></span>
            <c:if test="${sessionScope.acc == null}">
                <div class="sign-in-box btn-group">
                    <a href="Login" class="btn btn-sign-in">Log in</a>
                    <a href="Register" class="btn btn-sign-up">Sign up</a>
                </div> <!--  sign-in-box end -->
            </c:if>

            <c:if test="${sessionScope.acc != null}">
                <div class="sign-in-box btn-group">
                   Hello! ${sessionScope.acc.last_name}
                    <a href="Logout" class="btn btn-sign-up">Logout</a>
                </div> <!--  sign-in-box end -->
            </c:if>
        </nav>
    </div>
</section>