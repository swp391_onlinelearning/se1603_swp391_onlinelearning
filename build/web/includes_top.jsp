

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!-- <link rel="favicon" href="img/icons/favicon.ico' ?>">
<link rel="apple-touch-icon" href="img/icons/icon.png">
-->

<!-- <link rel="stylesheet" href="css/jquery.webui-popover.min.css">
<link rel="stylesheet" href="css/slick.css">
<link rel="stylesheet" href="css/slick-theme.css"> -->


<!-- font awesome 5 -->
<link rel="stylesheet" href="css/fontawesome-all.min.css">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/main.css">
<link rel="stylesheet" href="css/responsive.css">
<link rel="stylesheet" href="css/custom.css">
<link rel="stylesheet" href="css/tagify.css">
<link rel="stylesheet" href="css/toastr.css">
<script src="js/jquery-3.3.1.min.js"></script>