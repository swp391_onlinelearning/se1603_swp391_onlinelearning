

<%@page contentType="text/html" pageEncoding="UTF-8"%>


<!DOCTYPE html>
<html lang="en">
<head>

			<title>Java | Academy</title>
	

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="author" content="Creativeitem" />

			<meta name="keywords" content="LMS,Learning Management System,Creativeitem,demo,hello,How are you"/>
		<meta name="description" content="Study any topic, anytime. explore thousands of courses for the lowest price ever!" />
	
	<link name="favicon" type="image/x-icon" href="http://localhost/onlinecource/uploads/system/favicon.png" rel="shortcut icon" />
	<link rel="favicon" href="http://localhost/onlinecource/assets/frontend/default/img/icons/favicon.ico">
<link rel="apple-touch-icon" href="http://localhost/onlinecource/assets/frontend/default/img/icons/icon.png">
<link rel="stylesheet" href="http://localhost/onlinecource/assets/frontend/default/css/jquery.webui-popover.min.css">
<link rel="stylesheet" href="http://localhost/onlinecource/assets/frontend/default/css/select2.min.css">
<link rel="stylesheet" href="http://localhost/onlinecource/assets/frontend/default/css/slick.css">
<link rel="stylesheet" href="http://localhost/onlinecource/assets/frontend/default/css/slick-theme.css">
<!-- font awesome 5 -->
<link rel="stylesheet" href="http://localhost/onlinecource/assets/frontend/default/css/fontawesome-all.min.css">

<link rel="stylesheet" href="http://localhost/onlinecource/assets/frontend/default/css/bootstrap.min.css">
<link rel="stylesheet" href="http://localhost/onlinecource/assets/frontend/default/css/bootstrap-tagsinput.css">
<link rel="stylesheet" href="http://localhost/onlinecource/assets/frontend/default/css/main.css">
<link rel="stylesheet" href="http://localhost/onlinecource/assets/frontend/default/css/responsive.css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700" rel="stylesheet">
<link rel="stylesheet" href="http://localhost/onlinecource/assets/global/toastr/toastr.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/nestable2/1.6.0/jquery.nestable.min.css" />
<link rel="stylesheet" href="http://localhost/onlinecource/assets/lessons/css/custom.css">
<script src="http://localhost/onlinecource/assets/backend/js/jquery-3.3.1.min.js"></script>

<!-- Lesson page specific styles are here -->
<style type="text/css">
body {
    background-color: #fff !important;
}
.card {
    border-radius: 0px !important;
    background-color: #f7f8fa !important;
    border:0px !important;
}
.course_card {
    padding: 0px;
    background-color: #F7F8FA;
}
.course_container {
    background-color: #fff !important;
}
.course_col {
    padding: 0px;
}
.course_header_col {
    background-color: #29303b;
    color: #fff;
    padding: 15px 10px 10px;
}
.course_header_col img {
    padding: 0px 0px;
}
.course_btn {
    color: #95979a;
    border: 1px solid #95979a;
    padding: 7px 10px;
}
.course_btn:hover {
    color: #fff;
    border:1px solid #fff;
}
.lesson_duration{
    border-radius: 5px;
    padding-top: 8px;
    color: #5C5D61;
    font-size: 13px;
    font-weight: 100;
}
.quiz-card {
    border: 1px solid #dcdddf !important;
}
.bg-quiz-result-info {
    background-color: #007791 !important;
    padding: 13px !important;
}
a{
    text-decoration: none;
}
</style>
</head>
<body class="gray-bg">
	<div class="container-fluid course_container">
    <!-- Top bar -->
    <div class="row">
        <div class="col-md-12 col-lg-7 col-xl-9 course_header_col d-md-flex d-sm-grid">
            <h5>
                <img src="http://localhost/onlinecource/uploads/system/logo-light-sm.png" height="25"> |
                java            </h5>
                        <span class="d-inline-block ml-auto">100% Completed(3/3)</span>
        </div>
        <div class="col-md-12 col-lg-5 col-xl-3 course_header_col text-right">
            <a href="javascript:;" class="course_btn" onclick="toggle_lesson_view()"><i class="fa fa-arrows-alt-h"></i></a>
            <a href="http://localhost/onlinecource/home/my_courses" class="course_btn"> <i class="fa fa-chevron-left"></i> My courses</a>
            <a href="http://localhost/onlinecource/home/course/java/1" class="course_btn">Course details <i class="fa fa-chevron-right"></i></a>
        </div>
    </div>

    <div class="row" id = "lesson-container">
        <!-- Course sections and lesson selector sidebar starts-->
                    <div class="col-lg-3 order-2 mt-5 course_col hidden" id="lesson_list_loader" style="text-align: center;">
    <img src="http://localhost/onlinecource/assets/backend/images/loader.gif" alt="" height="50" width="50">
</div>
<div class="col-lg-3  order-2 course_col" id = "lesson_list_area">
    <div class="text-center" style="margin: 12px 10px;">
        <h5>Course content</h5>
    </div>
    <div class="row" style="margin: 12px -1px">
        <div class="col-12">
            <ul class="nav nav-tabs" id="lessonTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active text-muted" id="section_and_lessons-tab" data-bs-toggle="tab" href="#section_and_lessons" role="tab" aria-controls="section_and_lessons" aria-selected="true">Lessons</a>
                </li>
                <!-- ZOOM LIVE CLASS TAB STARTS -->
                                <!-- ZOOM LIVE CLASS TAB ENDS -->

                <!-- CERTIFICATE TAB -->
                                <!-- CERTIFICATE TAB -->
            </ul>
            <div class="tab-content" id="lessonTabContent">
                <div class="tab-pane fade show active" id="section_and_lessons" role="tabpanel" aria-labelledby="section_and_lessons-tab">
                    <!-- Lesson Content starts from here -->
                    <div class="accordion" id="accordionExample">
                                                    <div class="card" style="margin:0px 0px;">
                                <div class="card-header course_card" id="heading-1">

                                    <button class="btn btn-link w-100 text-start d-grid" type="button" data-bs-toggle="collapse" data-bs-target="#collapse-1"  aria-expanded="true"  aria-controls="collapse-1" style="color: #535a66; background: none; border: none; white-space: normal;" onclick = "toggleAccordionIcon(this, '1')">
                                        <p class="w-100" style="color: #959aa2; font-size: 13px;">
                                            <span class="d-block-inline float-start">Section 1</span>
                                            <span style="float: right; font-weight: 100;" class="accordion_icon" id="accordion_icon_1">
                                                                                                    <i class="fa fa-minus"></i>
                                                                                            </span>
                                        </p>
                                        <p class="d-inline-block float-start text-start text-13px fw-500">Chapter 1</p>
                                    </button>
                                </div>

                                <div id="collapse-1" class="collapse show" aria-labelledby="heading-1" data-parent="#accordionExample">
                                    <div class="card-body"  style="padding:0px;">
                                        <table style="width: 100%;">
                                                                                            
                                                <tr style="width: 100%; padding: 5px 0px;background-color: #E6F2F5;">
                                                    <td style="text-align: left; padding:7px 10px;">
                                                                                                                                                                            <div class="form-group">
                                                                <input type="checkbox" id="1" onchange="markThisLessonAsCompleted(this.id);" value = 1  checked >
                                                                <label for="1"></label>
                                                            </div>
                                                        
                                                        <a href="http://localhost/onlinecource/home/lesson/java/1/1" id = "1" style="color: #444549;font-size: 14px;font-weight: 400;">
                                                            1:
                                                                                                                            Java là gì?                                                                                                                    </a>

                                                        <div class="lesson_duration">
                                                                                                                            <i class="far fa-file-alt"></i> Text
                                                                                                                    </div>
                                                    </td>
                                                </tr>
                                                                                                                                            
                                                <tr style="width: 100%; padding: 5px 0px;background-color: #fff;">
                                                    <td style="text-align: left; padding:7px 10px;">
                                                                                                                                                                            <div class="form-group">
                                                                <input type="checkbox" id="2" onchange="markThisLessonAsCompleted(this.id);" value = 1  checked >
                                                                <label for="2"></label>
                                                            </div>
                                                        
                                                        <a href="http://localhost/onlinecource/home/lesson/java/1/2" id = "2" style="color: #444549;font-size: 14px;font-weight: 400;">
                                                            2:
                                                                                                                            Java là gì                                                                                                                     </a>

                                                        <div class="lesson_duration">
                                                                                                                            <i class="far fa-question-circle"></i> Quiz
                                                                                                                    </div>
                                                    </td>
                                                </tr>
                                                                                                                                    </table>
                                    </div>
                                </div>
                            </div>
                                                    <div class="card" style="margin:0px 0px;">
                                <div class="card-header course_card" id="heading-2">

                                    <button class="btn btn-link w-100 text-start d-grid" type="button" data-bs-toggle="collapse" data-bs-target="#collapse-2"  aria-expanded="false"  aria-controls="collapse-2" style="color: #535a66; background: none; border: none; white-space: normal;" onclick = "toggleAccordionIcon(this, '2')">
                                        <p class="w-100" style="color: #959aa2; font-size: 13px;">
                                            <span class="d-block-inline float-start">Section 2</span>
                                            <span style="float: right; font-weight: 100;" class="accordion_icon" id="accordion_icon_2">
                                                                                                    <i class="fa fa-plus"></i>
                                                                                            </span>
                                        </p>
                                        <p class="d-inline-block float-start text-start text-13px fw-500">Chapter 2</p>
                                    </button>
                                </div>

                                <div id="collapse-2" class="collapse " aria-labelledby="heading-2" data-parent="#accordionExample">
                                    <div class="card-body"  style="padding:0px;">
                                        <table style="width: 100%;">
                                                                                            
                                                <tr style="width: 100%; padding: 5px 0px;background-color: #fff;">
                                                    <td style="text-align: left; padding:7px 10px;">
                                                                                                                                                                            <div class="form-group">
                                                                <input type="checkbox" id="3" onchange="markThisLessonAsCompleted(this.id);" value = 1  checked >
                                                                <label for="3"></label>
                                                            </div>
                                                        
                                                        <a href="http://localhost/onlinecource/home/lesson/java/1/3" id = "3" style="color: #444549;font-size: 14px;font-weight: 400;">
                                                            1:
                                                                                                                            Tìm hiểu các thành phần có trong chương trình Java đầu tiên                                                                                                                    </a>

                                                        <div class="lesson_duration">
                                                                                                                            <i class="far fa-clock"></i>
                                                                17 Min                                                                                                                    </div>
                                                    </td>
                                                </tr>
                                                                                                                                    </table>
                                    </div>
                                </div>
                            </div>
                                            </div>
                    <!-- Lesson Content ends from here -->
                </div>

                <!-- ZOOM LIVE CLASS TAB STARTS-->
                
                    <div class="tab-pane fade" id="liveclass" role="tabpanel" aria-labelledby="liveclass-tab" style="text-align: center;">
                                                                                            </div>

                    <style>
                    .live_class_note {
                        border: 1px solid #bfdde4;
                        background-color: aliceblue;
                        margin: 30px 0px 10px;
                        color: #007791;
                        font-size: 12px;
                        padding: 10px;
                    }
                    .btn_zoom {
                        background-color: #2781FF;
                        border-color: #2781FF;
                    }
                    .btn_zoom:hover {
                        background-color: #2781FF;
                        border-color: #2781FF;
                    }
                    .btn_zoom:focus {
                        background-color: #2781FF;
                        border-color: #2781FF;
                    }
                    </style>
                <!-- ZOOM LIVE CLASS TAB ENDS-->

                            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
function toggleAccordionIcon(elem, section_id) {
    var accordion_section_ids = [];
    $(".accordion_icon").each(function(){ accordion_section_ids.push(this.id); });
    accordion_section_ids.forEach(function(item) {
        if (item === 'accordion_icon_'+section_id) {
            if ($('#'+item).html().trim() === '<i class="fa fa-plus"></i>') {
                $('#'+item).html('<i class="fa fa-minus"></i>')
            }else {
                $('#'+item).html('<i class="fa fa-plus"></i>')
            }
        }else{
            $('#'+item).html('<i class="fa fa-plus"></i>')
        }
    });
}

function checkCertificateEligibility() {
    $('#lesson_list_area').hide();
    $('#lesson_list_loader').show();
    $.ajax({
        url: 'http://localhost/onlinecource/addons/certificate/check_certificate_eligibility/1',
        success: function(response)
        {
            if (parseInt(response) === 1) {
                $('#download_certificate_area').show();
                $('#certificate-alert-success').show();
                $('#certificate-alert-warning').hide();

            }
            checkCourseProgression();
            getCertificateShareableUrl();

            $('#lesson_list_area').show();
            $('#lesson_list_loader').hide();
        }
    });
}

function checkCourseProgression() {
    $.ajax({
        url: 'http://localhost/onlinecource/home/check_course_progress/1',
        success: function(response)
        {
            if (parseInt(response) === 100) {
                $('#download_certificate_area').show();
                $('#certificate-alert-success').show();
                $('#certificate-alert-warning').hide();
            }else{
                $('#download_certificate_area').hide();
                $('#certificate-alert-success').hide();
                $('#certificate-alert-warning').show();
            }
            $('#progression').text(Math.round(response));
            $('#course_progress_area').attr('data-percent', Math.round(response));
            initProgressBar(Math.round(response));
        }
    });
}

function initProgressBar(dataPercent) {
    var totalProgress, progress;
    const circles = document.querySelectorAll('.circular-progress');
    for(var i = 0; i < circles.length; i++) {
        totalProgress = circles[i].querySelector('circle').getAttribute('stroke-dasharray');
        //progress = circles[i].parentElement.getAttribute('data-percent');
        progress = dataPercent;

        circles[i].querySelector('.bar').style['stroke-dashoffset'] = totalProgress * progress / 100;
    }
}

function getCertificateShareableUrl() {
    var user_id = '2';
    var course_id = '1';
    $.ajax({
        url: 'http://localhost/onlinecource/addons/certificate/get_certificate_url',
        type : 'POST',
        data : {user_id : user_id, course_id : course_id},
        success: function(response)
        {
            $('#certificate_download_btn').attr('href', response);
        }
    });
}

function sendCourseCompletionMail() {
    var user_id = '2';
    var course_id = '1';
    $.ajax({
        url: 'http://localhost/onlinecource/addons/certificate/send_course_completion_mail',
        type : 'POST',
        data : {user_id : user_id, course_id : course_id},
        success: function(response)
        {
            console.log(response);
        }
    });
}
</script>
                <!-- Course sections and lesson selector sidebar ends-->

                    <!-- Course content, video, quizes, files starts-->
            <div class="col-lg-9 order-1 course_col" id = "video_player_area">
    <!-- <div class="" style="background-color: #333;"> -->
            <div class="">
                            <div class="mt-5 px-3">
                    <p><span style="color: rgb(77, 81, 86); font-family: arial, sans-serif;">Java là một ngôn ngữ lập trình hướng đối tượng, dựa trên lớp được thiết kế để có càng ít phụ thuộc thực thi càng tốt.</span></p><p><span style="color: rgb(77, 81, 86); font-family: arial, sans-serif;"><br></span></p><table class="table table-bordered"><tbody><tr><td>aa</td><td>â</td></tr><tr><td>aa</td><td>qqq</td></tr><tr><td><br></td><td><br></td></tr></tbody></table><p><span style="color: rgb(77, 81, 86); font-family: arial, sans-serif;"><br></span><br></p>                </div>

                    </div>

        <div class="" style="margin: 20px 0;" id = "lesson-summary">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title w-100 mb-3">Note:</h5>
                                            <p><span style="color: rgb(77, 81, 86); font-family: arial, sans-serif;">Java là một ngôn ngữ lập trình hướng đối tượng, dựa trên lớp được thiết kế để có càng ít phụ thuộc thực thi càng tốt.</span><br></p>                                    </div>
            </div>
        </div>
    </div>
            <!-- Course content, video, quizes, files ends-->
        
    </div>

    
    <div class="row my-4">
        <div class="col-lg-9">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <ul class="nav nav-tabs border-0">
                                                                                            </ul>
                </div>
                <!--load body with ajax for any addon. First load course forum addon if exits or elseif-->
                <div class="col-md-12 p-4" id="load-tabs-body">
                                    </div>
                                                            </div>
        </div>
    </div>
</div>
<script src="http://localhost/onlinecource/assets/frontend/default/js/vendor/modernizr-3.5.0.min.js"></script>
<script src="http://localhost/onlinecource/assets/frontend/default/js/vendor/jquery-3.2.1.min.js"></script>
<script src="http://localhost/onlinecource/assets/frontend/default/js/popper.min.js"></script>
<script src="http://localhost/onlinecource/assets/frontend/default/js/bootstrap.min.js"></script>
<script src="http://localhost/onlinecource/assets/frontend/default/js/slick.min.js"></script>
<script src="http://localhost/onlinecource/assets/frontend/default/js/select2.min.js"></script>
<script src="http://localhost/onlinecource/assets/frontend/default/js/tinymce.min.js"></script>
<script src="http://localhost/onlinecource/assets/frontend/default/js/multi-step-modal.js"></script>
<script src="http://localhost/onlinecource/assets/frontend/default/js/jquery.webui-popover.min.js"></script>
<script src="https://content.jwplatform.com/libraries/O7BMTay5.js"></script>
<script src="http://localhost/onlinecource/assets/frontend/default/js/main.js"></script>
<script src="http://localhost/onlinecource/assets/global/toastr/toastr.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/nestable2/1.6.0/jquery.nestable.min.js" charset="utf-8"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js" integrity="sha384-FzT3vTVGXqf7wRfy8k4BiyzvbNfeYjK+frTVqZeNDFl8woCbF0CYG6g2fMEFFo/i" crossorigin="anonymous"></script>
<script src="http://localhost/onlinecource/assets/frontend/default/js/bootstrap-tagsinput.min.js"></script>
<script src="http://localhost/onlinecource/assets/frontend/default/js/custom.js"></script>
<script src="http://localhost/onlinecource/assets/lessons/js/custom.js"></script>
<script>
function toggle_lesson_view() {
    $('#lesson-container').toggleClass('justify-content-center');
    $("#video_player_area").toggleClass("order-md-1");
    $("#lesson_list_area").toggleClass("col-lg-5 order-md-1");
}
</script>
<script type="text/javascript">
//saving the current progress and starting from the saved progress
var newProgress;
var savedProgress;
var currentProgress = '1';
var lessonType = 'text';
var videoProvider = '';

function markThisLessonAsCompleted(lesson_id) {
  $('#lesson_list_area').hide();
  $('#lesson_list_loader').show();
  var course_id = "1";

  $.ajax({
    type : 'POST',
    url : 'http://localhost/onlinecource/home/update_watch_history_manually',
    data : {lesson_id : lesson_id, course_id:course_id},
    success : function(response){
      $('#lesson_list_area').show();
      $('#lesson_list_loader').hide();
      var responseVal = JSON.parse(response);
      // console.log(responseVal);
      // console.log(responseVal.course_progress);
    }
  });
}


$(document).ready(function() {
  if (lessonType == 'video' && videoProvider == 'html5') {
    var totalDuration = document.querySelector('#player').duration;

    if (currentProgress == 1 || currentProgress == totalDuration) {
      document.querySelector('#player').currentTime = 0;
    }else {
      document.querySelector('#player').currentTime = currentProgress;
    }
  }
});

var counter = 0;
player.on('canplay', event => {
  if (counter == 0) {
    if (currentProgress == 1) {
      document.querySelector('#player').currentTime = 0;
    }else{
      document.querySelector('#player').currentTime = currentProgress;
    }
  }
  counter++;
});


//const player = new Plyr('#player');
if(0 && typeof player === 'object' && player !== null){
    let lesson_id = '1';
    let course_id = '1';
    let previousSavedDuration = 0;
    let currentDuration = 0;
    setInterval(function(){
        if("text" == "video"){
            currentDuration = parseInt(player.currentTime);
        }else{
            currentDuration = 0;
        }

        if (lesson_id && course_id && (currentDuration%5) == 0 && previousSavedDuration != currentDuration) {
            previousSavedDuration = currentDuration;

            $.ajax({
              type : 'POST',
              url : 'http://localhost/onlinecource/home/update_watch_history_with_duration',
              data : {lesson_id : lesson_id, course_id : course_id, current_duration: currentDuration},
              success : function(response){
                var responseVal = JSON.parse(response);
                // console.log(responseVal);
                // console.log(responseVal.course_progress);

              }
            });
        }

        //console.log('Avoid Server Call'+currentDuration);
    }, 1000);
}


setTimeout(function(){
  $('.remove_video_src').remove();
}, 500);
</script><link rel="stylesheet" type="text/css" href="http://localhost/onlinecource/assets/frontend/eu-cookie/purecookie.css" async />

<div class="cookieConsentContainer" id="cookieConsentContainer" style="opacity: .9; display: block; display: none;">
  <!-- <div class="cookieTitle">
    <a>Cookies.</a>
  </div> -->
  <div class="cookieDesc">
    <p>
      This website uses cookies to personalize content and analyse traffic in order to offer you a better experience.      <a class="link-cookie-policy" href="http://localhost/onlinecource/home/cookie_policy">Cookie policy</a>
    </p>
  </div>
  <div class="cookieButton">
    <a onclick="cookieAccept();">Accept</a>
  </div>
</div>
<script>
  $(document).ready(function () {
    if (localStorage.getItem("accept_cookie_academy")) {
      //localStorage.removeItem("accept_cookie_academy");
    }else{
      $('#cookieConsentContainer').fadeIn(1000);
    }
  });

  function cookieAccept() {
    if (typeof(Storage) !== "undefined") {
      localStorage.setItem("accept_cookie_academy", true);
      localStorage.setItem("accept_cookie_time", "06/18/2022");
      $('#cookieConsentContainer').fadeOut(1200);
    }
  }
</script>
  </body>
</html>
