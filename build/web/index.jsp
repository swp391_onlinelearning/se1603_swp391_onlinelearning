
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Online courses</title>
        <link name="favicon" type="image/x-icon" href="#" rel="shortcut icon" />
        <jsp:include page="includes_top.jsp"/>
    </head>
    <body>
        <jsp:include page="header.jsp"/>
    </body>
</html>
