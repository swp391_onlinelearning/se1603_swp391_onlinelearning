/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author genni
 */
public class Lession {
    private int id;
    private String title;
    private int courseID;
    private int sectionID;
    private String videoURL;
    private String lestion_type;
    private String attachment;
    private String sumary;
    private String description;

    public Lession() {
    }

    public Lession(int id, String title, int courseID, int sectionID, String videoURL, String lestion_type, String attachment, String sumary, String description) {
        this.id = id;
        this.title = title;
        this.courseID = courseID;
        this.sectionID = sectionID;
        this.videoURL = videoURL;
        this.lestion_type = lestion_type;
        this.attachment = attachment;
        this.sumary = sumary;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getCourseID() {
        return courseID;
    }

    public void setCourseID(int courseID) {
        this.courseID = courseID;
    }

    public int getSectionID() {
        return sectionID;
    }

    public void setSectionID(int sectionID) {
        this.sectionID = sectionID;
    }

    public String getVideoURL() {
        return videoURL;
    }

    public void setVideoURL(String videoURL) {
        this.videoURL = videoURL;
    }

    public String getLestion_type() {
        return lestion_type;
    }

    public void setLestion_type(String lestion_type) {
        this.lestion_type = lestion_type;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public String getSumary() {
        return sumary;
    }

    public void setSumary(String sumary) {
        this.sumary = sumary;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
