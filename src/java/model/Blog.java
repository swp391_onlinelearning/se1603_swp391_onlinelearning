/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author 
 */
public class Blog {
    private int blog_id;
    private int blog_category_id;
    private int user_id;
    private String title;
    private String description;
    private String thumbnail;
    private String banner;
    private String added_date;
    private String short_description;

    public Blog(int blog_id, int blog_category_id, int user_id, String title, String description, String thumbnail, String banner, String added_date, String short_description) {
        this.blog_id = blog_id;
        this.blog_category_id = blog_category_id;
        this.user_id = user_id;
        this.title = title;
        this.description = description;
        this.thumbnail = thumbnail;
        this.banner = banner;
        this.added_date = added_date;
        this.short_description = short_description;
    }

    public Blog() {
    }

    public int getBlog_id() {
        return blog_id;
    }

    public void setBlog_id(int blog_id) {
        this.blog_id = blog_id;
    }

    public int getBlog_category_id() {
        return blog_category_id;
    }

    public void setBlog_category_id(int blog_category_id) {
        this.blog_category_id = blog_category_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public String getAdded_date() {
        return added_date;
    }

    public void setAdded_date(String added_date) {
        this.added_date = added_date;
    }

    public String getShort_description() {
        return short_description;
    }

    public void setShort_description(String short_description) {
        this.short_description = short_description;
    }
    
}
