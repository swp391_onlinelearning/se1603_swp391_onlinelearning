/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author 
 */
public class Course_Category {
    private int blog_category_id;
    private String title;
    private String thumnail;

    public int getBlog_category_id() {
        return blog_category_id;
    }

    public void setBlog_category_id(int blog_category_id) {
        this.blog_category_id = blog_category_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getThumnail() {
        return thumnail;
    }

    public void setThumnail(String thumnail) {
        this.thumnail = thumnail;
    }

    public Course_Category(int blog_category_id, String title, String thumnail) {
        this.blog_category_id = blog_category_id;
        this.title = title;
        this.thumnail = thumnail;
    }

    public Course_Category() {
    }
}
