/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author 
 */
public class Course {
    private int id;
    private String title;
    private String short_description;
    private String description;
    private int category_id;
    private float price;
    private String thumbnail;
    private int is_free_course;

    public Course(int id, String title, String short_description, String description, int category_id, float price, String thumbnail, int is_free_course) {
        this.id = id;
        this.title = title;
        this.short_description = short_description;
        this.description = description;
        this.category_id = category_id;
        this.price = price;
        this.thumbnail = thumbnail;
        this.is_free_course = is_free_course;
    }

    public Course() {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShort_description() {
        return short_description;
    }

    public void setShort_description(String short_description) {
        this.short_description = short_description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public int getIs_free_course() {
        return is_free_course;
    }

    public void setIs_free_course(int is_free_course) {
        this.is_free_course = is_free_course;
    }
}
