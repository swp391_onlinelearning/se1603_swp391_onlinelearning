/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import context.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.enrol;

/**
 *
 * @author genni
 */
public class EnrolDAO {
    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    
     public enrol getLast(){
       ResultSet resultSet = DBContext.querySet("SELECT TOP 1 * FROM [enrol] ORDER BY [enrol].[id] DESC");
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    return new enrol(
                            resultSet.getInt(1),
                            resultSet.getInt(2),
                            resultSet.getInt(3),
                            resultSet.getString(4));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
     
     public boolean addCourse(int uid, int course) {
        EnrolDAO dao = new EnrolDAO();
        try {
            String sql = "INSERT INTO [dbo].[enrol]\n" +
"           ([id]\n" +
"           ,[user_id]\n" +
"           ,[course_id]\n" +
"           ,[date_added])\n" +
"           VALUES\n" +
"           (?,?,?,GETDATE())";
            
            PreparedStatement pr = DBContext.getConnection().prepareStatement(sql);
            pr.setInt(1,dao.getLast().getId()+1);
            pr.setInt(2, uid);
            pr.setInt(3, course);
            pr.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
     
     public List<enrol> getListEnrollByID(String uid){
         List<enrol> list = new ArrayList<>();
         ResultSet resultSet = DBContext.querySet("SELECT * FROM [enrol] where [enrol].[user_id] = ?",uid);
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    list.add(new enrol(resultSet.getInt(1), resultSet.getInt(2), resultSet.getInt(3),resultSet.getString(4)));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return list;
     }
    
}
