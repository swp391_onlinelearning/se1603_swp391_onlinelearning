/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import context.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.User;

/**
 *
 * @author 
 */
public class UserDAO {
    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    
    public boolean deleteUser(int id) {
        try {
            String sql = "DELETE FROM [dbo].[User]  WHERE [User].[id] = ?";  
            PreparedStatement  pr = DBContext.getConnection().prepareStatement(sql);
            pr.setInt(1, id);     
            pr.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    
    public User login(String user, String pass) {
        ResultSet resultSet = DBContext.querySet("select * from [User] where [User].[email] = ?  and [User].[password] = ?", user, pass);
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    return new User(
                            resultSet.getInt(1),
                            resultSet.getString(2),
                            resultSet.getString(3),
                            resultSet.getString(4),
                            resultSet.getString(5),
                            resultSet.getInt(6),
                            resultSet.getInt(7),
                            resultSet.getString(8));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    public List<User> getListUser() {
        List<User> list = new ArrayList<>();
        ResultSet resultSet = DBContext.querySet("SELECT * FROM [User]");
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    list.add(new  User(
                            resultSet.getInt(1),
                            resultSet.getString(2),
                            resultSet.getString(3),
                            resultSet.getString(4),
                            resultSet.getString(5),
                            resultSet.getInt(6),
                            resultSet.getInt(7),
                            resultSet.getString(8)));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return list;
    }
    
    public User getLast(){
       ResultSet resultSet = DBContext.querySet("SELECT TOP 1 * FROM [User] ORDER BY [User].[id] DESC");
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    return new User(
                            resultSet.getInt(1),
                            resultSet.getString(2),
                            resultSet.getString(3),
                            resultSet.getString(4),
                            resultSet.getString(5),
                            resultSet.getInt(6),
                            resultSet.getInt(7),
                            resultSet.getString(8));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    
    public User getUserByID(String id){
       ResultSet resultSet = DBContext.querySet("select * from [User] where [User].[id] = ?",id);
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    return new User(
                            resultSet.getInt(1),
                            resultSet.getString(2),
                            resultSet.getString(3),
                            resultSet.getString(4),
                            resultSet.getString(5),
                            resultSet.getInt(6),
                            resultSet.getInt(7),
                            resultSet.getString(8));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    
   
    
    public User getUserBy(String email){
       ResultSet resultSet = DBContext.querySet("select * from [User] where [User].[email] = ?",email);
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    return new User(
                            resultSet.getInt(1),
                            resultSet.getString(2),
                            resultSet.getString(3),
                            resultSet.getString(4),
                            resultSet.getString(5),
                            resultSet.getInt(6),
                            resultSet.getInt(7),
                            resultSet.getString(8));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    
    public User checkEmailExit(String email){
       ResultSet resultSet = DBContext.querySet("select * from [User] where [User].[email] = ?",email);
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    return new User(
                            resultSet.getInt(1),
                            resultSet.getString(2),
                            resultSet.getString(3),
                            resultSet.getString(4),
                            resultSet.getString(5),
                            resultSet.getInt(6),
                            resultSet.getInt(7),
                            resultSet.getString(8));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    
    public boolean register(String first_name, String last_name, String email, String password, int role, int status, String image) {
        UserDAO dao = new UserDAO();
        try {
            String sql = "INSERT INTO [dbo].[User]\n" +
"           ([id]\n" +
"           ,[first_name]\n" +
"           ,[last_name]\n" +
"           ,[email]\n" +
"           ,[password]\n" +
"           ,[role_id]\n" +
"           ,[status]\n" +
"           ,[image])\n" +
"            VALUES (?,?,?,?,?,?,?,?)";
            
            PreparedStatement pr = DBContext.getConnection().prepareStatement(sql);
            pr.setInt(1,dao.getLast().getId()+1);
            pr.setString(2, first_name);
            pr.setString(3, last_name);
            pr.setString(4, email);
            pr.setString(5, password);
            pr.setInt(6, role);
            pr.setInt(7, status);
            pr.setString(8, image);
            pr.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    
    public boolean updateUser(String first_name, String last_name, String email, int role, int status, String image){
        try {
            String sql = "UPDATE [dbo].[User]\n" +
                        "   SET\n" +
                        "      [first_name] = ? "+
                        "      ,[last_name] = ?" +
                        "      ,[email] = ?" +
                        "      ,[role_id] = ?" +
                        "      ,[status] = ?" +
                        "      ,[image] = ?" +
                        " WHERE [User].[email] = ?";
            
            PreparedStatement pr = DBContext.getConnection().prepareStatement(sql);         
            pr.setString(1, first_name);
            pr.setString(2, last_name);
            pr.setString(3, email);
            pr.setInt(4, role);
            pr.setInt(5, status);
            pr.setString(6, image);
            pr.setString(7, email);
            pr.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    
    public static void main(String[] args) {
      UserDAO dao = new UserDAO();
        System.out.println(dao.updateUser("ky", "minh123", "gennikemk@gmail.com", 1, 1, "null"));
    };
}
