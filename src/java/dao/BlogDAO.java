/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import context.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Blog;

/**
 *
 * @author 
 */
public class BlogDAO {

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    public List<Blog> getAllBlogs() {
        List<Blog> list = new ArrayList<>();
        ResultSet resultSet = DBContext.querySet("select * from Blogs");
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    list.add(new Blog(
                            resultSet.getInt(1),
                            resultSet.getInt(2),
                            resultSet.getInt(3),
                            resultSet.getString(4),
                            resultSet.getString(5),
                            resultSet.getString(6),
                            resultSet.getString(7),
                            resultSet.getString(8),
                            resultSet.getString(9)
                    ));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    public Blog getBlogByID(int id) {
        ResultSet resultSet = DBContext.querySet("select * from Blogs where [Blogs].[blog_id] = ?", id);
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    return new Blog(
                            resultSet.getInt(1),
                            resultSet.getInt(2),
                            resultSet.getInt(3),
                            resultSet.getString(4),
                            resultSet.getString(5),
                            resultSet.getString(6),
                            resultSet.getString(7),
                            resultSet.getString(8),
                            resultSet.getString(9));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public List<Blog> getLatest() {
        List<Blog> list = new ArrayList<>();
        ResultSet resultSet = DBContext.querySet("SELECT TOP 5 * FROM Blogs");
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    list.add(new Blog(
                            resultSet.getInt(1),
                            resultSet.getInt(2),
                            resultSet.getInt(3),
                            resultSet.getString(4),
                            resultSet.getString(5),
                            resultSet.getString(6),
                            resultSet.getString(7),
                            resultSet.getString(8),
                            resultSet.getString(9)
                    ));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return list;
    }
    
    public Blog getLast() {
        ResultSet resultSet = DBContext.querySet("SELECT TOP 1 * FROM Blogs ORDER BY [Blogs].[blog_id] DESC");
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    return new Blog(
                            resultSet.getInt(1),
                            resultSet.getInt(2),
                            resultSet.getInt(3),
                            resultSet.getString(4),
                            resultSet.getString(5),
                            resultSet.getString(6),
                            resultSet.getString(7),
                            resultSet.getString(8),
                            resultSet.getString(9));         
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    public boolean addBlog(int categoryid, int uid, String title, String des, String thumnail, String banner, String sdes) {
        BlogDAO dao = new BlogDAO();
        try {
            String sql = "INSERT INTO [dbo].[Blogs] ([blog_id] ,[blog_category_id] ,[user_id] ,[title] ,[description] ,[thumbnail] ,[banner] ,[added_date] ,[short_description]) VALUES (?,?,?,?,?,?,?,GETDATE(),?)";  
            PreparedStatement pr = DBContext.getConnection().prepareStatement(sql);
            pr.setInt(1,dao.getLast().getBlog_id() +1);      
            pr.setInt(2, categoryid);
            pr.setInt(3, uid);
            pr.setString(4, title);
            pr.setString(5, des);
            pr.setString(6, thumnail);
            pr.setString(7, banner);
            pr.setString(8, sdes);
            pr.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    
    public boolean updateBlog(int categoryid, String title, String des, String thumnail, String banner, String sdes, int id) {
       BlogDAO dao = new BlogDAO();
        try {
            String sql = "UPDATE [dbo].[Blogs]\n" +
"   SET [blog_category_id] = ?" +
"      ,[title] = ?" +
"      ,[description] = ?" +
"      ,[thumbnail] = ?" +
"      ,[banner] = ?" +
"      ,[short_description] = ?" +
" WHERE [Blogs].[blog_id]=?";
            
            PreparedStatement pr = DBContext.getConnection().prepareStatement(sql);  
            pr.setInt(1, categoryid);       
            pr.setString(2, title);
            pr.setString(3, des);
            pr.setString(4, thumnail);
            pr.setString(5, banner);
            pr.setString(6, sdes);
            pr.setInt(7, id);
            pr.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    
    public static void main(String[] args) {
        BlogDAO dao = new BlogDAO();
        System.out.println(dao.getBlogByID(1));
    }
;
}
