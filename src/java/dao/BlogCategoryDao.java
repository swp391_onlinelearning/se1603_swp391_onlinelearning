/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import context.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Blog_Category;

/**
 *
 * @author 
 */
public class BlogCategoryDao {
    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    
    
    public boolean deleteCategory(int id) {
        try {
            String sql = "DELETE FROM [dbo].[Blog_category]  WHERE [Blog_category].[blog_category_id] = ?";  
            PreparedStatement  pr = DBContext.getConnection().prepareStatement(sql);
            pr.setInt(1, id);     
            pr.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    
     public boolean addCategory(String name) {
        BlogCategoryDao dao = new BlogCategoryDao();
        try {
            String sql = "INSERT INTO [dbo].[Blog_category] ([blog_category_id],[title]) VALUES (?,?)";  
            PreparedStatement pr = DBContext.getConnection().prepareStatement(sql);
            pr.setInt(1,dao.getLast().getBlog_category_id()+1);
            pr.setString(2, name);
           
            pr.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
     
    public List<Blog_Category> getAllBlogCategory() {
        List<Blog_Category> list = new ArrayList<>();
        ResultSet resultSet = DBContext.querySet("select * from Blog_category");
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    list.add(new Blog_Category(
                            resultSet.getInt(1), 
                            resultSet.getString(2)
                    ));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return list;
    }
    
    public Blog_Category getLast() {
        ResultSet resultSet = DBContext.querySet("SELECT TOP 5 * FROM Blog_category ORDER BY [Blog_category].[blog_category_id] DESC");
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    return new Blog_Category(
                            resultSet.getInt(1),
                            resultSet.getString(2));         
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    
    public List<Blog_Category> getLatest() {
        List<Blog_Category> list = new ArrayList<>();
        ResultSet resultSet = DBContext.querySet("SELECT TOP 5 * FROM Blog_category");
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    list.add(new Blog_Category(
                            resultSet.getInt(1), 
                            resultSet.getString(2)
                    ));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return list;
    }
    public static void main(String[] args) {
        BlogCategoryDao dao = new BlogCategoryDao();
        boolean kq = dao.deleteCategory(4);
    }
}
