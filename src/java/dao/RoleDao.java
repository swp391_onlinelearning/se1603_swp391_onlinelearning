/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import context.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Role;

/**
 *
 * @author 
 */
public class RoleDao {
    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    
    
    
    public List<Role> getListRole() {
        List<Role> list = new ArrayList<>();
        ResultSet resultSet = DBContext.querySet("SELECT * FROM [Role]");
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    list.add(new  Role(
                            resultSet.getInt(1),
                            resultSet.getString(2)));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return list;
    }
}
