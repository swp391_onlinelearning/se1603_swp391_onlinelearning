/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import context.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Course;
import model.Lession;
import model.Section;
import model.enrol;



/**
 *
 * @author 
 */
public class CourseDAO {

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    public List<Course> getListCourse() {
        List<Course> list = new ArrayList<>();
        ResultSet resultSet = DBContext.querySet("SELECT * FROM [Course]");
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    list.add(new Course(
                            resultSet.getInt(1),
                            resultSet.getString(2),
                            resultSet.getString(3),
                            resultSet.getString(4),
                            resultSet.getInt(5),
                            resultSet.getFloat(6),
                            resultSet.getString(7),
                            resultSet.getInt(8)));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return list;
    }
    
    public Course getCourseByID(String id){
       ResultSet resultSet = DBContext.querySet("select * from [Course] where [Course].[id] = ?",id);
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    return new Course(
                            resultSet.getInt(1),
                            resultSet.getString(2),
                            resultSet.getString(3),
                            resultSet.getString(4),
                            resultSet.getInt(5),
                            resultSet.getFloat(6),
                            resultSet.getString(7),
                            resultSet.getInt(8));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    
    public boolean addCourse(String title, String short_description, String description, int category_id, float price, String thumbnail, int is_free_course) {
        CourseDAO dao = new CourseDAO();
        try {
            String sql = "INSERT INTO [dbo].[course]\n" +
"           ([id]\n" +
"           ,[title]\n" +
"           ,[short_description]\n" +
"           ,[description]\n" +
"           ,[category_id]\n" +
"           ,[price]\n" +
"           ,[thumbnail]\n" +
"           ,[is_free_course])\n" +
"            VALUES (?,?,?,?,?,?,?,?)";
            
            PreparedStatement pr = DBContext.getConnection().prepareStatement(sql);
            pr.setInt(1,dao.getLast().getId()+1);
            pr.setString(2, title);
            pr.setString(3, short_description);
            pr.setString(4, description);
            pr.setInt(5, category_id);
            pr.setFloat(6, price);
            pr.setString(7, thumbnail);
            pr.setInt(8, is_free_course);
            pr.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    
    public boolean updateCourse(String title, String short_description, String description, int category_id, float price, String thumbnail, int is_free_course,int id) {
        CourseDAO dao = new CourseDAO();
        try {
            String sql = "UPDATE [dbo].[course]\n" +
"   SET [title] = ?\n" +
"      ,[short_description] = ?\n" +
"      ,[description] = ?\n" +
"      ,[category_id] = ?\n" +
"      ,[price] = ?\n" +
"      ,[thumbnail] = ?\n" +
"      ,[is_free_course] = ?\n" +
" WHERE [course].[id]=?";
            
            PreparedStatement pr = DBContext.getConnection().prepareStatement(sql);
            pr.setString(1, title);
            pr.setString(2, short_description);
            pr.setString(3, description);
            pr.setInt(4, category_id);
            pr.setFloat(5, price);
            pr.setString(6, thumbnail);
            pr.setInt(7, is_free_course);
            pr.setInt(8, id);
            pr.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    
    public Course getLast(){
       ResultSet resultSet = DBContext.querySet("SELECT TOP 1 * FROM [Course] ORDER BY [Course].[id] DESC");
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    return new Course(
                            resultSet.getInt(1),
                            resultSet.getString(2),
                            resultSet.getString(3),
                            resultSet.getString(4),
                            resultSet.getInt(5),
                            resultSet.getFloat(6),
                            resultSet.getString(7),
                            resultSet.getInt(8));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    public List<Section> getListSectionByCourseId(String id){
         List<Section> list = new ArrayList<>();
        ResultSet resultSet = DBContext.querySet("SELECT * FROM section where [section].[course_id] = ?",id);
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    list.add(new Section(resultSet.getInt(1), resultSet.getString(2), resultSet.getInt(3),resultSet.getInt(4)));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return list;
    }
    
    public Section getSectionById(String id){
         
        ResultSet resultSet = DBContext.querySet("SELECT * FROM section where [section].[id] = ?",id);
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    new Section(resultSet.getInt(1), resultSet.getString(2), resultSet.getInt(3),resultSet.getInt(4));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    
    public List<Lession> getListLessionByCourseId(String id){
         List<Lession> list = new ArrayList<>();
        ResultSet resultSet = DBContext.querySet("SELECT * FROM lesson where [lesson].[course_id] = ?",id);
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    list.add(new Lession(resultSet.getInt(1), resultSet.getString(2), resultSet.getInt(3), resultSet.getInt(4), resultSet.getString(5), resultSet.getString(6), resultSet.getString(7), resultSet.getString(8), resultSet.getString(9)));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return list;
    }
    
    public Lession getLastLession(){
       ResultSet resultSet = DBContext.querySet("SELECT TOP 1 * FROM lesson ORDER BY lesson.id DESC");
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    return new Lession(resultSet.getInt(1), 
                            resultSet.getString(2), 
                            resultSet.getInt(3), 
                            resultSet.getInt(4), 
                            resultSet.getString(5),
                            resultSet.getString(6), 
                            resultSet.getString(7), 
                            resultSet.getString(8), 
                            resultSet.getString(9));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    
    
    
    public boolean addLession(String title, int cid, int sid, String des) {
        CourseDAO dao = new CourseDAO();
        try {
            String sql = "INSERT INTO [dbo].[lesson]([id] ,[title] ,[course_id],[section_id],[video_url],[lesson_type],[attachment],[summary],[description]) VALUES (?,?,?,?,?,?,?,?,?)";
            
            PreparedStatement pr = DBContext.getConnection().prepareStatement(sql);
            pr.setInt(1,dao.getLastLession().getId()+1);
            pr.setString(2,title);
            pr.setInt(3,cid);
            pr.setInt(4,sid);
            pr.setString(5,"null");
            pr.setString(6,"null");
            pr.setString(7,"null");
            pr.setString(8,"null");
            pr.setString(9,des);
            pr.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    
    public Section getLastSection(){
       ResultSet resultSet = DBContext.querySet("SELECT TOP 1 * FROM section ORDER BY section.id DESC");
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    return new Section(resultSet.getInt(1), resultSet.getString(2), resultSet.getInt(3), resultSet.getInt(4));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    
    public Section getLastSectionId(int id){
       ResultSet resultSet = DBContext.querySet("SELECT TOP 1 * FROM section where section.course_id= ? ORDER BY section.id DESC",id);
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    return new Section(resultSet.getInt(1), resultSet.getString(2), resultSet.getInt(3), resultSet.getInt(4));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    
    public boolean addSection(String title, int cid) {
        CourseDAO dao = new CourseDAO();
        try {
            String sql = "INSERT INTO [dbo].[section] ([id],[title],[course_id] ,[section]) VALUES  (?,?,?,?)";
            PreparedStatement pr = DBContext.getConnection().prepareStatement(sql);
            pr.setInt(1,dao.getLastSection().getId()+1);
            pr.setString(2,title);
            pr.setInt(3,cid);
            pr.setInt(4,dao.getLastSectionId(cid).getSection()+1);
          
            pr.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    
    
    public boolean updateLession(String title, int secid, String des, int id) {
        try {
            String sql = "UPDATE [dbo].[lesson] SET [title] = ?,[section_id] = ?, [description] = ? WHERE [id] = ?";
            
            PreparedStatement pr = DBContext.getConnection().prepareStatement(sql);
            pr.setString(1, title);
            pr.setInt(2, secid);
            pr.setString(3, des);
            pr.setInt(4, id);
            pr.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    
    public  Lession getLessionById(String cid,String lid){
        ResultSet resultSet = DBContext.querySet("SELECT * FROM lesson where [lesson].[course_id] = ? and [lesson].[id] = ?",cid,lid);
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    return new Lession(resultSet.getInt(1),
                            resultSet.getString(2), 
                            resultSet.getInt(3), 
                            resultSet.getInt(4), 
                            resultSet.getString(5), 
                            resultSet.getString(6), 
                            resultSet.getString(7), 
                            resultSet.getString(8), 
                            resultSet.getString(9));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    
    public boolean deleteLession(String id) {
        try {
            String sql = "DELETE FROM [dbo].[lesson]  WHERE [lesson].[id] = ?";  
            PreparedStatement  pr = DBContext.getConnection().prepareStatement(sql);
            pr.setString(1, id);     
            pr.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    
    public boolean deleteSection(String id) {
        try {
            String sql = "DELETE FROM [dbo].[section]  WHERE [section].[id] = ?";  
            PreparedStatement  pr = DBContext.getConnection().prepareStatement(sql);
            pr.setString(1, id);     
            pr.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    
    public List<Course> getListCourseByUserID(String uid){
        List<Course> list = new ArrayList<>();
        EnrolDAO daoE = new EnrolDAO();
        CourseDAO daoC = new CourseDAO();
        List<enrol> listE = daoE.getListEnrollByID(uid);
        for (enrol o : listE) {
            list.add(daoC.getCourseByID(String.valueOf(o.getCid())));        
        }       
        return list;
    }
     public boolean deleteCourse(int id) {
        try {
            String sql = "DELETE FROM [dbo].[Course]  WHERE [Course].[id] = ?";  
            PreparedStatement  pr = DBContext.getConnection().prepareStatement(sql);
            pr.setInt(1, id);     
            pr.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    
     
    public static void main(String[] args) {
        CourseDAO dao = new CourseDAO();
            
        dao.addSection("test", 1);
    }
    
}
